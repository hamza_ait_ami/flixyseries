<?php

ob_start();
/*echo '<center><div class="success">All The movies & episodes Are UP!! .. if you have any suggestions press <a href="/requestmovie/" style="color:#40C2DF;"> HERE </a> .. We will provide it within 24H MAX!</div></center>';*/

$grid = $db->prepare("SELECT * FROM flixygrid ");
$grid->execute();

$xd =array();
while($data = $grid -> fetch(PDO:: FETCH_ASSOC)) {
   array_push($xd, $data);
}
?>


<div class="wrapper-fluid">
<div class="row home_header">
	<div class="column-6">
		<h2>Latest episodes Added</h2>
		<div class="turn_episodes">
			<a href="#" class="turn_episode_left"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
			<a href="#" class="turn_episode_right"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
		</div>
		<div class="home_header_container">
			<ul class="home_header_content">
				<!-- Dir 12 last episodes ibano -->
				<?php
				$daysafter = date("Y-m-d",time());
				$episodes = $db->prepare("SELECT * FROM flixyepisodes where openload != '' order by pinned desc,air_date desc limit 12 ");
				$episodes->execute();
				$counts = $episodes -> rowCount();
				while($gepisodes = $episodes->fetch(PDO:: FETCH_ASSOC)){
				$serie = $db->prepare("SELECT * FROM flixyseries where id = :id");
				$serie->execute(array(':id'=>$gepisodes['serieid']));
				$gserieinfos = $serie->fetch(PDO:: FETCH_ASSOC);
				echo'
					<li>
						<a href="'.ROOTPATH.'/episode/'.$gepisodes['id'].'/'.str_replace(" ", "-",$gserieinfos['name']).'-season-'.$gepisodes['season'].'-episode-'.$gepisodes['epid'].'/">
							<img src="'.ROOTPATH.'/images/series/'.str_replace(" ", "-",$gserieinfos['name']).'.jpg" alt="">
							<h2>'.$gserieinfos['name'].' - Season '.$gepisodes['season'].'</h2>
							<span>Episode '.$gepisodes['epid'];
							if ($gepisodes['pinned']){
								echo ' <i class="fa fa-thumb-tack" aria-hidden="true" style="color:red;"></i>';
							}
							echo' </span>
						</a>
					</li>
				';
				}
				?>
			</ul>
		</div>
		
	</div>
	<div class="column-6">
		<h2>Top Movies This Month! </h2>
		<div class="turn_episodes">
			<a href="#" class="turn_movie_left"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
			<a href="#" class="turn_movie_right"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
		</div>
		<div class="home_header_container">
			<ul class="home_header_content">
			<?php
				$daysbefore = date("Y-m-d",time() - 60*24*60*60);
				$daysafter = date("Y-m-d",time() + 60*24*60*60);
				$movies = $db->prepare("SELECT * FROM flixymovies where openload != '' and Released >= '$daysbefore' and Released <= '$daysafter' order by views desc limit 12 ");
				$movies->execute();
				$counts = $movies -> rowCount();
				while($gmovies = $movies->fetch(PDO:: FETCH_ASSOC)){
				echo'
					<li>
						<a href="'.ROOTPATH.'/movies/'.$gmovies['id'].'/'.str_replace(" ", "-",$gmovies['name']).'-'.$gmovies['Year'].'/">
							<img src="'.ROOTPATH.'/images/movies/'.str_replace("?", "_",str_replace(":", "_",str_replace(" ", "-",$gmovies['name']))).'.jpg" alt="">
							<h2>'.$gmovies['name'].' - '.$gmovies['Year'].'</h2>
							<span>'.$gmovies["Quality"].'</span>
						</a>
					</li>
				';
				}
			?>
			</ul>
		</div>
	</div>
</div>
<?php
if (@$_GET['id'] == "rating"){
	$order = "Rating";
}elseif (@$_GET['id'] == "watched"){
	$order = "views";
}else{
	$order = "pinned desc, Released";
}

?>
<div class="row movies_boxes">
	<ul class="movies_page">
		<li><a href="<?=ROOTPATH.'/index/'?>">Latest Movies</a></li>
		<li><a href="<?=ROOTPATH.'/index/rating/'?>">Best Movies</a></li>
		<li><a href="<?=ROOTPATH.'/index/watched/'?>">Most Watched</a></li>
	</ul>
<?php
$sqls = " SELECT * FROM flixymovies where openload != '' order by $order desc, id desc limit 30";
$qs=$db->prepare($sqls);
$qs->execute();

while ($r=$qs->fetch(PDO::FETCH_ASSOC)) {
	$cats = $db->prepare("SELECT * FROM flixycats_movies where id_movie = '".$r['id']."' limit 1");
	$cats->execute();
	$cato = $cats->fetch(PDO::FETCH_ASSOC);
	$catss = $db->prepare("SELECT * FROM flixycats where id = '".$cato['id_cat']."' limit 1");
	$catss->execute();
	$catoo = $catss->fetch(PDO::FETCH_ASSOC);
?>
	<div class="column-3">
		<div><a href="<?=ROOTPATH.'/movies/'.$r['id'].'/'.str_replace(" ", "-",$r['name']).'-'.$r['Year'].'/';?>">
			<img src="<?=ROOTPATH.'/images/movies/'.str_replace("/", "_",str_replace("?", "_",str_replace(":", "_",str_replace(" ", "-",$r['name']))))?>.jpg" alt="">
			<div class="movie_desc" style="background-image: url('<?=ROOTPATH.'/images/background/'.str_replace("/", "_",str_replace("?", "_",str_replace(":", "_",str_replace(" ", "-",$r['name']))))?>.jpg');">
				<h1><?=$r['name']." - ".$r['Year']?></h1>
				<p><?php
				echo substr($r["Plot"], 0,200);
				if (strlen($r["Plot"]) >= 200){
					echo " ...";
				}
				?></p>
				<span class="movie_rating"><i class="fa fa-star"></i><b><?php if ($r["Rating"] == 0){echo "-";}else{echo $r["Rating"];}?></b></span>
				<div class="row">
					<div class="column-4 duration">
						Duration : <br>
						<strong><?php if (str_replace('min','',$r["Runtime"]) == "0"){ echo "N/A"; }else{ echo str_replace('min','',$r["Runtime"]); } ?></strong> minutes
					</div>
					<div class="column-4 quality">
						Quality : <br>
						<strong><?=$r["Quality"]?></strong>
					</div>
					<div class="column-4 quality">
						Category : <br>
						<strong><?php if (empty($catoo['name'])){ echo "N/A"; }else{ echo  $catoo['name'];}?></strong>
					</div>
				</div>
				<div class="row home_down_buttons">
					<a href="<?php echo ROOTPATH.'/movies/'.$r['id'].'/'.str_replace(" ", "-",$r['name']).'-'.$r['Year'].'/watching/';?>" class="watch_button"><i class="fa fa-television" aria-hidden="true"></i> Watch now</a>
					<a href="<?php echo ROOTPATH.'/movies/'.$r['id'].'/'.str_replace(" ", "-",$r['name']).'-'.$r['Year'].'/downloading/';?>" class="download_button"><i class="fa fa-download" aria-hidden="true"></i> Download now</a>
					<a href="<?php echo ROOTPATH.'/movies/'.$r['id'].'/'.str_replace(" ", "-",$r['name']).'-'.$r['Year'].'/subtitles/';?>" class="subtitles_button"><i class="fa fa-commenting" aria-hidden="true"></i> Subtitles </a>
				</div>
			</div>
		</a></div>
	</div>
<?php
}
?>	


</div>
<div id="loader-icon" style="display: none;">
	<center><img src="<?=ROOTPATH?>/images/loading.gif" /></center>
</div>
</div>


