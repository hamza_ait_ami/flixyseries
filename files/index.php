<?php 

include("./header.php");
if ($_POST) {
	$sql = "INSERT INTO flixytodo(text) VALUES (:text)";
		                                          
	$stmt = $db->prepare($sql);		                                              
	$stmt->bindParam(':text', $_POST['text'], PDO::PARAM_STR);          
	$stmt->execute(); 		
	header("location:./index.php");
}
$s=$db->prepare("SELECT * FROM `flixyseries`");
$s->execute();
$v=$db->prepare("SELECT * FROM `flixyviewslog`");
$v->execute();
$e=$db->prepare("SELECT * FROM `flixyepisodes`");
$e->execute();
$m=$db->prepare("SELECT * FROM `flixymovies`");
$m->execute();
$c=$db->prepare("SELECT * FROM `flixycats`");
$c->execute();
$shaday = sha1("?vn=3&s=summary&f=JSON&g=daily&sd=".date("d")."&sm=".date("m")."&sy=".date("Y")."&ed=".date("d")."&em=".date("m")."&ey=".date("Y")."&pi=10918661&n=10&t=".time()."&u=hamzalife142536");
$jsonday = file_get_contents("http://api.statcounter.com/stats/?vn=3&s=summary&f=JSON&g=daily&sd=".date("d")."&sm=".date("m")."&sy=".date("Y")."&ed=".date("d")."&em=".date("m")."&ey=".date("Y")."&pi=10918661&n=10&t=".time()."&u=hamzalife&sha1=".$shaday);
$result = json_decode($jsonday, true);

$shamonth = sha1("?vn=3&s=summary&f=JSON&g=weekly&sw=".date("W")."&sy=".date("Y")."&ew=".date("W",time() + (7 * 24 * 60 * 60))."&sy=".date("Y")."&ey=".date("Y")."&pi=10918661&n=10&t=".time()."&u=hamzalife142536");
$jsonmonth = file_get_contents("http://api.statcounter.com/stats/?vn=3&s=summary&f=JSON&g=weekly&sw=".date("W")."&sy=".date("Y")."&ew=".date("W",time() + (7 * 24 * 60 * 60))."&sy=".date("Y")."&ey=".date("Y")."&pi=10918661&n=10&t=".time()."&u=hamzalife&sha1=".$shamonth);

$result2 = json_decode($jsonmonth, true);
?>
<div class="wrapper">
	<div class="row">
		<div class="column-4">
			<div class="stats">
				<i class="fa fa-check-square"></i>
				<span class="Number"><?=$result['sc_data'][0]['page_views']?></span>
				<span class="Text">Visits this Day (<?= date("D")." ".date("d")?>)</span>
			</div>
		</div>
	<div class="column-4">
			<div class="stats">
				<i class="fa fa-user-plus"></i>
				<span class="Number"> <?=$result['sc_data'][0]['unique_visits']?></span>
				<span class="Text">Visitors this Day ( Unique )</span>
			</div>
		</div>
		<div class="column-4">
			<div class="stats">
				<i class="fa fa-files-o"></i>
				<span class="Number"> <?= $c -> rowCount()?></span>
				<span class="Text">Category</span>
			</div>
		</div>
	</div>
	<div class="row"><div class="column-4">
			<div class="stats">
				<i class="fa fa-sort"></i>
				<span class="Number"><?php
$url="http://www.flixyseries.com/";
$xml = simplexml_load_file('http://data.alexa.com/data?cli=10&dat=snbamz&url='.$url);
$rank=isset($xml->SD[1]->POPULARITY)?$xml->SD[1]->POPULARITY->attributes()->TEXT:0;
echo $rank;
				?></span>
				<span class="Text">Alexa Rank</span>
			</div>
		</div>
			<div class="column-4">
			<div class="stats">
				<i class="fa fa-file-video-o"></i>
				<span class="Number"> <?= $s -> rowCount()?></span>
				<span class="Text">Series</span>
			</div>
		</div>
		<div class="column-4">
			<div class="stats">
				<i class="fa fa-film"></i>
				<span class="Number"> <?= $m -> rowCount()?></span>
				<span class="Text">Movies</span>
			</div>
		</div>
	</div>	<div class="row">
			<div class="column-4">
			<div class="stats">
				<i class="fa fa-users"></i>
				<span class="Number"><?=$result2['sc_data'][0]['page_views']?></span>
				<span class="Text">Visits this week (<?= date("W")."th" ?>)</span>
			</div>
		</div>
		<div class="column-4">
			<div class="stats">
				<i class="fa fa-eye"></i>
				<span class="Number"> <?= $v -> rowCount()?></span>
				<span class="Text">Views ( Unique )</span>
			</div>
		</div>
		<div class="column-4">
			<div class="stats">
				<i class="fa fa-video-camera"></i>
				<span class="Number"> <?= $e -> rowCount()?></span>
				<span class="Text">Episodes</span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="column-4">
			<div class="widget to-do">
				<h3><i class="fa fa-calendar-check-o"></i> To do list</h3>
				<ul>
				<?php

					$todolist = $db->prepare("SELECT * FROM flixytodo");
					$todolist->execute();

					while($gtodolist = $todolist->fetch(PDO:: FETCH_ASSOC)){
							echo '<li>'.$gtodolist['text'].' <span style="float:right;" class="record" id="record-',$gtodolist['id'],'"><a href="./ajax.php?type=deletetodo&id=',$gtodolist['id'],'" class="delete"><i class="fa fa-trash"></i></span></a></li>';
					}

				?>
					<li id="result"></li>

				</ul>
				<a class="new-task" >Add a new task</a>
				<form action="index.php" method="post" name="form" class="to-do-form">
					<input type="text" name="text" id="text" placeholder="Write tasks here">
					<button  id="sub" type="submit"  onclick = "sendData()">Submit</button>
				</form>
			</div>
		</div>
		<div class="column-8">
			<div class="widget">
			<center><img src="http://statcounter.com/p10918661/summary/daily-psr-labels-area-last7days.png" /></center>

			</div>
		</div>
	</div>
</div>

</body>
</html>