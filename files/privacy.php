<main>
<div>
	<div class="serie-head">
		<div class="serie-title">
			<h1>Privacy</h1>
		</div>
	</div>
</div>
<div class="wrapper">
	<div class="row">
		<div class="column-12">
			<div class="dmca">

We use cookies to help identify your computer so we can tailor your user experience, track shopping basket contents and remember where you are in the order process.

You can disable any cookies already stored on your computer, but these may stop our website from functioning properly.

The following is strictly necessary in the operation of our website.

<br><b>Flixyseries</b> Will: <br>

Remember what is in your shopping basket
Remember where you are in the order process
Remember that you are logged in and that your session is secure.  You need to be logged in to complete an order.
The following are not Strictly Necessary, but are required to provide you with the best user experience and also to tell us which pages you find most interesting (anonymously).

<br><b>Functional Cookies</b><br>

<br><b>Flixyseries</b> Will:<br>

- Save Last Episode Watched
- Track the pages you visits via Google Analytics

<br><b>Targeting Cookies</b><br>

<br><b>Flixyseries</b> Will:<br>

Allow you to share pages with social networks such as Facebook 
Allow you to share pages via AddToAny


<br><b>Flixyseries</b> will not<br>

- Share any personal information with third parties.
			</div>
		</div>
	</div>
</div>