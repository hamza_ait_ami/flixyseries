<?php

ob_start();
/*echo '<center><div class="success">All The movies & episodes Are UP!! .. if you have any suggestions press <a href="/requestmovie/" style="color:#40C2DF;"> HERE </a> .. We will provide it within 24H MAX!</div></center>';*/

$grid = $db->prepare("SELECT * FROM flixygrid ");
$grid->execute();

$xd =array();
while($data = $grid -> fetch(PDO:: FETCH_ASSOC)) {
   array_push($xd, $data);
}
?>


<div class="wrapper-fluid">
<div class="row home_header">
	<div class="column-6">
		<h2>Latest episodes uploaded</h2>
		<div class="turn_episodes">
			<a href="#" class="turn_episode_left"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
			<a href="#" class="turn_episode_right"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
		</div>
		<div class="home_header_container">
			<ul class="home_header_content">
			<!-- Dert 12 last episodes ibano :) -->
			<?php
				$daysafter = date("Y-m-d",time());
				$episodes = $db->prepare("SELECT * FROM flixyepisodes where openload != '' and air_date <= '$daysafter' order by air_date desc limit 12 ");
				$episodes->execute();
				$counts = $episodes -> rowCount();
				while($gepisodes = $episodes->fetch(PDO:: FETCH_ASSOC)){
				$serie = $db->prepare("SELECT * FROM flixyseries where id = :id");
				$serie->execute(array(':id'=>$gepisodes['serieid']));
				$gserieinfos = $serie->fetch(PDO:: FETCH_ASSOC);
				echo'
					<li>
						<a href="'.ROOTPATH.'/episode/'.$gepisodes['id'].'/'.str_replace(" ", "-",$gserieinfos['name']).'-season-'.$gepisodes['season'].'-episode-'.$gepisodes['epid'].'/">
							<img src="'.ROOTPATH.'/images/series/'.str_replace(" ", "-",$gserieinfos['name']).'.jpg" alt="">
							<h2>'.$gserieinfos['name'].' - Season '.$gepisodes['season'].'</h2>
							<span>Episode '.$gepisodes['epid'].'</span>
						</a>
					</li>
				';
				}
			?>
			</ul>
		</div>
		
	</div>
	<div class="column-6">
		<h2>Top films this month</h2>
		<div class="turn_episodes">
			<a href="#" class="turn_movie_left"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
			<a href="#" class="turn_movie_right"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
		</div>
		<div class="home_header_container">
			<ul class="home_header_content">
			<?php
				$daysbefore = date("Y-m-d",time() - 60*24*60*60);
				$daysafter = date("Y-m-d",time() + 60*24*60*60);
				$movies = $db->prepare("SELECT * FROM flixymovies where openload != '' and Released >= '$daysbefore' and Released <= '$daysafter' order by Rating,Released desc limit 12 ");
				$movies->execute();
				$counts = $movies -> rowCount();
				while($gmovies = $movies->fetch(PDO:: FETCH_ASSOC)){
				echo'
					<li>
						<a href="">
							<img src="'.ROOTPATH.'/images/movies/'.str_replace("?", "_",str_replace(":", "_",str_replace(" ", "-",$gmovies['name']))).'.jpg" alt="">
							<h2>'.$gmovies['name'].' - '.$gmovies['Year'].'</h2>
							<span>'.$gmovies["Quality"].'</span>
						</a>
					</li>
				';
				}
			?>
			</ul>
		</div>
	</div>
</div>
<div class="row movies_boxes">
	<ul class="movies_page">
		<li><a href="#">Latest films</a></li>
		<li><a href="#">Top films this month</a></li>
		<li><a href="#">Top films in all time</a></li>
	</ul>
<?php
$sqls = " SELECT * FROM flixymovies where openload != '' order by Released desc, id desc limit 30";
$qs=$db->prepare($sqls);
$qs->execute();

while ($r=$qs->fetch(PDO::FETCH_ASSOC)) {
?>
	<div class="column-3">
		<div onclick="document.location.href = '<?php echo ROOTPATH.'/movies/'.$r['id'].'/'.str_replace(" ", "-",$r['name']).'-'.$r['Year'].'/';?>'; return false">
			<img src="<?=ROOTPATH.'/images/movies/'.str_replace("?", "_",str_replace(":", "_",str_replace(" ", "-",$r['name'])))?>.jpg" alt="">
			<div class="movie_desc">
				<h1><?=$r['name']." - ".$r['Year']?></h1>
				<p><?php
				echo substr($r["Plot"], 0,200);
				if (strlen($r["Plot"]) >= 200){
					echo " ...";
				}
				?></p>
				<span class="movie_rating"><i class="fa fa-star"></i><b><?=$r["Rating"]?></b></span>
				<div class="row">
					<div class="column-4 duration">
						Duration : <br>
						<strong><?php echo str_replace('min','',$r["Runtime"]); ?></strong> minutes
					</div>
					<div class="column-4 quality">
						Quality : <br>
						<strong><?=$r["Quality"]?></strong>
					</div>
					<div class="column-4 quality">
						Category : <br>
						<strong>Action</strong>
					</div>
				</div>
				<div class="row home_down_buttons">
					<a href="<?php echo ROOTPATH.'/movies/'.$r['id'].'/'.str_replace(" ", "-",$r['name']).'-'.$r['Year'].'/watching/';?>" class="watch_button"><i class="fa fa-television" aria-hidden="true"></i> Watch now</a>
					<a href="<?php echo ROOTPATH.'/movies/'.$r['id'].'/'.str_replace(" ", "-",$r['name']).'-'.$r['Year'].'/downloading/';?>" class="download_button"><i class="fa fa-download" aria-hidden="true"></i> Download now</a>
					<a href="<?php echo ROOTPATH.'/movies/'.$r['id'].'/'.str_replace(" ", "-",$r['name']).'-'.$r['Year'].'/subtitles/';?>" class="subtitles_button"><i class="fa fa-commenting" aria-hidden="true"></i> Subtitles </a>
				</div>
			</div>
		</div>
	</div>
<?php
}
?>	


</div>
<div id="loader-icon" style="display: none;">
	<center><img src="<?=ROOTPATH?>/images/loading.gif" /></center>
</div>
</div>


