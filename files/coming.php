<?php


?>
<main>
<div>
	<div class="serie-head">
		<div class="serie-title">
			<h1>Movies Coming Soon !!</h1>
		</div>
	</div>
</div>
<div class="wrapper">
	<div class="row dispflex">
	<?php

	$movies = $db->prepare("SELECT * FROM flixymovies where trailer != '' order by id desc");
	$movies->execute();
	while($gmovies = $movies->fetch(PDO:: FETCH_ASSOC)){

	echo'
		<div class="column-3">
			<a href="'.ROOTPATH.'/movies/'.$gmovies['id'].'/'.str_replace(" ", "-",$gmovies['name']).'/">
			<div class="latest-ep">
				<div class="ep-image">
					<img src="'.ROOTPATH.'/'.str_replace("./", "/",$gmovies['pic']).'" alt="'.$gmovies['name'].'"></a>
				</div>
				<h2>'.$gmovies['name'].'</h2>
			</div>
			</a>
		</div>';

	}
?>
	</div>
</div>
<?php  ?>
