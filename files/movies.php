<?php

if(@$_GET['id']){

$id = (int) $_GET['id'];
$episode = $db->prepare("SELECT * FROM flixymovies where id = ".$id);
$episode->execute();
$gepisodeinfos = $episode->fetch(PDO:: FETCH_ASSOC);
if(!empty($gepisodeinfos)){



		$sql = "UPDATE flixymovies set views = views + 1 WHERE  id = :id";	                                          

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':id', $id, PDO::PARAM_STR);    

		$stmt->execute(); 
 
?>

<div class="movie_wrapper">
	<div class="movie_head">
		<div class="subtitles_wrapper">
			<div class="subtitles_box">
				<h3>Subtitles</h3>
				<p>Download the movie subtitles so you can link them with the movie</p>
				<table border="1">
					<tr>
						<td width="80%">Subtitle name</td>
						<td>Subtitle duration</td>
						<td>Download</td>
					</tr>
				<?php
				$request  = xmlrpc_encode_request(
				            "LogIn",
				            array('', '', 'en', 'flixyseries')
				        );
				$context  = stream_context_create(
				            array(
				                'http' => array(
				                    'method'  => "POST",
				                    'header'  => "Content-Type: text/xml",
				                    'content' => $request
				                )
				            )
				        );
				$server = 'http://api.opensubtitles.org/xml-rpc'; // api url
				$file = file_get_contents($server, false, $context);
				$response = xmlrpc_decode($file);	
				$request  = xmlrpc_encode_request(
				            "SearchSubtitles",
				            array($response['token'], array(
				   array('imdbid' => str_replace("tt", "", $gepisodeinfos['imdbID']), 'sublanguageid' => 'eng')
				),
				array(
				   'limit' => 5
				))
				        );
				$context  = stream_context_create(
				            array(
				                'http' => array(
				                    'method'  => "POST",
				                    'header'  => "Content-Type: text/xml",
				                    'content' => $request
				                )
				            )
				        );
				$server = 'http://api.opensubtitles.org/xml-rpc'; // api url
				$file = file_get_contents($server, false, $context);
				$response = xmlrpc_decode($file);
				for ($i=0;$i < count($response['data']); $i++){
					echo '
					<tr>
						<td>'.$response['data'][$i]['MovieReleaseName'].'</td>
						<td>'.$response['data'][$i]['SubLastTS'].'</td>
						<td><a href="'.$response['data'][$i]['ZipDownloadLink'].'" target="_blank"><i class="fa fa-download"></i></a></td>
					</tr>';
				}
				?>

				</table>
				<div>Make sure to check if the subtitles duration's and the movie duration are the same</div>
				<span style="float:right;">Powered by opensubtitles.org</span>
				<a href="#" class="close_subtitles"><i class="fa fa-close"></i></a>
			</div>
		</div>
		<div class="serie_head_buttons">
			<?php
				if (@isset($_COOKIE['mylist'])){
					$cookie = $_COOKIE['mylist'];
					$savedmylist = json_decode($cookie, true);
					if (in_array(array($id, 1), $savedmylist)) {
						echo '<a href="#" class="add_to_mylist" style="background-color:green;" id="1" kappa=""><i class="fa fa-heart" aria-hidden="true"></i> Already in your list</a>';
					}else{
						echo '<a href="#" class="add_to_mylist" id="1" kappa="'.$id.'"><i class="fa fa-heart" aria-hidden="true"></i> Add to my list</a>';
					}
				}else{
						echo '<a href="#" class="add_to_mylist" id="1" kappa="'.$id.'"><i class="fa fa-heart" aria-hidden="true"></i> Add to my list</a>';
				}
			?>
			<a href="/report/<?= $gepisodeinfos['id']?>/" class="add_to_mylist report_clip"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Report</a>
			<a href="./downloading/" ><i class="fa fa-download" aria-hidden="true"></i> Download</a>
			<a href="#" class="subtitles_b"><i class="fa fa-commenting" aria-hidden="true"></i> Subtitles</a>
		</div>
		<div class="row">
			<div class="column-4">
			<?php
			if (!empty($gepisodeinfos["trailer"])){
			?>
				<iframe width="100%" src="<?=$gepisodeinfos["trailer"]?>" frameborder="0" allowfullscreen></iframe>
			<?php
			}else{
			?>
				<img src="<?=ROOTPATH.'/images/movies/'.str_replace("/", "_",str_replace("?", "_",str_replace(":", "_",str_replace(" ", "-",$gepisodeinfos['name']))))?>.jpg" alt="">
			<?php
			}
			?>
			</div>
			<div class="column-8">
				<h1><?= $gepisodeinfos['name'];?></h1>
				
				<p><?php echo $gepisodeinfos["Plot"]; ?></p>
				<div class="more_movie_infos">
					<span>Quality : <b><?php echo str_replace('min','',$gepisodeinfos["Quality"]); ?></b></span>
					<span>Duration : <b><?php echo str_replace('min','',$gepisodeinfos["Runtime"]); ?> <i>mins</i></b></span>
					<span>imDB Rating : <b><?php echo $gepisodeinfos["Rating"]; ?></b></span>
					<span>Release Date :  <b><?= $gepisodeinfos['Released'];?></b></span>
					
				</div>
		
				<div>
					<style>.stButton * {box-sizing: content-box;}</style>
					<span class='st_facebook_hcount' displayText='Facebook'></span>
					<span class='st_twitter_hcount' displayText='Tweet'></span>
					<span class='st_email_hcount' displayText='Email'></span>
				</div>
				
			</div>
		</div>
	</div>

		<?php


	if (@$_GET['method'] == "watching")	{	
		$file_headers = @get_headers($url);
			if ($gepisodeinfos['openload'] == ''){
				echo '<iframe width="100%" height="430"  src="'.$gepisodeinfos['trailer'].'" frameborder="0" allowfullscreen></iframe>';
			}else {
				echo '<div class="serie-player active"><iframe src="https://openload.co/embed/'.$gepisodeinfos['openload'].'/" scrolling="no" frameborder="0" width="100%" height="430" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"></iframe></div>';
			}

	}elseif (@$_GET['method'] == "downloading")	{
		if($_POST){
			$openloadvid = file_get_contents("https://api.openload.co/1/file/dl?file=".$gepisodeinfos['openload']."&ticket=".$_POST['ticket']."&captcha_response=".$_POST['capatcha']."");
			$getopenloadvid =json_decode($openloadvid, true);
			if ($getopenloadvid['status'] == 200){
				echo '<center><div class="success">The download will start automatically .. if not please click <a href="'.$getopenloadvid['result']['url'].'" style="color:#40C2DF;"> HERE </a></div><meta http-equiv="refresh" content="2;url='.$getopenloadvid['result']['url'].'" /></center>';
			}else{
				echo '<center><div class="error">Capatcha code is wrong '.$getopenloadvid['status'].'</div><meta http-equiv="refresh" content="3;url=../downloading/" /></center>';

			}
		}else{ 

		$openloadvid = file_get_contents("https://api.openload.co/1/file/dlticket?file=".$gepisodeinfos['openload']."");
		$getopenloadvid =json_decode($openloadvid, true);
		//echo'<e>'.print_r($getopenloadvid).'</e>';
		echo "
		
		<div class='movie_head'>
		<br>
		<h2>Please enter the code shown in the picture below ( Valid for 30mins ) </h2>
		<br>
			<img src='".$getopenloadvid['result']['captcha_url']."'><br>
			<form action='../downloading/' method='post'>
				<input type='text' name='capatcha' />
				<input type='hidden' name='ticket' value='".$getopenloadvid['result']['ticket']."' />
				<button>Generate Link </button>
			</form><br><br>
		</div>
		";
		}
	}elseif(@$_GET['method'] == "actors"){
	echo '<div class="row">';
	$cats = $db->prepare("SELECT * FROM flixyactors_movies where movie_id = '".$id."'");
	$cats->execute();
	while($gcats = $cats->fetch(PDO:: FETCH_ASSOC)){
	$cat = $db->prepare("SELECT * FROM flixyactors where person_id = :id");
	$cat->execute(array(':id'=>$gcats['actor_id']));
	$gacts = $cat->fetch(PDO:: FETCH_ASSOC);
	if ($gacts['gender'] == 1){
		$gender = "actress";
	}else{
		$gender = "actor";
	}
	echo'
		<div class="column-3">
			<a href="'.ROOTPATH.'/'.$gender.'/'.$gacts['person_id'].'/'.str_replace(" ", "-",$gacts['name']).'/">
			<div class="latest-ep">
				<div class="ep-image">
					<img src="'.ROOTPATH.'/images/actors/'.str_replace(" ", "-",$gacts['name']).'.jpg" alt="'.$gacts['name'].'"></a>
				</div>
				<h2>'.$gacts['name'].'</h2>
			</div>
			</a>
		</div>';
	}
	echo'</div>';
	}elseif(@$_GET['method'] == "subtitles"){
		?>
		<div class="subtitles_page">
			<h3>Subtitles</h3>
			<p>Download the movie subtitles so you can link them with the movie</p>
			<table border="1">
				<tr>
					<td width="80%">Subtitle name</td>
					<td>Subtitle duration</td>
					<td>Download</td>
				</tr>
			<?php
			$request  = xmlrpc_encode_request(
			            "LogIn",
			            array('', '', 'en', 'flixyseries')
			        );
			$context  = stream_context_create(
			            array(
			                'http' => array(
			                    'method'  => "POST",
			                    'header'  => "Content-Type: text/xml",
			                    'content' => $request
			                )
			            )
			        );
			$server = 'http://api.opensubtitles.org/xml-rpc'; // api url
			$file = file_get_contents($server, false, $context);
			$response = xmlrpc_decode($file);	
			$request  = xmlrpc_encode_request(
			            "SearchSubtitles",
			            array($response['token'], array(
			   array('imdbid' => str_replace("tt", "", $gepisodeinfos['imdbID']), 'sublanguageid' => 'eng')
			),
			array(
			   'limit' => 30
			))
			        );
			$context  = stream_context_create(
			            array(
			                'http' => array(
			                    'method'  => "POST",
			                    'header'  => "Content-Type: text/xml",
			                    'content' => $request
			                )
			            )
			        );
			$server = 'http://api.opensubtitles.org/xml-rpc'; // api url
			$file = file_get_contents($server, false, $context);
			$response = xmlrpc_decode($file);
			for ($i=0;$i < count($response['data']); $i++){
				if (!empty($response['data'][$i]['MovieReleaseName'])){
					echo '
					<tr>
						<td>'.$response['data'][$i]['MovieReleaseName'].'</td>
						<td>'.$response['data'][$i]['SubLastTS'].'</td>
						<td><a href="'.$response['data'][$i]['ZipDownloadLink'].'" target="_blank"><i class="fa fa-download"></i></a></td>
					</tr>';
				}
			}
			?>

			</table>
			<div>Make sure to check if the subtitles duration's and the movie duration are the same</div>
			<span style="float:right;">Powered by opensubtitles.org</span>			
		</div>
		<?php
	}else{
			if ($gepisodeinfos['openload'] == ''){
				echo '<iframe width="100%" height="430"  src="'.$gepisodeinfos['trailer'].'" frameborder="0" allowfullscreen></iframe>';
			}else{
				//if (file_exists(ROOTPATH."/images/background/".str_replace("/", "_",str_replace("?", "_",str_replace(":", "_",str_replace(" ", "-",$gepisodeinfos["name"])))).".jpg")){
					$splash =  ROOTPATH.'/images/background/'.str_replace("/", "_",str_replace("?", "_",str_replace(":", "_",str_replace(" ", "-",$gepisodeinfos['name'])))).'.jpg';
				/*}else{
					$splash = ROOTPATH."/images/splash.jpg";
				}*/
			echo '
				<div class="serie-player active">
				<a href="./watching/" id="show_iframe" class="main_player" style="background-image:url('.$splash.');">
				<div id="panel1">
						
						<span class="play_button"><i class="fa fa-play" aria-hidden="true"></i></span>
				</div>
			</a>';
			}	

	}


		?>
		</div>
		<?php
			
		?>
		<div class="movie_under_player">
			<div class="column-5">
				<h3>Also Known As :</h3>
				<?php
				if (is_array(unserialize($gepisodeinfos['alternative_titles']))){
					foreach (unserialize($gepisodeinfos['alternative_titles']) as $key => $value) {
						echo " <img src='".ROOTPATH."/images/flags/".strtolower($value['iso_3166_1']).".png' /> : ".$value['title']."<br>";
					}
				}
				?>
			</div>
			<div class="column-7">
				<div class="serie_actors">
					<h3>Actors :</h3>
					<ul>
					<?php
					$cats = $db->prepare("SELECT * FROM flixyactors_movies where movie_id = '".$id."' limit 5");
					$cats->execute();
					while($gcats = $cats->fetch(PDO:: FETCH_ASSOC)){
					$acts = $db->prepare("SELECT * FROM flixyactors where person_id = '".$gcats['actor_id']."'");
					$acts->execute();
					$gacts = $acts->fetch(PDO:: FETCH_ASSOC);
					if ($gacts['gender'] == 1){
						$gender = "actress";
					}else{
						$gender = "actor";
					}
					?>
					<?php
					echo '
				    	<li><a href="'.ROOTPATH.'/'.$gender.'/'.$gacts['person_id'].'/'.str_replace(" ", "-",$gacts['name']).'/"><img src="'.ROOTPATH.'/images/actors/'.str_replace(" ", "-",$gacts['name']).'.jpg" title="'.$gacts['name'].'"></a></li>
					';
					}
					?>
						<li class="last-picture"><a href="./actors/" title="More actors">...</a></li>
					</ul>
				</div>
			</div>
			
		</div>

				
			<h2 class="other_movies">Other movies you will like : </h2>		
			<div class="row dispflex movies_boxes movies_suggestions">
	<?php 
	$movies = $db->prepare("SELECT * FROM flixymovies where id != $id and openload !='' order by RAND() limit 5");
	$movies->execute();
	while($gmovies = $movies->fetch(PDO:: FETCH_ASSOC)){
	$cats = $db->prepare("SELECT * FROM flixycats_movies where id_movie = '".$gmovies['id']."' limit 1");
	$cats->execute();
	$cato = $cats->fetch(PDO::FETCH_ASSOC);
	$catss = $db->prepare("SELECT * FROM flixycats where id = '".$cato['id_cat']."' limit 1");
	$catss->execute();
	$catoo = $catss->fetch(PDO::FETCH_ASSOC);
	?>
	<div class="column-3">
		<div><a href="<?=ROOTPATH.'/movies/'.$gmovies['id'].'/'.str_replace(" ", "-",$gmovies['name']).'-'.$gmovies['Year'].'/';?>">
			<img src="<?=ROOTPATH.'/images/movies/'.str_replace("/", "_",str_replace("?", "_",str_replace(":", "_",str_replace(" ", "-",$gmovies['name']))))?>.jpg" alt="">
			<div class="movie_desc" style="background-image: url('<?=ROOTPATH.'/images/background/'.str_replace("/", "_",str_replace("?", "_",str_replace(":", "_",str_replace(" ", "-",$gmovies['name']))))?>.jpg');">
				<h1><?=$gmovies['name']." - ".$gmovies['Year']?></h1>
				<p><?php
				echo substr($gmovies["Plot"], 0,200);
				if (strlen($gmovies["Plot"]) >= 200){
					echo "...";
				}
				?></p>
				<span class="movie_rating"><i class="fa fa-star"></i><b><?php if ($gmovies["Rating"] == 0){echo "-";}else{echo $gmovies["Rating"];}?></b></span>
				<div class="row">
					<div class="column-4 duration">
						Duration : <br>
						<strong><?php if (str_replace('min','',$gmovies["Runtime"]) == "0"){echo "N/A"; }else{ echo str_replace('min','',$gmovies["Runtime"]); } ?></strong> minutes
					</div>
					<div class="column-4 quality">
						Quality : <br>
						<strong><?=$gmovies["Quality"]?></strong>
					</div>
					<div class="column-4 quality">
						Category : <br>
						<strong><?php if (empty($catoo['name'])){ echo "N/A"; }else{ echo  $catoo['name'];}?></strong>
					</div>
				</div>
				<div class="row home_down_buttons">
					<a href="<?php echo ROOTPATH.'/movies/'.$gmovies['id'].'/'.str_replace(" ", "-",$gmovies['name']).'-'.$gmovies['Year'].'/watching/';?>" class="watch_button"><i class="fa fa-television" aria-hidden="true"></i> Watch now</a>
					<a href="<?php echo ROOTPATH.'/movies/'.$gmovies['id'].'/'.str_replace(" ", "-",$gmovies['name']).'-'.$gmovies['Year'].'/downloading/';?>" class="download_button"><i class="fa fa-download" aria-hidden="true"></i> Download now</a>
					<a href="<?php echo ROOTPATH.'/movies/'.$gmovies['id'].'/'.str_replace(" ", "-",$gmovies['name']).'-'.$gmovies['Year'].'/subtitles/';?>" class="subtitles_button"><i class="fa fa-commenting" aria-hidden="true"></i> Subtitles </a>
				</div>
			</a></div>
		</div>
	</div>
	<?php

	}
?>
			</div>
</div>
<?php

}else{

	include(ROOTPATH."/404.php");
}

}else{
?>

<div>
	
	<?php

	if (@$_GET['method'] == "comingsoon"){
			$title = "Movies Coming Soon";
	}elseif (@$_GET['method'] == "on2016"){
			$title = "2016 Movies";
	}elseif (@$_GET['method'] == "on2017"){
			$title = "2017 Movies";
	}else{
			$title = "All Movies";
	}

	?>
<div class="main request">
	<h1><?=$title?></h1>
	<!--<div class="series_type">
		<a href="<?=ROOTPATH?>/movies/">All Movies</a>
		<a href="<?=ROOTPATH?>/movies/comingsoon/">Coming Soon!</a>
	</div>-->
</div>
<div class="wrapper">
	<div class="row dispflex">
	<?php
	if( @$_GET['page'] ==  "all") {	
	$movies = $db->prepare("SELECT * FROM flixymovies where openload != '' order by id desc");
	$movies->execute();
	while($gmovies = $movies->fetch(PDO:: FETCH_ASSOC)){

	echo'
		<div class="column-3">
			<a href="'.ROOTPATH.'/movies/'.$gmovies['id'].'/'.str_replace(" ", "-",$gmovies['name']).'-'.$gmovies['Year'].'/">
			<div class="latest-ep">
				<div class="ep-image">
					<ul class="search_cat">
						<li>'.$gmovies['Quality'].'</li>
					</ul>
					<img src="'.ROOTPATH.'/images/movies/'.str_replace("?", "_",str_replace(":", "_",str_replace(" ", "-",$gmovies['name']))).'.jpg" alt="'.$gmovies['name'].'"></a>
				</div>
				<h2>'.$gmovies['name'].'</h2>
			</div>
			</a>
		</div>';

	}
	}elseif (@$_GET['method'] == "comingsoon"){	
	$movies = $db->prepare("SELECT * FROM flixymovies where openload = '' order by Released desc");
	$movies->execute();
	while($gmovies = $movies->fetch(PDO:: FETCH_ASSOC)){

	echo'
		<div class="column-3">
			<a href="'.ROOTPATH.'/movies/'.$gmovies['id'].'/'.str_replace(" ", "-",$gmovies['name']).'-'.$gmovies['Year'].'/">
			<div class="latest-ep">
				<div class="ep-image">
					<ul class="search_cat">
						<li>'.$gmovies['Released'].'</li>
					</ul>
					<img src="'.ROOTPATH.'/images/movies/'.str_replace("?", "_",str_replace(":", "_",str_replace(" ", "-",$gmovies['name']))).'.jpg" alt="'.$gmovies['name'].'"></a>
				</div>
				<h2>'.$gmovies['name'].'</h2>
			</div>
			</a>
		</div>';
	}
}elseif (@$_GET['method'] == "on2017"){
	$movies = $db->prepare("SELECT * FROM flixymovies where Year = '2017' and openload = '' order by id desc");
	$movies->execute();
	while($gmovies = $movies->fetch(PDO:: FETCH_ASSOC)){


	echo'
		<div class="column-3">
			<a href="'.ROOTPATH.'/movies/'.$gmovies['id'].'/'.str_replace(" ", "-",$gmovies['name']).'-'.$gmovies['Year'].'/">
			<div class="latest-ep">
				<div class="ep-image">
					<ul class="search_cat">
						<li>'.$gmovies['Quality'].'</li>
					</ul>
					<img src="'.ROOTPATH.'/images/movies/'.str_replace("?", "_",str_replace(":", "_",str_replace(" ", "-",$gmovies['name']))).'.jpg" alt="'.$gmovies['name'].'"></a>
				</div>
				<h2>'.$gmovies['name'].'</h2>
			</div>
			</a>
		</div>';
	}
	}elseif (@$_GET['method'] == "on2016"){
	$movies = $db->prepare("SELECT * FROM flixymovies where Year = '2016' and openload = '' order by id desc");
	$movies->execute();
	while($gmovies = $movies->fetch(PDO:: FETCH_ASSOC)){


	echo'
		<div class="column-3">
			<a href="'.ROOTPATH.'/movies/'.$gmovies['id'].'/'.str_replace(" ", "-",$gmovies['name']).'-'.$gmovies['Year'].'/">
			<div class="latest-ep">
				<div class="ep-image">
					<ul class="search_cat">
						<li>'.$gmovies['Quality'].'</li>
					</ul>
					<img src="'.ROOTPATH.'/images/movies/'.str_replace("?", "_",str_replace(":", "_",str_replace(" ", "-",$gmovies['name']))).'.jpg" alt="'.$gmovies['name'].'"></a>
				</div>
				<h2>'.$gmovies['name'].'</h2>
			</div>
			</a>
		</div>';
	}
	}else{
	$rec_limit = 8;
	    $c=$db->prepare("SELECT * FROM `flixymovies`");
	$c->execute();
    $rec_count =  $c -> rowCount();
      $pages = $rec_count / $rec_limit ;
	if( @$_GET['page'] ) {
            $page = @$_GET['page'];
            $next = @$_GET['page'] + 1;
            $offset = $rec_limit * $page ;
    }else {
            $page = 0;
            $next = @$_GET['page'] + 1;
            $offset = $rec_limit * $page ;
    }

    
    $left_rec = $rec_count - ($page * $rec_limit);

	$movies = $db->prepare("SELECT * FROM flixymovies where openload != '' order by Year desc, id desc LIMIT $offset, $rec_limit");
	$movies->execute();
	while($gmovies = $movies->fetch(PDO:: FETCH_ASSOC)){

	echo'
		<div class="column-3">
			<a href="'.ROOTPATH.'/movies/'.$gmovies['id'].'/'.str_replace(" ", "-",$gmovies['name']).'-'.$gmovies['Year'].'/">
			<div class="latest-ep">
				<div class="ep-image">
					<ul class="search_cat">
						<li>'.$gmovies['Quality'].'</li>
					</ul>
					<img src="'.ROOTPATH.'/images/movies/'.str_replace("?", "_",str_replace(":", "_",str_replace(" ", "-",$gmovies['name']))).'.jpg" alt="'.$gmovies['name'].'"></a>
				</div>
				<h2>'.$gmovies['name'].'</h2>
			</div>
			</a>
		</div>';

	}
	
		if( $left_rec < $rec_limit ) {
            $last = $page - 1;
            echo "<div class='page'><span class='pages'><a href = '".ROOTPATH."/movies/page/$last/''><<</a></span><span class='pages'>".$page."</span><span class='pages'><a href = '".ROOTPATH."/movies/page/all/''>All</a></span></div>";
         }elseif( $page == 0 ) {
            echo " <div class='page'><span class='pages'>".$page."</span><span class='pages'><a href = '".ROOTPATH."/movies/page/$next/'>>></a> </span><span class='pages'><a href = '".ROOTPATH."/movies/page/all/''>All</a></span></div>";
         }elseif( $page > 0 ) {
            $last = $page - 1;      
            echo "<div class='page'><span class='pages'><a href = '".ROOTPATH."/movies/page/$last/'><<</a> </span><span class='pages'>";
            echo $page ;
            echo " </span><span class='pages'><a href = '".ROOTPATH."/movies/page/$next/'>>></a> </span><span class='pages'><a href = '".ROOTPATH."/movies/page/all/''>All</a></span></div>";
         }
     }
     }
?>
	</div>
</div>
<?php  ?>
