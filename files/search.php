
<?php
if (@$_GET["method"] == 'suggest'){
	@session_start();
	$Kappa = $_POST['seriesuggest'];
	$sql = "UPDATE flixysuggest set seggestion = :seggestion WHERE time = :do";

	$stmt = $db->prepare($sql);                                             
	$stmt->bindParam(':seggestion',$Kappa , PDO::PARAM_STR);          
	$stmt->bindParam(':do',$_SESSION['suggest_time'] , PDO::PARAM_STR);          
	$stmt->execute(); 		
	exit; 	
 }elseif (@$_GET["method"] == 'update'){
	@session_start();
	$Kappa = $_POST['email'];
	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    	$ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    	$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
    	$ip = $_SERVER['REMOTE_ADDR'];
	}

	if (isset($_SESSION['suggest_time'])){
		$sql = "UPDATE flixysuggest set email = :email WHERE time = :do";
			                                          
		$stmt = $db->prepare($sql);	                                          		                                              
		$stmt->bindParam(':do',$_SESSION['suggest_time'] , PDO::PARAM_STR);   
		$stmt->bindParam(':email', $Kappa , PDO::PARAM_STR);          
		$stmt->execute(); 

	    unset($_SESSION['suggest_time']);
	    session_unset();
	    session_destroy();
	}
 	exit;	
}

if ($_POST){
	@session_start();
	$k = @trim(preg_replace('/\d+/u','',str_replace(['episode','season'], "",$_POST['Kappa'])));
	$_SESSION['suggest_time'] = time();
	$sql = "INSERT INTO flixysuggest(ip,search,time) VALUES (
		            :ip ,
		            :search ,
		            :time

		   )";
				
	if(!empty($k)){
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		    $ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
		    $ip = $_SERVER['REMOTE_ADDR'];
		}	                                          
		$stmt = $db->prepare($sql);                                              
		$stmt->bindParam(':ip', $ip, PDO::PARAM_STR);       
		$stmt->bindParam(':search',$k , PDO::PARAM_STR);          
		$stmt->bindParam(':time',$_SESSION['suggest_time'] , PDO::PARAM_STR);          
		$stmt->execute(); 
	}

	$sqls = " SELECT * FROM flixymovies WHERE ( alternative_titles LIKE :keyword ) OR ( name LIKE :keyword )";
	$qs=$db->prepare($sqls);
	$qs->bindValue(':keyword','%'.$k.'%');
	$qs->execute();
	/*
	$k1 = strtolower(htmlentities($k));
	$value = explode(' ' , $k1);
	$i = 0;
	$len = count($value);
	$q = 'WHERE ';
	$z = 1;
	foreach ($value as $val) {
		if ($val != "" and $val != " " and $val != "a" and $val != "of" and $val != "the"){
		    $q .= "name LIKE :keyword$i ";
		    if ($i != $len - 1) {
		        $q .= ' OR ';
		        $z ++;    
		    }
	    }
	$i++;   
	}
	$sql1 = " SELECT * FROM flixyseries $q";
	$qa=$db->prepare($sql1);
	$j = 0;
	foreach ($value as $val) {
		if ($val != "" and $val != " " and $val != "a" and $val != "of" and $val != "the"){

			$qa->bindValue(':keyword'.($j),'%'.$value[$j].'%');
		}
	$j++;
	}
	$qa->execute();
	*/
	$sql1 = " SELECT * FROM flixyseries WHERE name LIKE :keyword or alternative_titles  LIKE :keyword ";
	$qa=$db->prepare($sql1);
	$qa->bindValue(':keyword','%'.$k.'%');
	$qa->execute();

	if ( $qa -> rowCount() != 0 or $qs -> rowCount() != 0){
	echo '<div class="row search_results">';
	while ($r=$qa->fetch(PDO::FETCH_ASSOC)) {
	?>
	<div class="column-3">
		<a href="<?=ROOTPATH."/serie/".$r['id']."/".str_replace(" ", "-",$r['name'])?>/">
			<div class="latest-ep">
				<div class="ep-image">
					<ul class="search_cat">
						<li>Serie</li>
					</ul>
					<img src="<?=ROOTPATH.'/images/series/'.str_replace(" ", "-",$r['name'])?>.jpg" alt="">
				</div>
				<h2><?=$r['name']?></h2>
			</div>
		</a>
	</div>
	<?php
	}
	while ($r=$qs->fetch(PDO::FETCH_ASSOC)) {
	echo'
		<div class="column-3">
			<a href="'.ROOTPATH.'/movies/'.$r['id'].'/'.str_replace(" ", "-",$r['name']).'-'.$r['Year'].'/">
				<div class="latest-ep">
					<div class="ep-image">
						<ul class="search_cat">
							<li>Movie</li>
							';
							if ($r['openload'] != ""){
								echo '<li>'.$r['Quality'].'</li>';
							}else{
								echo '<li style="background:#f53838;">Coming Soon</li>';
							}
							
							echo '
						</ul>
						<img src="'.ROOTPATH.'/images/movies/'.str_replace("?", "_",str_replace(":", "_",str_replace(" ", "-",$r['name']))).'.jpg" alt="'.$r['name'].'"></a>
					</div>
					<h2>'.$r['name'].'</h2>
				</div>
			</a>
		</div>';	
	}
}else{
?>
<div class="main request">
		<h1>Results Found : 0 </h1>
		<div class="wrapper search-nf">
			<div class="row">
				<div class="column-12" style="color:black;">
					Sorry, we couldn't find what you are looking for, but maybe :
					<ul>
						<li>You miss spelled the name of the serie you are looking for</li>
						<li>You are looking for a film not a serie, then you can research from <a href="#">here</a></li>
						<li>Our website doesn't provide the serie you are looking for then you can suggest its name from here

							<div class="search-nf-input">
								<form method="post" id="reg-search">
								<input type="text" placeholder="Name of the Serie" name="seriesuggest" id="seriesuggest" value="<?=$k?>" style="color:black;">
								<button id="gosearch">Submit</button></form>
							</div>
							We will try to provide it in the next 24 hours
						</li>
					</ul>
				</div>
			</div>
		</div>
</div>
<?php
}
}else{
?>
<div class="main request">
<br><br><br><br>
	<center><div class="row mylist" style="border:1px solid black; padding: 8px; width: 50%; background: #fcfcfc;">
	<h1>Search</h1>
		<form name='frmSearch' method='post' action='<?=ROOTPATH?>/search/'>
				<input type="search" name='Kappa' style="border:1px solid black;" placeholder="Type the name of the serie or movie"><br>
				<button>GO</button>
		</form>
	</div></center>
</div>
<?php
}
?>



