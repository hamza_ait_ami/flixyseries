<?php
$id = (int) $_GET['id'];

$cat = $db->prepare("SELECT * FROM flixyactors where person_id = :id");
$cat->execute(array(':id'=>$id));
$gcatinfos = $cat->fetch(PDO:: FETCH_ASSOC);
if(!empty($gcatinfos)){
?>


	<div class="row">
	<?php
	$cats = $db->prepare("SELECT * FROM flixyactors_series where actor_id = '".$id."'");
	$cats->execute();
	while($gcats = $cats->fetch(PDO:: FETCH_ASSOC)){

	$serie = $db->prepare("SELECT * FROM flixyseries where id = :id");
	$serie->execute(array(':id'=>$gcats['serie_id']));
	$gserieinfos = $serie->fetch(PDO:: FETCH_ASSOC);

	echo'
		<div class="column-3">
			<a href="'.ROOTPATH.'/serie/'.$gserieinfos['id'].'/'.str_replace(" ", "-",$gserieinfos['name']).'/">
			<div class="latest-ep">
				<div class="ep-image">
					<img src="'.ROOTPATH.'/images/series/'.str_replace(" ", "-",$gserieinfos['name']).'.jpg" alt="'.$gserieinfos['name'].'"></a>
				</div>
				<h2><a href="'.ROOTPATH.'/serie/'.$gserieinfos['id'].'/'.str_replace(" ", "-",$gserieinfos['name']).'/">'.$gserieinfos['name'].'</a></h2>
			</div>
			</a>
		</div>';
		}	

	$cats = $db->prepare("SELECT * FROM flixyactors_movies where actor_id = '".$id."'");
	$cats->execute();
	while($gcats = $cats->fetch(PDO:: FETCH_ASSOC)){
	$movies = $db->prepare("SELECT * FROM flixymovies where id = :id");
	$movies->execute(array(':id'=>$gcats['movie_id']));
	$gmovies = $movies->fetch(PDO:: FETCH_ASSOC);

	echo'
		<div class="column-3">
			<a href="'.ROOTPATH.'/movies/'.$gmovies['id'].'/'.str_replace(" ", "-",$gmovies['name']).'-'.$gmovies['Year'].'/">
			<div class="latest-ep">
				<div class="ep-image">
					<ul class="search_cat">
						<li>'.$gmovies['Quality'].'</li>
					</ul>
					<img src="'.ROOTPATH.'/images/movies/'.str_replace("?", "_",str_replace(":", "_",str_replace(" ", "-",$gmovies['name']))).'.jpg" alt="'.$gmovies['name'].'"></a>
				</div>
				<h2>'.$gmovies['name'].'</h2>
			</div>
			</a>
		</div>';

	}
?>


	</div>
</div>
<?php

}else{

	include("/404.php");
}

?>