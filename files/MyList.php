<div class="main">
	<h1>My List</h1>
	<div class="row mylist">
	<?php 
	$m = 0;
	$s = 0;
	if (@isset($_COOKIE['mylist'])){

			$cookie = $_COOKIE['mylist'];
			$savedmylist = json_decode($cookie, true);
			//print_r($savedmylist );
			foreach ($savedmylist as $key => $val){
				foreach ($val as $xd => $valu){
						if (!empty($valu) and $xd == 1){
							$m++;
						}
				}
			}
			foreach ($savedmylist as $key => $val){
				foreach ($val as $xd => $valu){
						if (!empty($valu) and $xd == 0){
							$s++;
						}
				}
			}
	}
	?>
		<div class="column-6">
			<h2>My Movies</h2>
			<?php
			if($m > 0){
				foreach ($savedmylist as $key => $val){
					foreach ($val as $xd => $valu){
						if (!empty($valu) and $xd == 1){
							$movie = $db->prepare("SELECT * FROM flixymovies where id = ".$val[0]);
							$movie->execute();
							$gmovies = $movie->fetch(PDO:: FETCH_ASSOC);
							echo '<a href="'.ROOTPATH.'/movies/'.$gmovies['id'].'/'.str_replace(" ", "-",$gmovies['name']).'/"><img src="'.ROOTPATH.'/images/movies/'.str_replace("?", "_",str_replace(":", "_",str_replace(" ", "-",$gmovies['name']))).'.jpg" alt="'.$gmovies['name'].'"></a>';
						}
					}
				}
			}else{
						echo "<p>You don't have any movie in your list</p>";
			}
			?>
		</div>
		<div class="column-6">
			<h2>My Series</h2>
			<?php
			if($s > 0){
				foreach ($savedmylist as $key => $val){
					foreach ($val as $xd => $valu){
						if (!empty($valu) and $val[1] == 0){
							$serie = $db->prepare("SELECT * FROM flixyseries where id = ".$val[0]);
							$serie->execute();
							$gseries = $serie->fetch(PDO:: FETCH_ASSOC);
							echo '<a href="'.ROOTPATH.'/serie/'.$gseries['id'].'/'.str_replace(" ", "-",$gseries['name']).'/"><img src="'.ROOTPATH.'/images/series/'.str_replace(" ", "-",$gseries['name']).'.jpg" alt="'.$gseries['name'].'"></a>';
						}
					}
				}
			}else{
						echo "<p>You don't have any serie in your list</p>";
			}
			?>
		</div>
	</div>
</div>
