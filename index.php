<?php

# For : FlixySeries.com © 2016
# Index File

require_once("./flixy.php");

if (@$_GET["type"] != "player"){
	GetHead();
}
switch (@$_GET["type"]){

	case"index":

		include("./files/home.php");

	break;
	case"serie":

		include("./files/serie.php");

	break;	
	case"anime":

		include("./files/anime_page.php");

	break;	
	case"search":

		include("./files/search.php");

	break;
	case"actors":

		include("./files/actors.php");

	break;
	case"actor":
	case"actress":

		include("./files/actor.php");

	break;
	case"download":

		include("./files/download.php");

	break;
	case"episode":

		include("./files/episode.php");

	break;
	case"report":

		include("./files/report.php");

	break;
	case"reportepisode":

		include("./files/reportepisode.php");

	break;
	case"series":

		include("./files/series.php");

	break;
	case"category":

		include("./files/category.php");

	break;
	case"genre":

		include("./files/genres.php");

	break;
	case"wweshows":

		include("./files/wweshows.php");

	break;
	case"animes":

		include("./files/anime.php");

	break;
	case"movies":

		include("./files/movies.php");

	break;	
	case"calendar":

		include("./files/calendar.php");

	break;
	case"contact":

		include("./files/contact.php");

	break;
	case"dmca":

		include("./files/dmca.php");

	break;	
	case"copa-america-2016":

		include("./files/copa.php");

	break;
	case"player":

		include("./files/player.php");

	break;
	case"privacy":

		include("./files/privacy.php");

	break;
	case"subtitles":

		include("./files/subtitles.php");

	break;
	case"post":

		include("./files/post.php");

	break;
	case"blog":

		include("./files/blog.php");

	break;	
	case"coming":

		include("./files/coming.php");

	break;
	case"requestmovie":

		include("./files/Request_movie.php");

	break;
	case"requestserie":

		include("./files/Request_serie.php");

	break;
	case"mylist":

		include("./files/MyList.php");

	break;
	case"store":

		include("./files/store.php");

	break;
	default:

		include("./files/home.php");

	break;
}
require_once("./files/footer.php");