-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Dim 16 Juillet 2017 à 12:51
-- Version du serveur :  10.1.21-MariaDB
-- Version de PHP :  5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `flixyseries`
--

-- --------------------------------------------------------

--
-- Structure de la table `flixyanimes`
--

CREATE TABLE `flixyanimes` (
  `id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `description` text NOT NULL,
  `keywords` text NOT NULL,
  `name` varchar(255) NOT NULL,
  `japname` varchar(255) NOT NULL,
  `pic` varchar(255) NOT NULL,
  `date` int(11) NOT NULL,
  `background` varchar(255) NOT NULL,
  `views` int(11) NOT NULL,
  `seasons` int(11) NOT NULL DEFAULT '1',
  `category` int(11) NOT NULL,
  `trailer` int(11) NOT NULL DEFAULT '0',
  `trailerembed` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `flixyblog`
--

CREATE TABLE `flixyblog` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` longtext NOT NULL,
  `time` int(11) NOT NULL,
  `img` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `flixycats`
--

CREATE TABLE `flixycats` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `flixycats`
--

INSERT INTO `flixycats` (`id`, `name`, `img`, `color`) VALUES
(12, 'Adventure', '', ''),
(14, 'Fantasy', '', ''),
(16, 'Animation', '', ''),
(18, 'Drama', '', ''),
(27, 'Horror', '', ''),
(28, 'Action', '', ''),
(35, 'Comedy', '', ''),
(36, 'History', '', ''),
(37, 'Western', '', ''),
(53, 'Thriller', '', ''),
(80, 'Crime', '', ''),
(99, 'Documentary', '', ''),
(878, 'Science Fiction', '', ''),
(9648, 'Mystery', '', ''),
(10402, 'Music', '', ''),
(10749, 'Romance', '', ''),
(10751, 'Family', '', ''),
(10752, 'War', '', ''),
(10770, 'TV Movie', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `flixycatsseries`
--

CREATE TABLE `flixycatsseries` (
  `id` int(11) NOT NULL,
  `name` varchar(18) NOT NULL,
  `img` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `flixycatsseries`
--

INSERT INTO `flixycatsseries` (`id`, `name`, `img`, `color`) VALUES
(16, 'Animation', '', '#fff'),
(18, 'Drama', '', '#fff'),
(35, 'Comedy', '', '#fff'),
(37, 'Western', '', '#fff'),
(80, 'Crime', '', '#fff'),
(99, 'Documentary', '', '#fff'),
(9648, 'Mystery', '', '#fff'),
(10751, 'Family', '', '#fff'),
(10759, 'Action & Adventure', '', '#fff'),
(10762, 'Kids', '', '#fff'),
(10763, 'News', '', '#fff'),
(10764, 'Reality', '', '#fff'),
(10765, 'Sci-Fi & Fantasy', '', '#fff'),
(10766, 'Soap', '', '#fff'),
(10767, 'Talk', '', '#fff'),
(10768, 'War & Politics', '', '#fff');

-- --------------------------------------------------------

--
-- Structure de la table `flixycats_movies`
--

CREATE TABLE `flixycats_movies` (
  `id` int(11) NOT NULL,
  `id_movie` int(11) NOT NULL,
  `id_cat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `flixycats_movies`
--

INSERT INTO `flixycats_movies` (`id`, `id_movie`, `id_cat`) VALUES
(1, 1, 28),
(2, 1, 12),
(3, 2, 80),
(4, 2, 27),
(5, 2, 9648),
(6, 3, 28),
(7, 3, 53),
(8, 3, 878),
(9, 3, 9648),
(10, 3, 12),
(11, 4, 18),
(12, 4, 53),
(13, 4, 9648),
(14, 5, 28),
(15, 5, 12),
(16, 5, 35),
(17, 6, 10749),
(18, 6, 35),
(19, 7, 28),
(20, 7, 80),
(21, 8, 18),
(22, 9, 18),
(23, 9, 53),
(24, 10, 12),
(25, 10, 14),
(26, 10, 28),
(27, 11, 12),
(28, 11, 28),
(29, 11, 878),
(30, 12, 12),
(31, 12, 28),
(32, 12, 878),
(33, 13, 10751),
(34, 13, 14),
(35, 13, 12),
(36, 14, 12),
(37, 14, 14),
(38, 14, 10751),
(39, 15, 18),
(40, 15, 12),
(41, 15, 37),
(42, 16, 18),
(43, 17, 18),
(44, 17, 36),
(45, 18, 35),
(46, 19, 35),
(47, 20, 16),
(48, 20, 10751),
(49, 21, 27),
(50, 22, 80),
(51, 22, 18),
(52, 22, 53),
(53, 23, 12),
(54, 23, 18),
(55, 23, 53),
(56, 24, 14),
(57, 24, 12),
(58, 24, 16),
(59, 24, 10751),
(60, 25, 53),
(61, 25, 28),
(62, 25, 12),
(63, 25, 35),
(64, 25, 80),
(65, 26, 28),
(66, 26, 12),
(67, 26, 18),
(68, 26, 10751),
(69, 27, 53),
(70, 27, 12),
(71, 27, 28),
(72, 28, 28),
(73, 28, 53),
(74, 28, 878),
(75, 29, 28),
(76, 29, 12),
(77, 29, 10751),
(78, 29, 14),
(79, 30, 12),
(80, 30, 14),
(81, 30, 28),
(82, 30, 10749),
(83, 31, 27),
(84, 31, 53),
(85, 32, 18),
(86, 32, 10749),
(87, 33, 35),
(88, 33, 18),
(89, 33, 10749),
(90, 34, 28),
(91, 34, 12),
(92, 34, 27),
(93, 34, 878),
(94, 35, 18),
(95, 35, 12),
(96, 35, 36),
(97, 36, 18),
(98, 37, 53),
(99, 37, 80),
(100, 37, 27),
(101, 38, 28),
(102, 38, 9648),
(103, 38, 53),
(104, 39, 28),
(105, 39, 878),
(106, 39, 12),
(107, 39, 53),
(108, 40, 9648),
(109, 40, 27),
(110, 41, 18),
(111, 41, 10749),
(112, 42, 28),
(113, 42, 53),
(114, 42, 10749),
(115, 43, 18),
(116, 43, 27),
(117, 43, 53),
(118, 44, 14),
(119, 44, 12),
(120, 44, 28),
(121, 44, 35),
(122, 44, 18),
(123, 45, 12),
(124, 45, 14),
(125, 45, 18),
(126, 45, 10749),
(127, 46, 878),
(128, 46, 35),
(129, 46, 12),
(130, 47, 28),
(131, 48, 28),
(132, 48, 12),
(133, 48, 18),
(134, 48, 53),
(135, 49, 28),
(136, 49, 37),
(137, 49, 18),
(138, 49, 14),
(139, 49, 53),
(140, 50, 27),
(141, 50, 80),
(142, 51, 35),
(143, 51, 12),
(144, 51, 14),
(145, 51, 16),
(146, 51, 10751),
(147, 52, 9648),
(148, 52, 53),
(149, 52, 18),
(150, 53, 80),
(151, 53, 35),
(152, 53, 28),
(153, 53, 12),
(154, 54, 27),
(155, 55, 14),
(156, 55, 18),
(157, 55, 80),
(158, 56, 18),
(159, 56, 53),
(160, 56, 9648),
(161, 56, 10749),
(162, 57, 12),
(163, 57, 18),
(164, 57, 10749),
(165, 57, 28),
(166, 58, 16),
(167, 58, 28),
(168, 58, 12),
(169, 59, 28),
(170, 59, 12),
(171, 59, 18),
(172, 59, 14),
(173, 60, 35),
(174, 61, 28),
(175, 61, 12),
(176, 61, 18),
(177, 62, 27),
(178, 62, 9648),
(179, 63, 35),
(180, 64, 35),
(181, 64, 80),
(182, 64, 9648),
(183, 65, 35),
(184, 66, 35),
(185, 66, 28),
(186, 66, 878),
(187, 67, 99),
(188, 68, 28),
(189, 68, 27),
(190, 68, 35),
(191, 69, 35),
(192, 69, 18),
(193, 69, 9648),
(194, 70, 35),
(195, 70, 80),
(196, 71, 18),
(197, 72, 28),
(198, 72, 35),
(199, 72, 10749),
(200, 73, 28),
(201, 73, 16),
(202, 73, 878),
(203, 74, 35),
(204, 74, 10749),
(205, 75, 35),
(206, 75, 18),
(207, 76, 35),
(208, 76, 18),
(209, 76, 10749),
(210, 77, 28),
(211, 77, 53),
(212, 78, 27),
(213, 78, 53),
(214, 79, 18),
(215, 79, 14),
(216, 80, 35),
(217, 81, 18),
(218, 81, 53),
(219, 81, 9648),
(220, 82, 18),
(221, 82, 10749),
(222, 82, 36),
(223, 83, 80),
(224, 83, 35),
(225, 83, 28),
(226, 84, 35),
(227, 84, 28),
(228, 85, 18),
(229, 86, 35),
(230, 86, 12),
(231, 87, 28),
(232, 87, 35),
(233, 88, 28),
(234, 88, 18),
(235, 88, 10752),
(236, 89, 35),
(237, 90, 35),
(238, 90, 36),
(239, 91, 28),
(240, 91, 878),
(241, 91, 53),
(242, 92, 53),
(243, 92, 18),
(244, 92, 80),
(245, 93, 878),
(246, 93, 28),
(247, 93, 12),
(248, 93, 14),
(249, 93, 35),
(250, 94, 27),
(251, 95, 10770),
(252, 95, 53),
(253, 96, 27),
(254, 97, 28),
(255, 97, 10752),
(256, 98, 28),
(257, 98, 80),
(258, 99, 28),
(259, 99, 878),
(260, 99, 53),
(261, 99, 12),
(262, 100, 18),
(263, 101, 53),
(264, 101, 28),
(265, 102, 18),
(266, 103, 35),
(267, 104, 18),
(268, 105, 18),
(269, 107, 18),
(270, 107, 27),
(271, 108, 53),
(272, 108, 28),
(273, 108, 80),
(274, 109, 35),
(275, 109, 10749),
(276, 110, 14),
(277, 110, 28),
(278, 110, 12),
(279, 111, 18),
(280, 112, 99),
(281, 113, 35),
(282, 114, 18),
(283, 116, 18),
(284, 117, 16),
(285, 118, 28),
(286, 118, 12),
(287, 118, 14),
(288, 118, 27),
(289, 118, 53),
(290, 119, 53),
(291, 119, 28),
(292, 120, 12),
(293, 120, 16),
(294, 121, 27),
(295, 121, 878),
(296, 121, 28),
(297, 124, 53),
(298, 124, 18),
(299, 125, 99),
(300, 126, 53),
(301, 126, 99),
(302, 127, 35),
(303, 128, 28),
(304, 128, 80),
(305, 128, 10402),
(306, 128, 53),
(307, 129, 12),
(308, 129, 878),
(309, 129, 28),
(310, 130, 18),
(311, 130, 10749),
(312, 131, 12),
(313, 131, 35),
(314, 131, 10749),
(315, 132, 18),
(316, 133, 18),
(317, 133, 36),
(318, 134, 28),
(319, 134, 53),
(320, 135, 18),
(321, 136, 18),
(322, 137, 12),
(323, 137, 16),
(324, 137, 35),
(325, 137, 10751),
(326, 138, 18),
(327, 138, 36),
(328, 139, 18),
(329, 140, 10749),
(330, 140, 35),
(331, 141, 10749),
(332, 142, 10402),
(333, 142, 99),
(334, 144, 18),
(335, 144, 878),
(336, 145, 53),
(337, 145, 27),
(338, 145, 9648),
(339, 146, 28),
(340, 146, 12),
(341, 146, 878),
(342, 147, 53),
(343, 148, 80),
(344, 148, 18),
(345, 148, 27),
(346, 148, 9648),
(347, 148, 53),
(348, 149, 35),
(349, 150, 16),
(350, 150, 10751),
(351, 151, 35),
(352, 151, 18),
(353, 153, 16),
(354, 154, 18),
(355, 155, 28),
(356, 155, 12),
(357, 155, 18),
(358, 155, 878),
(359, 156, 35),
(360, 156, 16),
(361, 156, 10751),
(362, 156, 28),
(363, 158, 18),
(364, 159, 18),
(365, 160, 53),
(366, 161, 18),
(367, 162, 35),
(368, 162, 80),
(369, 162, 18),
(370, 163, 18),
(371, 164, 35),
(372, 164, 16),
(373, 164, 12),
(374, 165, 10749),
(375, 165, 10752),
(376, 165, 35),
(377, 165, 18),
(378, 166, 18),
(379, 166, 35),
(380, 167, 53),
(381, 167, 18),
(382, 168, 10751),
(383, 168, 18),
(384, 169, 28),
(385, 169, 12),
(386, 169, 35),
(387, 169, 878),
(388, 170, 18),
(389, 173, 53),
(390, 173, 27),
(391, 174, 35),
(392, 175, 35),
(393, 176, 35),
(394, 177, 12),
(395, 177, 16),
(396, 178, 53),
(397, 178, 28),
(398, 178, 878),
(399, 179, 53),
(400, 180, 18),
(401, 181, 99),
(402, 182, 35),
(403, 183, 37),
(404, 183, 878),
(405, 183, 14),
(406, 183, 27),
(407, 184, 28),
(408, 184, 53),
(409, 185, 878),
(410, 186, 35),
(411, 187, 10751),
(412, 189, 99),
(413, 190, 99),
(414, 191, 10749),
(415, 191, 10402),
(416, 191, 18),
(417, 192, 18),
(418, 192, 35),
(419, 193, 28),
(420, 193, 27),
(421, 193, 53),
(422, 194, 53),
(423, 194, 27),
(424, 195, 28),
(425, 195, 18),
(426, 195, 36),
(427, 195, 53),
(428, 195, 10752),
(429, 196, 18),
(430, 196, 9648),
(431, 196, 53),
(432, 197, 28),
(433, 197, 80),
(434, 197, 18),
(435, 197, 53),
(436, 198, 18),
(437, 198, 53),
(438, 198, 878),
(439, 199, 18),
(440, 199, 37),
(441, 200, 53),
(442, 200, 27),
(443, 200, 14),
(444, 201, 35),
(445, 201, 10751),
(446, 201, 16),
(447, 202, 35),
(448, 203, 10752),
(449, 203, 18),
(450, 204, 99),
(451, 205, 99),
(452, 206, 99),
(453, 207, 99),
(454, 208, 80),
(455, 208, 18),
(456, 208, 36),
(457, 208, 10770);

-- --------------------------------------------------------

--
-- Structure de la table `flixycontact`
--

CREATE TABLE `flixycontact` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `email` text NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `flixyepisodes`
--

CREATE TABLE `flixyepisodes` (
  `id` int(11) NOT NULL,
  `epid` int(11) NOT NULL DEFAULT '1',
  `embed` varchar(255) NOT NULL,
  `openload` varchar(255) NOT NULL,
  `openload2` varchar(255) NOT NULL,
  `date` int(11) NOT NULL,
  `serieid` int(11) NOT NULL,
  `season` int(11) NOT NULL,
  `pinned` int(11) NOT NULL DEFAULT '0',
  `infos` text NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL,
  `air_date` date NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `flixyepisodes`
--

INSERT INTO `flixyepisodes` (`id`, `epid`, `embed`, `openload`, `openload2`, `date`, `serieid`, `season`, `pinned`, `infos`, `type`, `status`, `air_date`, `name`) VALUES
(1, 1, '', '', '', 0, 1, 1, 0, '', 0, 0, '2016-06-23', 'Piloto'),
(2, 2, '', '', '', 0, 1, 1, 0, '', 0, 0, '2016-06-30', 'Cuarenta Minutos'),
(3, 3, '', '', '', 0, 1, 1, 0, '', 0, 0, '2016-07-07', 'Estrategia de Entrada'),
(4, 4, '', '', '', 0, 1, 1, 0, '', 0, 0, '2016-07-14', 'Lirio de los Valles'),
(5, 5, '', '', '', 0, 1, 1, 0, '', 0, 0, '2016-07-21', 'Un Alma. Un Mapa. Dos Futuros.'),
(6, 6, '', '', '', 0, 1, 1, 0, '', 0, 0, '2016-07-28', 'El EnganÌƒo como la Regla'),
(7, 7, '', '', '', 0, 1, 1, 0, '', 0, 0, '2016-08-04', 'El Hombre PaÌjaro'),
(8, 8, '', '', '', 0, 1, 1, 0, '', 0, 0, '2016-08-11', 'Billete de Magia'),
(9, 9, '', '', '', 0, 1, 1, 0, '', 0, 0, '2016-08-18', 'Coge Todo Lo Que Puede Llevar'),
(10, 10, '', '', '', 0, 1, 1, 0, '', 0, 0, '2016-08-25', 'Esta â€˜Cosaâ€™ Que Es Nuestra'),
(11, 11, '', '', '', 0, 1, 1, 0, '', 0, 0, '2016-09-01', 'Punto Sin Retorno'),
(12, 12, '', '', '', 0, 1, 1, 0, '', 0, 0, '2016-09-08', 'Quinientos Mil'),
(13, 13, '', '', '', 0, 1, 1, 0, '', 0, 0, '2016-09-15', 'Cicatriz'),
(14, 1, '', 'qtbQRdWZMB', '', 0, 1, 2, 0, '', 0, 0, '2017-06-08', 'El Cuerpo De Cristo'),
(15, 2, '', '', '', 0, 1, 2, 0, '', 0, 0, '2017-06-15', 'Dios Y El Abogado'),
(16, 3, '', '', '', 0, 1, 2, 0, '', 0, 0, '2017-06-22', 'Un Pacto Con El Diablo'),
(17, 4, '', '', '', 0, 1, 2, 0, '', 0, 0, '2017-06-29', 'El Beso De Judas'),
(18, 5, '', '', '', 0, 1, 2, 0, '', 0, 0, '2017-07-06', 'El Nacimiento De Bolivia'),
(19, 6, '', '', '', 0, 1, 2, 0, '', 0, 0, '2017-07-13', 'El Camino De La Muerte'),
(20, 7, '', '', '', 0, 1, 2, 0, '', 0, 0, '2017-07-20', 'El Precio De La Fe'),
(21, 8, '', '', '', 0, 1, 2, 0, '', 0, 0, '2017-07-27', 'Sacar Con SifÃ³n El Mar'),
(22, 1, '', '', '', 0, 2, 1, 0, '', 0, 0, '2017-03-31', 'Tape 1, Side A'),
(23, 2, '', '', '', 0, 2, 1, 0, '', 0, 0, '2017-03-31', 'Tape 1, Side B'),
(24, 3, '', '', '', 0, 2, 1, 0, '', 0, 0, '2017-03-31', 'Tape 2, Side A'),
(25, 4, '', '', '', 0, 2, 1, 0, '', 0, 0, '2017-03-31', 'Tape 2, Side B'),
(26, 5, '', '', '', 0, 2, 1, 0, '', 0, 0, '2017-03-31', 'Tape 3, Side A'),
(27, 6, '', '', '', 0, 2, 1, 0, '', 0, 0, '2017-03-31', 'Tape 3, Side B'),
(28, 7, '', '', '', 0, 2, 1, 0, '', 0, 0, '2017-03-31', 'Tape 4, Side A'),
(29, 8, '', '', '', 0, 2, 1, 0, '', 0, 0, '2017-03-31', 'Tape 4, Side B'),
(30, 9, '', '', '', 0, 2, 1, 0, '', 0, 0, '2017-03-31', 'Tape 5, Side A'),
(31, 10, '', '', '', 0, 2, 1, 0, '', 0, 0, '2017-03-31', 'Tape 5, Side B'),
(32, 11, '', '', '', 0, 2, 1, 0, '', 0, 0, '2017-03-31', 'Tape 6, Side A'),
(33, 12, '', '', '', 0, 2, 1, 0, '', 0, 0, '2017-03-31', 'Tape 6, Side B'),
(34, 13, '', '', '', 0, 2, 1, 0, '', 0, 0, '2017-03-31', 'Tape 7, Side A'),
(35, 1, '', '', '', 0, 3, 1, 0, '', 0, 0, '2015-08-28', 'Descenso'),
(36, 2, '', '', '', 0, 3, 1, 0, '', 0, 0, '2015-08-28', 'The Sword of SimÃ³n Bolivar'),
(37, 3, '', '', '', 0, 3, 1, 0, '', 0, 0, '2015-08-28', 'The Men of Always'),
(38, 4, '', '', '', 0, 3, 1, 0, '', 0, 0, '2015-08-28', 'The Palace in Flames'),
(39, 5, '', '', '', 0, 3, 1, 0, '', 0, 0, '2015-08-28', 'There Will Be a Future'),
(40, 6, '', '', '', 0, 3, 1, 0, '', 0, 0, '2015-08-28', 'Explosivos'),
(41, 7, '', '', '', 0, 3, 1, 0, '', 0, 0, '2015-08-28', 'You Will Cry Tears of Blood'),
(42, 8, '', '', '', 0, 3, 1, 0, '', 0, 0, '2015-08-28', 'La Gran Mentira'),
(43, 9, '', '', '', 0, 3, 1, 0, '', 0, 0, '2015-08-28', 'La Catedral'),
(44, 10, '', '', '', 0, 3, 1, 0, '', 0, 0, '2015-08-28', 'Despegue'),
(45, 1, '', '', '', 0, 3, 2, 0, '', 0, 0, '2016-09-02', 'Free at Last'),
(46, 2, '', '', '', 0, 3, 2, 0, '', 0, 0, '2016-09-02', 'Cambalache'),
(47, 3, '', '', '', 0, 3, 2, 0, '', 0, 0, '2016-09-02', 'Our Man in Madrid'),
(48, 4, '', '', '', 0, 3, 2, 0, '', 0, 0, '2016-09-02', 'The Good, The Bad, and The Dead'),
(49, 5, '', '', '', 0, 3, 2, 0, '', 0, 0, '2016-09-02', 'The Enemies of My Enemy'),
(50, 6, '', '', '', 0, 3, 2, 0, '', 0, 0, '2016-09-02', 'Los Pepes'),
(51, 7, '', '', '', 0, 3, 2, 0, '', 0, 0, '2016-09-02', 'Deutschland 93'),
(52, 8, '', '', '', 0, 3, 2, 0, '', 0, 0, '2016-09-02', 'Exit El PatrÃ³n'),
(53, 9, '', '', '', 0, 3, 2, 0, '', 0, 0, '2016-09-02', 'Neuestra Finca'),
(54, 10, '', '', '', 0, 3, 2, 0, '', 0, 0, '2016-09-02', 'Al Fin CayÃ³!'),
(55, 1, '', '', '', 0, 4, 1, 0, '', 0, 0, '2015-05-27', 'eps1.0_hellofriend.mov'),
(56, 2, '', '', '', 0, 4, 1, 0, '', 0, 0, '2015-07-01', 'eps1.1_ones-and-zer0es.mpeg'),
(57, 3, '', '', '', 0, 4, 1, 0, '', 0, 0, '2015-07-08', 'eps1.2_d3bug.mkv'),
(58, 4, '', '', '', 0, 4, 1, 0, '', 0, 0, '2015-07-15', 'eps1.3_da3m0ns.mp4'),
(59, 5, '', '', '', 0, 4, 1, 0, '', 0, 0, '2015-07-22', 'eps1.4_3xpl0its.wmv'),
(60, 6, '', '', '', 0, 4, 1, 0, '', 0, 0, '2015-07-29', 'eps1.5_br4ve-trave1er.asf'),
(61, 7, '', '', '', 0, 4, 1, 0, '', 0, 0, '2015-08-05', 'eps1.6_v1ew-s0urce.flv'),
(62, 8, '', '', '', 0, 4, 1, 0, '', 0, 0, '2015-08-12', 'eps1.7_wh1ter0se.m4v'),
(63, 9, '', '', '', 0, 4, 1, 0, '', 0, 0, '2015-08-19', 'eps1.8_m1rr0r1ng.qt'),
(64, 10, '', '', '', 0, 4, 1, 0, '', 0, 0, '2015-09-02', 'eps1.9_zer0-day.avi'),
(65, 1, '', '', '', 0, 4, 2, 0, '', 0, 0, '2016-07-13', 'eps2.0_unm4sk-pt1.tc'),
(66, 2, '', '', '', 0, 4, 2, 0, '', 0, 0, '2016-07-13', 'eps2.0_unm4sk-pt2.tc'),
(67, 3, '', '', '', 0, 4, 2, 0, '', 0, 0, '2016-07-20', 'eps2.1_k3rnel-pan1c.ksd'),
(68, 4, '', '', '', 0, 4, 2, 0, '', 0, 0, '2016-07-27', 'eps2.2_init_1.asec'),
(69, 5, '', '', '', 0, 4, 2, 0, '', 0, 0, '2016-08-03', 'eps2.3_logic-b0mb.hc'),
(70, 6, '', '', '', 0, 4, 2, 0, '', 0, 0, '2016-08-10', 'eps2.4_m4ster-s1ave.aes'),
(71, 7, '', '', '', 0, 4, 2, 0, '', 0, 0, '2016-08-17', 'eps2.5_h4ndshake.sme'),
(72, 8, '', '', '', 0, 4, 2, 0, '', 0, 0, '2016-08-24', 'eps2.6_succ3ss0r.p12'),
(73, 9, '', '', '', 0, 4, 2, 0, '', 0, 0, '2016-08-31', 'eps2.7_init_5.fve'),
(74, 10, '', '', '', 0, 4, 2, 0, '', 0, 0, '2016-09-07', 'eps2.8_h1dden-pr0cess.axx'),
(75, 11, '', '', '', 0, 4, 2, 0, '', 0, 0, '2016-09-14', 'eps2.9_pyth0n-pt1.p7z'),
(76, 12, '', '', '', 0, 4, 2, 0, '', 0, 0, '2016-09-21', 'eps2.9_pyth0n-pt2.p7z'),
(77, 1, '', '15ECmS7SMpg', '', 0, 5, 1, 0, '', 0, 0, '2011-04-17', 'Winter Is Coming'),
(78, 2, '', 'GjrnCKmTxCo', '', 0, 5, 1, 0, '', 0, 0, '2011-04-24', 'The Kingsroad'),
(79, 3, '', 'clPN9esRmD8', '', 0, 5, 1, 0, '', 0, 0, '2011-05-01', 'Lord Snow'),
(80, 4, '', 'YxXifsYUllg', '', 0, 5, 1, 0, '', 0, 0, '2011-05-08', 'Cripples, Bastards, and Broken Things'),
(81, 5, '', 'v31BP2j-vj4', '', 0, 5, 1, 0, '', 0, 0, '2011-05-15', 'The Wolf and the Lion'),
(82, 6, '', '15ECmS7SMpg', '', 0, 5, 1, 0, '', 0, 0, '2011-05-22', 'A Golden Crown'),
(83, 7, '', 'GjrnCKmTxCo', '', 0, 5, 1, 0, '', 0, 0, '2011-05-29', 'You Win or You Die'),
(84, 8, '', '15ECmS7SMpg', '', 0, 5, 1, 0, '', 0, 0, '2011-06-05', 'The Pointy End'),
(85, 9, '', '15ECmS7SMpg15ECmS7SMpg', '', 0, 5, 1, 0, '', 0, 0, '2011-06-12', 'Baelor'),
(86, 10, '', '15ECmS7SMpg', '', 0, 5, 1, 0, '', 0, 0, '2011-06-19', 'Fire and Blood'),
(87, 1, '', 'YxXifsYUllg', '', 0, 5, 2, 0, '', 0, 0, '2012-04-01', 'The North Remembers'),
(88, 2, '', 'YxXifsYUllg', '', 0, 5, 2, 0, '', 0, 0, '2012-04-08', 'The Night Lands'),
(89, 3, '', 'YxXifsYUllg', '', 0, 5, 2, 0, '', 0, 0, '2012-04-15', 'What is Dead May Never Die'),
(90, 4, '', '15ECmS7SMpg15ECmS7SMpg', '', 0, 5, 2, 0, '', 0, 0, '2012-04-22', 'Garden of Bones'),
(91, 5, '', '15ECmS7SMpg', '', 0, 5, 2, 0, '', 0, 0, '2012-04-29', 'The Ghost of Harrenhal'),
(92, 6, '', '15ECmS7SMpg', '', 0, 5, 2, 0, '', 0, 0, '2012-05-06', 'The Old Gods and the New'),
(93, 7, '', '15ECmS7SMpg', '', 0, 5, 2, 0, '', 0, 0, '2012-05-13', 'A Man Without Honor'),
(94, 8, '', '15ECmS7SMpg', '', 0, 5, 2, 0, '', 0, 0, '2012-05-20', 'The Prince of Winterfell'),
(95, 9, '', '15ECmS7SMpg', '', 0, 5, 2, 0, '', 0, 0, '2012-05-27', 'Blackwater'),
(96, 10, '', '15ECmS7SMpg', '', 0, 5, 2, 0, '', 0, 0, '2012-06-03', 'Valar Morghulis'),
(97, 1, '', '15ECmS7SMpg', '', 0, 5, 3, 0, '', 0, 0, '2013-03-31', 'Valar Dohaeris'),
(98, 2, '', '15ECmS7SMpg', '', 0, 5, 3, 0, '', 0, 0, '2013-04-07', 'Dark Wings, Dark Words'),
(99, 3, '', '15ECmS7SMpg', '', 0, 5, 3, 0, '', 0, 0, '2013-04-14', 'Walk of Punishment'),
(100, 4, '', '15ECmS7SMpg', '', 0, 5, 3, 0, '', 0, 0, '2013-04-21', 'And Now His Watch Is Ended'),
(101, 5, '', 'YxXifsYUllg', '', 0, 5, 3, 0, '', 0, 0, '2013-04-28', 'Kissed by Fire'),
(102, 6, '', '15ECmS7SMpg', '', 0, 5, 3, 0, '', 0, 0, '2013-05-05', 'The Climb'),
(103, 7, '', '15ECmS7SMpg15ECmS7SMpg', '', 0, 5, 3, 0, '', 0, 0, '2013-05-12', 'The Bear and the Maiden Fair'),
(104, 8, '', '15ECmS7SMpg', '', 0, 5, 3, 0, '', 0, 0, '2013-05-19', 'Second Sons'),
(105, 9, '', '15ECmS7SMpg', '', 0, 5, 3, 0, '', 0, 0, '2013-06-02', 'The Rains of Castamere'),
(106, 10, '', '15ECmS7SMpg', '', 0, 5, 3, 0, '', 0, 0, '2013-06-09', 'Mhysa'),
(107, 1, '', '15ECmS7SMpg', '', 0, 5, 4, 0, '', 0, 0, '2014-04-06', 'Two Swords'),
(108, 2, '', '15ECmS7SMpg', '', 0, 5, 4, 0, '', 0, 0, '2014-04-13', 'The Lion and the Rose'),
(109, 3, '', '15ECmS7SMpg', '', 0, 5, 4, 0, '', 0, 0, '2014-04-20', 'Breaker of Chains'),
(110, 4, '', '15ECmS7SMpg', '', 0, 5, 4, 0, '', 0, 0, '2014-04-27', 'Oathkeeper'),
(111, 5, '', '15ECmS7SMpg', '', 0, 5, 4, 0, '', 0, 0, '2014-05-04', 'First of His Name'),
(112, 6, '', '15ECmS7SMpg', '', 0, 5, 4, 0, '', 0, 0, '2014-05-11', 'The Laws of Gods and Men'),
(113, 7, '', '15ECmS7SMpg', '', 0, 5, 4, 0, '', 0, 0, '2014-05-18', 'Mockingbird'),
(114, 8, '', '15ECmS7SMpg', '', 0, 5, 4, 0, '', 0, 0, '2014-06-01', 'The Mountain and the Viper'),
(115, 9, '', '15ECmS7SMpg', '', 0, 5, 4, 0, '', 0, 0, '2014-06-08', 'The Watchers on the Wall'),
(116, 10, '', '15ECmS7SMpg', '', 0, 5, 4, 0, '', 0, 0, '2014-06-15', 'The Children'),
(117, 1, '', '15ECmS7SMpg', '', 0, 5, 5, 0, '', 0, 0, '2015-04-12', 'The Wars to Come'),
(118, 2, '', '15ECmS7SMpg', '', 0, 5, 5, 0, '', 0, 0, '2015-04-19', 'The House of Black and White'),
(119, 3, '', '15ECmS7SMpg', '', 0, 5, 5, 0, '', 0, 0, '2015-04-26', 'High Sparrow'),
(120, 4, '', '15ECmS7SMpg', '', 0, 5, 5, 0, '', 0, 0, '2015-05-03', 'Sons of the Harpy'),
(121, 5, '', '15ECmS7SMpg', '', 0, 5, 5, 0, '', 0, 0, '2015-05-10', 'Kill the Boy'),
(122, 6, '', '15ECmS7SMpg', '', 0, 5, 5, 0, '', 0, 0, '2015-05-17', 'Unbowed, Unbent, Unbroken'),
(123, 7, '', '15ECmS7SMpg', '', 0, 5, 5, 0, '', 0, 0, '2015-05-24', 'The Gift'),
(124, 8, '', '15ECmS7SMpg', '', 0, 5, 5, 0, '', 0, 0, '2015-05-31', 'Hardhome'),
(125, 9, '', '15ECmS7SMpg', '', 0, 5, 5, 0, '', 0, 0, '2015-06-07', 'The Dance of Dragons'),
(126, 10, '', '15ECmS7SMpg', '', 0, 5, 5, 0, '', 0, 0, '2015-06-14', 'Mother\'s Mercy'),
(127, 1, '', '15ECmS7SMpg', '', 0, 5, 6, 0, '', 0, 0, '2016-04-24', 'The Red Woman'),
(128, 2, '', 'YxXifsYUllg', '', 0, 5, 6, 0, '', 0, 0, '2016-05-01', 'Home'),
(129, 3, '', '15ECmS7SMpg', '', 0, 5, 6, 0, '', 0, 0, '2016-05-08', 'Oathbreaker'),
(130, 4, '', '15ECmS7SMpg', '', 0, 5, 6, 0, '', 0, 0, '2016-05-15', 'Book of the Stranger'),
(131, 5, '', '15ECmS7SMpg15ECmS7SMpg', '', 0, 5, 6, 0, '', 0, 0, '2016-05-22', 'The Door'),
(132, 6, '', '15ECmS7SMpg', '', 0, 5, 6, 0, '', 0, 0, '2016-05-29', 'Blood of My Blood'),
(133, 7, '', '15ECmS7SMpg', '', 0, 5, 6, 0, '', 0, 0, '2016-06-05', 'The Broken Man'),
(134, 8, '', '15ECmS7SMpg', '', 0, 5, 6, 0, '', 0, 0, '2016-06-12', 'No One'),
(135, 9, '', '15ECmS7SMpg', '', 0, 5, 6, 0, '', 0, 0, '2016-06-19', 'Battle of the Bastards'),
(136, 10, '', '15ECmS7SMpg', '', 0, 5, 6, 0, '', 0, 0, '2016-06-26', 'The Winds of Winter'),
(138, 1, '', '', '', 0, 6, 1, 0, '', 0, 0, '2016-02-15', 'The Rabbit Hole'),
(139, 2, '', '', '', 0, 6, 1, 0, '', 0, 0, '2016-02-22', 'The Kill Floor'),
(140, 3, '', '', '', 0, 6, 1, 0, '', 0, 0, '2016-02-29', 'Other Voices, Other Rooms'),
(141, 4, '', '', '', 0, 6, 1, 0, '', 0, 0, '2016-03-07', 'The Eyes of Texas'),
(142, 5, '', '', '', 0, 6, 1, 1, '', 0, 0, '2016-03-14', 'The Truth'),
(143, 6, '', '', '', 0, 6, 1, 0, '', 0, 0, '2016-03-21', 'Happy Birthday, Lee Harvey Oswald'),
(144, 7, '', '', '', 0, 6, 1, 0, '', 0, 0, '2016-03-28', 'Soldier Boy'),
(145, 8, '', '', '', 0, 6, 1, 0, '', 0, 0, '2016-04-04', 'The Day in Question'),
(146, 1, '', '15ECmS7SMpg', '', 0, 5, 7, 0, '', 0, 0, '2017-07-16', 'Episode 1'),
(147, 2, '', '', '', 0, 5, 7, 0, '', 0, 0, '2017-07-23', 'Stormborn'),
(148, 3, '', '', '', 0, 5, 7, 0, '', 0, 0, '2017-07-30', 'The Queenâ€™s Justice'),
(149, 1, '', '', '', 0, 7, 1, 0, '', 0, 0, '2006-10-01', 'Dexter'),
(150, 2, '', '', '', 0, 7, 1, 0, '', 0, 0, '2006-10-08', 'Crocodile'),
(151, 3, '', '', '', 0, 7, 1, 0, '', 0, 0, '2006-10-15', 'Popping Cherry'),
(152, 4, '', '', '', 0, 7, 1, 0, '', 0, 0, '2006-10-22', 'Let\'s Give the Boy a Hand'),
(153, 5, '', '', '', 0, 7, 1, 0, '', 0, 0, '2006-10-29', 'Love American Style'),
(154, 6, '', '', '', 0, 7, 1, 0, '', 0, 0, '2006-11-05', 'Return to Sender'),
(155, 7, '', '', '', 0, 7, 1, 0, '', 0, 0, '2006-11-12', 'Circle of Friends'),
(156, 8, '', '', '', 0, 7, 1, 0, '', 0, 0, '2006-11-19', 'Shrink Wrap'),
(157, 9, '', '', '', 0, 7, 1, 0, '', 0, 0, '2006-11-26', 'Father Knows Best'),
(158, 10, '', '', '', 0, 7, 1, 0, '', 0, 0, '2006-12-03', 'Seeing Red'),
(159, 11, '', '', '', 0, 7, 1, 0, '', 0, 0, '2006-12-10', 'Truth Be Told'),
(160, 12, '', '', '', 0, 7, 1, 0, '', 0, 0, '2006-12-17', 'Born Free'),
(161, 1, '', '', '', 0, 7, 2, 0, '', 0, 0, '2007-09-30', 'It\'s Alive!'),
(162, 2, '', '', '', 0, 7, 2, 0, '', 0, 0, '2007-10-07', 'Waiting to Exhale'),
(163, 3, '', '', '', 0, 7, 2, 0, '', 0, 0, '2007-10-14', 'An Inconvenient Lie'),
(164, 4, '', '', '', 0, 7, 2, 0, '', 0, 0, '2007-10-21', 'See-Through'),
(165, 5, '', '', '', 0, 7, 2, 0, '', 0, 0, '2007-10-28', 'The Dark Defender'),
(166, 6, '', '', '', 0, 7, 2, 0, '', 0, 0, '2007-11-04', 'Dex, Lies, and Videotape'),
(167, 7, '', '', '', 0, 7, 2, 0, '', 0, 0, '2007-11-11', 'That Night, A Forest Grew'),
(168, 8, '', '', '', 0, 7, 2, 0, '', 0, 0, '2007-11-18', 'Morning Comes'),
(169, 9, '', '', '', 0, 7, 2, 0, '', 0, 0, '2007-11-25', 'Resistance Is Futile'),
(170, 10, '', '', '', 0, 7, 2, 0, '', 0, 0, '2007-12-02', 'There\'s Something About Harry'),
(171, 11, '', '', '', 0, 7, 2, 0, '', 0, 0, '2007-12-09', 'Left Turn Ahead'),
(172, 12, '', '', '', 0, 7, 2, 0, '', 0, 0, '2007-12-16', 'The British Invasion'),
(173, 1, '', '', '', 0, 7, 3, 0, '', 0, 0, '2008-09-28', 'Our Father'),
(174, 2, '', '', '', 0, 7, 3, 0, '', 0, 0, '2008-10-05', 'Finding Freebo'),
(175, 3, '', '', '', 0, 7, 3, 0, '', 0, 0, '2008-10-12', 'The Lion Sleeps Tonight'),
(176, 4, '', '', '', 0, 7, 3, 0, '', 0, 0, '2008-10-19', 'All In the Family'),
(177, 5, '', '', '', 0, 7, 3, 0, '', 0, 0, '2008-10-26', 'Turning Biminese'),
(178, 6, '', '', '', 0, 7, 3, 0, '', 0, 0, '2008-11-02', 'SÃ­ Se Puede'),
(179, 7, '', '', '', 0, 7, 3, 0, '', 0, 0, '2008-11-09', 'Easy as Pie'),
(180, 8, '', '', '', 0, 7, 3, 0, '', 0, 0, '2008-11-16', 'The Damage a Man Can Do'),
(181, 9, '', '', '', 0, 7, 3, 0, '', 0, 0, '2008-11-23', 'About Last Night'),
(182, 10, '', '', '', 0, 7, 3, 0, '', 0, 0, '2008-11-30', 'Go Your Own Way'),
(183, 11, '', '', '', 0, 7, 3, 0, '', 0, 0, '2008-12-07', 'I Had a Dream'),
(184, 12, '', '', '', 0, 7, 3, 0, '', 0, 0, '2008-12-14', 'Do You Take Dexter Morgan?'),
(185, 1, '', '', '', 0, 7, 4, 0, '', 0, 0, '2009-09-27', 'Living the Dream'),
(186, 2, '', '', '', 0, 7, 4, 0, '', 0, 0, '2009-10-04', 'Remains to Be Seen'),
(187, 3, '', '', '', 0, 7, 4, 0, '', 0, 0, '2009-10-11', 'Blinded By the Light'),
(188, 4, '', '', '', 0, 7, 4, 0, '', 0, 0, '2009-10-18', 'Dex Takes a Holiday'),
(189, 5, '', '', '', 0, 7, 4, 0, '', 0, 0, '2009-10-25', 'Dirty Harry'),
(190, 6, '', '', '', 0, 7, 4, 0, '', 0, 0, '2009-11-01', 'If I Had a Hammer'),
(191, 7, '', '', '', 0, 7, 4, 0, '', 0, 0, '2009-11-08', 'Slack Tide'),
(192, 8, '', '', '', 0, 7, 4, 0, '', 0, 0, '2009-11-15', 'Road Kill'),
(193, 9, '', '', '', 0, 7, 4, 0, '', 0, 0, '2009-11-22', 'Hungry Man'),
(194, 10, '', '', '', 0, 7, 4, 0, '', 0, 0, '2009-11-29', 'Lost Boys'),
(195, 11, '', '', '', 0, 7, 4, 0, '', 0, 0, '2009-12-06', 'Hello, Dexter Morgan'),
(196, 12, '', '', '', 0, 7, 4, 0, '', 0, 0, '2009-12-13', 'The Getaway'),
(197, 1, '', '', '', 0, 7, 5, 0, '', 0, 0, '2010-09-26', 'My Bad'),
(198, 2, '', '', '', 0, 7, 5, 0, '', 0, 0, '2010-10-03', 'Hello Bandit'),
(199, 3, '', '', '', 0, 7, 5, 0, '', 0, 0, '2010-10-10', 'Practically Perfect'),
(200, 4, '', '', '', 0, 7, 5, 0, '', 0, 0, '2010-10-17', 'Beauty And The Beast'),
(201, 5, '', '', '', 0, 7, 5, 0, '', 0, 0, '2010-10-24', 'First Blood'),
(202, 6, '', '', '', 0, 7, 5, 0, '', 0, 0, '2010-10-31', 'Everything Is Illumenated'),
(203, 7, '', '', '', 0, 7, 5, 0, '', 0, 0, '2010-11-07', 'Circle Us'),
(204, 8, '', '', '', 0, 7, 5, 0, '', 0, 0, '2010-11-14', 'Take It!'),
(205, 9, '', '', '', 0, 7, 5, 0, '', 0, 0, '2010-11-21', 'Teenage Wasteland'),
(206, 10, '', '', '', 0, 7, 5, 0, '', 0, 0, '2010-11-28', 'In The Beginning'),
(207, 11, '', '', '', 0, 7, 5, 0, '', 0, 0, '2010-12-05', 'Hop a Freighter'),
(208, 12, '', '', '', 0, 7, 5, 0, '', 0, 0, '2010-12-12', 'The Big One'),
(209, 1, '', '', '', 0, 7, 6, 0, '', 0, 0, '2011-10-02', 'Those Kinds of Things'),
(210, 2, '', '', '', 0, 7, 6, 0, '', 0, 0, '2011-10-09', 'Once Upon a Time...'),
(211, 3, '', '', '', 0, 7, 6, 0, '', 0, 0, '2011-10-16', 'Smokey and the Bandit'),
(212, 4, '', '', '', 0, 7, 6, 0, '', 0, 0, '2011-10-23', 'A Horse of a Different Color'),
(213, 5, '', '', '', 0, 7, 6, 0, '', 0, 0, '2011-10-30', 'The Angel of Death'),
(214, 6, '', '', '', 0, 7, 6, 0, '', 0, 0, '2011-11-06', 'Just Let Go'),
(215, 7, '', '', '', 0, 7, 6, 0, '', 0, 0, '2011-11-13', 'Nebraska'),
(216, 8, '', '', '', 0, 7, 6, 0, '', 0, 0, '2011-11-20', 'Sin of Omission'),
(217, 9, '', '', '', 0, 7, 6, 0, '', 0, 0, '2011-11-27', 'Get Gellar'),
(218, 10, '', '', '', 0, 7, 6, 0, '', 0, 0, '2011-12-04', 'Ricochet Rabbit'),
(219, 11, '', '', '', 0, 7, 6, 0, '', 0, 0, '2011-12-11', 'Talk to the Hand'),
(220, 12, '', '', '', 0, 7, 6, 0, '', 0, 0, '2011-12-18', 'This is the Way the World Ends'),
(221, 1, '', '', '', 0, 7, 7, 0, '', 0, 0, '2012-09-30', 'Are You... ?'),
(222, 2, '', '', '', 0, 7, 7, 0, '', 0, 0, '2012-10-07', 'Sunshine and Frosty Swirl'),
(223, 3, '', '', '', 0, 7, 7, 0, '', 0, 0, '2012-10-14', 'Buck the System'),
(224, 4, '', '', '', 0, 7, 7, 0, '', 0, 0, '2012-10-21', 'Run'),
(225, 5, '', '', '', 0, 7, 7, 0, '', 0, 0, '2012-10-28', 'Swim Deep'),
(226, 6, '', '', '', 0, 7, 7, 0, '', 0, 0, '2012-11-04', 'Do the Wrong Thing'),
(227, 7, '', '', '', 0, 7, 7, 0, '', 0, 0, '2012-11-11', 'Chemistry'),
(228, 8, '', '', '', 0, 7, 7, 0, '', 0, 0, '2012-11-18', 'Argentina'),
(229, 9, '', '', '', 0, 7, 7, 0, '', 0, 0, '2012-11-25', 'Helter Skelter'),
(230, 10, '', '', '', 0, 7, 7, 0, '', 0, 0, '2012-12-02', 'The Dark... Whatever'),
(231, 11, '', '', '', 0, 7, 7, 0, '', 0, 0, '2012-12-09', 'Do You See What I See?'),
(232, 12, '', '', '', 0, 7, 7, 0, '', 0, 0, '2012-12-16', 'Surprise, Motherfucker!'),
(233, 1, '', '', '', 0, 7, 8, 0, '', 0, 0, '2013-06-30', 'A Beautiful Day'),
(234, 2, '', '', '', 0, 7, 8, 0, '', 0, 0, '2013-07-07', 'Every Silver Lining'),
(235, 3, '', '', '', 0, 7, 8, 0, '', 0, 0, '2013-07-14', 'What\'s Eating Dexter Morgan?'),
(236, 4, '', '', '', 0, 7, 8, 0, '', 0, 0, '2013-07-21', 'Scar Tissue'),
(237, 5, '', '', '', 0, 7, 8, 0, '', 0, 0, '2013-07-28', 'This Little Piggy'),
(238, 6, '', '', '', 0, 7, 8, 0, '', 0, 0, '2013-08-04', 'A Little Reflection'),
(239, 7, '', '', '', 0, 7, 8, 0, '', 0, 0, '2013-08-11', 'Dress Code'),
(240, 8, '', '', '', 0, 7, 8, 0, '', 0, 0, '2013-08-18', 'Are We There Yet?'),
(241, 9, '', '', '', 0, 7, 8, 0, '', 0, 0, '2013-08-25', 'Make Your Own Kind of Music'),
(242, 10, '', '', '', 0, 7, 8, 0, '', 0, 0, '2013-09-08', 'Goodbye Miami'),
(243, 11, '', '', '', 0, 7, 8, 0, '', 0, 0, '2013-09-15', 'Monkey in a Box'),
(244, 12, '', '', '', 0, 7, 8, 0, '', 0, 0, '2013-09-22', 'Remember the Monsters?');

-- --------------------------------------------------------

--
-- Structure de la table `flixygrid`
--

CREATE TABLE `flixygrid` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `flixymails`
--

CREATE TABLE `flixymails` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `serieid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `flixymails`
--

INSERT INTO `flixymails` (`id`, `email`, `serieid`) VALUES
(1, 'hamza2life@gmail.com', 1),
(2, 'hamza2life@gmail.com', 2),
(3, 'hamzalife@gmail.com', 2),
(4, 'hmzlife@gmail.com', 1),
(5, 'gfdg', 1);

-- --------------------------------------------------------

--
-- Structure de la table `flixymovies`
--

CREATE TABLE `flixymovies` (
  `id` int(11) NOT NULL,
  `imdbID` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `Year` int(11) NOT NULL,
  `pic` varchar(255) NOT NULL,
  `Released` varchar(255) NOT NULL,
  `olstatus` int(11) NOT NULL,
  `openload` varchar(255) NOT NULL,
  `streamango` varchar(255) DEFAULT NULL,
  `Status` int(11) NOT NULL DEFAULT '3',
  `trailer` varchar(255) DEFAULT NULL,
  `Quality` varchar(255) NOT NULL DEFAULT 'HD',
  `Rating` float DEFAULT '0',
  `Plot` text,
  `Actors` text,
  `Genre` int(11) DEFAULT NULL,
  `Runtime` varchar(255) DEFAULT '0',
  `splash` varchar(255) DEFAULT NULL,
  `views` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `flixymovies`
--

INSERT INTO `flixymovies` (`id`, `imdbID`, `name`, `Year`, `pic`, `Released`, `olstatus`, `openload`, `streamango`, `Status`, `trailer`, `Quality`, `Rating`, `Plot`, `Actors`, `Genre`, `Runtime`, `splash`, `views`) VALUES
(1, 'tt0955308', 'Robin Hood', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/4gH0vhyOcl7QV3t81653CpWjEB6.jpg', '2010-05-12', 0, '63RwBXYkgbE', NULL, 3, 'https://www.youtube.com/embed/aMcDeNo6KUs', 'HD', 6.2, 'When soldier Robin happens upon the dying Robert of Loxley, he promises to return the man\'s sword to his family in Nottingham. There, he assumes Robert\'s identity; romances his widow, Marion; and draws the ire of the town\'s sheriff and King John\'s henchman, Godfrey.', NULL, NULL, '140', NULL, 0),
(2, 'tt1100051', 'Bereavement', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/jMMOIVcFtCDUsNjJo1t1Wl5d3CJ.jpg', '2010-07-16', 0, 'U59A6SJbCnk', NULL, 3, 'https://www.youtube.com/embed/', 'HD', 5.4, 'In 1989, six year old Martin Bristoll was kidnapped from his backyard swing in Minersville Pennsylvania. Graham Sutter, a psychotic recluse, kept Martin imprisoned on his derelict pig farm, forcing him to witness and participate in unspeakable horrors. Chosen at random, his victim\'s screams were drowned out by the rural countryside. For five years, Martin\'s whereabouts have remained a mystery, until 17 year old Allison Miller (Alexandra Daddario) comes to live with her Uncle, Jonathan (Michael Biehn). While exploring her new surroundings, Allison discovers things aren\'t quite right at the farmhouse down the road. Her curiosity disturbs a hornet\'s nest of evil and despair that once torn open, can never be closed. Starring Michael Biehn, Brett Rickaby, Alexandra Daddario, and John Savage. Written and Directed by Stevan Mena', NULL, NULL, '103', NULL, 0),
(3, 'tt1375666', 'Inception', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/qmDpIHrmpJINaRKAfWQfftjCdyi.jpg', '2010-07-14', 0, 'jlb9uR4tuC0', NULL, 3, 'https://www.youtube.com/embed/xitHF0IPJSQ', 'HD', 8, 'Cobb, a skilled thief who commits corporate espionage by infiltrating the subconscious of his targets is offered a chance to regain his old life as payment for a task considered to be impossible: \"inception\", the implantation of another person\'s idea into a target\'s subconscious.', NULL, NULL, '148', NULL, 0),
(4, 'tt1130884', 'Shutter Island', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/aZqKsvpJDFy2UzUMsdskNFbfkOd.jpg', '2010-02-18', 0, '72fA-_Lq8Ak3', NULL, 3, 'https://www.youtube.com/embed/qdPw9x9h5CY', 'HD', 7.8, 'World War II soldier-turned-U.S. Marshal Teddy Daniels investigates the disappearance of a patient from a hospital for the criminally insane, but his efforts are compromised by his troubling visions and also by a mysterious doctor.', NULL, NULL, '138', NULL, 0),
(5, 'tt0446029', 'Scott Pilgrim vs. the World', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/lafRuPbjEQrrHG9QEaoyV2znZC.jpg', '2010-07-27', 0, 'https://openload.co/f/CFQmXErL4Ho/Scott.Pilgrim.vs.the.World.2010.Bluray.720p.x264.YIFY.mp4\r', NULL, 3, 'https://www.youtube.com/embed/7wd5KEaOtm4', 'HD', 7.2, 'Scott Pilgrim is a film adaptation of the critically acclaimed, award-winning series of graphic novels of the same name by Canadian cartoonist Bryan Lee Oâ€™Malley. Scott Pilgrim is a 23 year old Canadian slacker and wannabe rockstar who falls in love with an American delivery girl, Ramona V. Flowers, and must defeat her seven \"evil exes\" to be able to date her.', NULL, NULL, '112', NULL, 0),
(6, 'tt1216492', 'Leap Year', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/9UWKpkQvZaPqpb85Or4e0PghJft.jpg', '2010-01-08', 0, 'https://openload.co/f/qnx2Di5b7gw/Leap.Year.2010.720p.BluRay.x264.YIFY.mp4\r', NULL, 3, 'https://www.youtube.com/embed/DVN17lxkwDg', 'HD', 6.4, 'When yet another anniversary passes without a marriage proposal from her boyfriend, Anna decides to take action. Aware of a Celtic tradition that allows women to pop the question on Feb. 29, she plans to follow her lover to Dublin and ask him to marry her. Fate has other plans, however, and Anna winds up on the other side of the Emerald Isle with handsome, but surly, Declan -- an Irishman who may just lead Anna down the road to true love.', NULL, NULL, '100', NULL, 0),
(7, 'tt1250777', 'Kick-Ass', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/gtfggr5n3ED1neoTqVYmsVCoSS.jpg', '2010-03-22', 0, 'https://openload.co/f/rF8qyDuIE8U/Kick.Ass.2010.mp4\r', NULL, 3, 'https://www.youtube.com/embed/yxbjexP3Isc', 'HD', 7, 'Dave Lizewski is an unnoticed high school student and comic book fan who one day decides to become a super-hero, even though he has no powers, training or meaningful reason to do so.', NULL, NULL, '117', NULL, 0),
(8, 'tt1285016', 'The Social Network', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/ok5Wh8385Kgblq9MSU4VGvazeMH.jpg', '2010-09-30', 0, 'https://openload.co/f/IeJEgWEFEl8/The_Social_Network_2010\r', NULL, 3, 'https://www.youtube.com/embed/lB95KLmpLR4', 'HD', 7.1, 'On a fall night in 2003, Harvard undergrad and computer programming genius Mark Zuckerberg sits down at his computer and heatedly begins working on a new idea. In a fury of blogging and programming, what begins in his dorm room as a small site among friends soon becomes a global social network and a revolution in communication. A mere six years and 500 million friends later, Mark Zuckerberg is the youngest billionaire in history... but for this entrepreneur, success leads to both personal and legal complications.', NULL, NULL, '120', NULL, 0),
(9, 'tt0947798', 'Black Swan', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/dQ7uxvsVTspVIsqjfgQj8usJpwX.jpg', '2010-12-02', 0, 'https://openload.co/f/Vfe1uI_Z1TI/Black.Swan.720p.x264.YIFY.mkv.mp4\r', NULL, 3, 'https://www.youtube.com/embed/5jaI1XOB-bs', 'HD', 7.3, 'A ballet dancer wins the lead in \"Swan Lake\" and is perfect for the role of the delicate White Swan - Princess Odette - but slowly loses her mind as she becomes more and more like Odile, the Black Swan.', NULL, NULL, '108', NULL, 0),
(10, 'tt0800320', 'Clash of the Titans', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/n8W2Y72VzSi8Yz6IvYWwfoiMTS6.jpg', '2010-04-01', 0, 'https://openload.co/f/z4lnd_6OpJs/Clash.of.the.Titans..2010..720p.x264.-.800MB.-.YIFY.mp4\r', NULL, 3, 'https://www.youtube.com/embed/TfyunOnMGqM', 'HD', 5.6, 'Born of a god but raised as a man, Perseus is helpless to save his family from Hades, vengeful god of the underworld. With nothing to lose, Perseus volunteers to lead a dangerous mission to defeat Hades before he can seize power from Zeus and unleash hell on earth. Battling unholy demons and fearsome beasts, Perseus and his warriors will only survive if Perseus accepts his power as a god, defies fate and creates his own destiny.', NULL, NULL, '106', NULL, 2),
(11, 'tt1104001', 'TRON: Legacy', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/vllvystwQjmXzy5OvBKnGl1JREF.jpg', '2010-12-10', 0, 'https://openload.co/f/Cl8LgsLWXr8/Tron.Legacy.2010.Br.720p.YIFY.mkv.mp4\r', NULL, 3, 'https://www.youtube.com/embed/L9szn1QQfas', 'HD', 6.3, 'Sam Flynn, the tech-savvy and daring son of Kevin Flynn, investigates his father\'s disappearance and is pulled into The Grid. With the help of a mysterious program named Quorra, Sam quests to stop evil dictator Clu from crossing into the real world.', NULL, NULL, '125', NULL, 0),
(12, 'tt1228705', 'Iron Man 2', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/ArqpkNYGfcTIA6umWt6xihfIZZv.jpg', '2010-04-28', 0, 'https://openload.co/f/b2T1t36VA_8/Iron.Man.II.x264.720p.mkv.mp4\r', NULL, 3, 'https://www.youtube.com/embed/BoohRoVA9WQ', 'HD', 6.6, 'With the world now aware of his dual life as the armored superhero Iron Man, billionaire inventor Tony Stark faces pressure from the government, the press and the public to share his technology with the military. Unwilling to let go of his invention, Stark, with Pepper Potts and James \'Rhodey\' Rhodes at his side, must forge new alliances â€“ and confront powerful enemies.', NULL, NULL, '124', NULL, 0),
(13, 'tt1014759', 'Alice in Wonderland', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/pvEE5EN5N1yjmHmldfL4aJWm56l.jpg', '2010-03-03', 0, 'https://openload.co/f/NsgQVkI-TrY/Alice.in.Wonderland.2010.BRRip.X264.AC3-PLAYNOW.mkv.mp4.mp4\r', NULL, 3, 'https://www.youtube.com/embed/pMiCJefpn9Q', 'HD', 6.4, 'Alice, an unpretentious and individual 19-year-old, is betrothed to a dunce of an English nobleman. At her engagement party, she escapes the crowd to consider whether to go through with the marriage and falls down a hole in the garden after spotting an unusual rabbit. Arriving in a strange and surreal place called \'Underland,\' she finds herself in a world that resembles the nightmares she had as a child, filled with talking animals, villainous queens and knights, and frumious bandersnatches. Alice realizes that she is there for a reason â€“ to conquer the horrific Jabberwocky and restore the rightful queen to her throne.', NULL, NULL, '108', NULL, 0),
(14, 'tt0926084', 'Harry Potter and the Deathly Hallows: Part 1', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/maP4MTfPCeVD2FZbKTLUgriOW4R.jpg', '2010-10-17', 0, 'https://openload.co/f/KYwPNbNWTzs/Harry.Potter.and.the.Deathly.Hallows.Part.1.2010.720p.BRRip.1.3GB.mkv.mp4\r', NULL, 3, 'https://www.youtube.com/embed/9hXH0Ackz6w', 'HD', 7.5, 'Harry, Ron and Hermione walk away from their last year at Hogwarts to find and destroy the remaining Horcruxes, putting an end to Voldemort\'s bid for immortality. But with Harry\'s beloved Dumbledore dead and Voldemort\'s unscrupulous Death Eaters on the loose, the world is more dangerous than ever.', NULL, NULL, '146', NULL, 0),
(15, 'tt1403865', 'True Grit', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/qc7vaF5fIeTllAykdP3wldlMyRh.jpg', '2010-12-22', 0, 'https://openload.co/f/w14sMtlKfxU/True.Grit.2010.BrRip.720p.x264.YIFY.mkv.mp4\r', NULL, 3, 'https://www.youtube.com/embed/unEO_p71cik', 'HD', 7.1, 'Following the murder of her father by hired hand Tom Chaney, 14-year-old farm girl Mattie Ross sets out to capture the killer. To aid her, she hires the toughest U.S. Marshal she can find, a man with \"true grit,\" Reuben J. \"Rooster\" Cogburn. Mattie insists on accompanying Cogburn, whose drinking, sloth, and generally reprobate character do not augment her faith in him. Against his wishes, she joins him in his trek into the Indian Nations in search of Chaney. They are joined by Texas Ranger LaBoeuf, who wants Chaney for his own purposes. The unlikely trio find danger and adventure on the journey, and each has his or her \"grit\" tested.', NULL, NULL, '110', NULL, 0),
(16, 'tt0964517', 'The Fighter', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/ytGIpdxYyvXwch5QILfquSTMA9f.jpg', '2010-12-17', 0, 'https://openload.co/f/3oM3pwNsJc0/The.Fighter.2010.BrRip.720p.YIFY.mkv.mp4\r', NULL, 3, 'https://www.youtube.com/embed/LRIZ4mCi5Ps', 'HD', 7.2, 'The Fighter, is a drama about boxer \"Irish\" Micky Ward\'s unlikely road to the world light welterweight title. His Rocky-like rise was shepherded by half-brother Dicky, a boxer-turned-trainer who rebounded in life after nearly being KO\'d by drugs and crime.', NULL, NULL, '116', NULL, 0),
(17, 'tt1504320', 'The King\'s Speech', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/nkFTbA1XjKWo9LCTaV1hV2Lsgr1.jpg', '2010-09-06', 0, 'https://openload.co/f/kqfqhNENsuY/The.Kings.Speech.2010.720p.BRRip.X264.AC3-PLAYNOW.mp4\r', NULL, 3, 'https://www.youtube.com/embed/kYoSQkfrjfA', 'HD', 7.5, 'The King\'s Speech tells the story of the man who became King George VI, the father of Queen Elizabeth II. After his brother abdicates, George (\'Bertie\') reluctantly assumes the throne. Plagued by a dreaded stutter and considered unfit to be king, Bertie engages the help of an unorthodox speech therapist named Lionel Logue. Through a set of unexpected techniques, and as a result of an unlikely friendship, Bertie is able to find his voice and boldly lead the country into war.', NULL, NULL, '118', NULL, 0),
(18, 'tt1375670', 'Grown Ups', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/t6Yj2UlqsrHHK6kJO1FWepR6mEd.jpg', '2010-06-24', 0, 'https://openload.co/f/bPL2Z0OzOM0/Grown_Ups_2010\r', NULL, 3, 'https://www.youtube.com/embed/bZMyY0WuwyY', 'HD', 6, 'After their high school basketball coach passes away, five good friends and former teammates reunite for a Fourth of July holiday weekend.', NULL, NULL, '102', NULL, 0),
(19, 'tt1282140', 'Easy A', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/1pgPO3OfSm2inpODMqg2RNmLW90.jpg', '2010-09-10', 0, 'https://openload.co/f/9oonnqbLlaI/Easy_A_2010\r', NULL, 3, 'https://www.youtube.com/embed/KNbPnqyvItk', 'HD', 6.7, 'After a little white lie about losing her virginity gets out, a clean cut high school girl sees her life paralleling Hester Prynne\'s in \"The Scarlet Letter,\" which she is currently studying in school - until she decides to use the rumor mill to advance her social and financial standing.', NULL, NULL, '92', NULL, 0),
(20, 'tt1323594', 'Despicable Me', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/4zHJhBSY4kNZXfhTlmy2TzXD51M.jpg', '2010-07-08', 0, 'https://openload.co/f/T67czFINK1Q/Despicable_Me_2010\r', NULL, 3, 'https://www.youtube.com/embed/RXZY_XRjABs', 'HD', 7.1, 'Villainous Gru lives up to his reputation as a despicable, deplorable and downright unlikable guy when he hatches a plan to steal the moon from the sky. But he has a tough time staying on task after three orphans land in his care.', NULL, NULL, '95', NULL, 0),
(21, 'tt1038686', 'Legion', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/mXJJPsVfQdf0OWxgoWgDUFyzJQo.jpg', '2010-01-21', 0, 'https://openload.co/f/5WGZQK4e24k/Legion.2010.mp4\r', NULL, 3, 'https://www.youtube.com/embed/P6p01-in6-k', 'HD', 5.1, 'When God loses faith in humankind, he sends his legion of angels to bring on the Apocalypse. Humanity\'s only hope for survival lies in a group of strangers trapped in an out-of-the-way, desert diner with the Archangel Michael.', NULL, NULL, '100', NULL, 0),
(22, 'tt0840361', 'The Town', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/zX4fKmDXKGt4hlzhAJirdlRKFgO.jpg', '2010-09-15', 0, 'https://openload.co/f/MNCrpwz50-U/The_Town_2010.mp4\r', NULL, 3, 'https://www.youtube.com/embed/WcXt9aUMbBk', 'HD', 7, 'Doug MacRay is a longtime thief, who, smarter than the rest of his crew, is looking for his chance to exit the game. When a bank job leads to the group kidnapping an attractive branch manager, he takes on the role of monitoring her â€“ but their burgeoning relationship threatens to unveil the identities of Doug and his crew to the FBI Agent who is on their case.', NULL, NULL, '125', NULL, 0),
(23, 'tt1542344', '127 Hours', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/c6Nu7UjhGCQtV16WXabqOQfikK6.jpg', '2010-11-05', 0, 'https://openload.co/f/iPp-5held5c/127.Hours.2010.720p.BrRip.x264.YIFY.mp4\r', NULL, 3, 'https://www.youtube.com/embed/OlhLOWTnVoQ', 'HD', 7, 'The true story of mountain climber Aron Ralston\'s remarkable adventure to save himself after a fallen boulder crashes on his arm and traps him in an isolated canyon in Utah.', NULL, NULL, '94', NULL, 0),
(24, 'tt0892769', 'How to Train Your Dragon', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/3itnFVfx74ccUayMcywRHR8LLSd.jpg', '2010-03-05', 0, 'https://openload.co/f/rLHJcvZkuqM/How_To_Train_Your_Dragon_2010\r', NULL, 3, 'https://www.youtube.com/embed/IT_ufPxiXl8', 'HD', 7.5, 'As the son of a Viking leader on the cusp of manhood, shy Hiccup Horrendous Haddock III faces a rite of passage: he must kill a dragon to prove his warrior mettle. But after downing a feared dragon, he realizes that he no longer wants to destroy it, and instead befriends the beast â€“ which he names Toothless â€“ much to the chagrin of his warrior father', NULL, NULL, '98', NULL, 0),
(25, 'tt0429493', 'The A-Team', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/eMwotYpkcE42lGlaoCpPFaolvst.jpg', '2010-06-10', 0, 'https://openload.co/f/QoOtPDEppDA/The_A_Team_2010\r', NULL, 3, 'https://www.youtube.com/embed/g0fSsvQ-bG8', 'HD', 6.2, 'A group of Iraq War veterans goes on the run from U.S. military forces while they try to clear their names after being framed for a crime they didn\'t commit. Along the way, Col. Hannibal Smith, Capt. H.M. â€˜Howling Madâ€™ Murdock , Sgt. Bosco â€˜B.A.â€™ Baracus, and Lt. Templeton â€˜Facemanâ€™ Peck help out various people they encounter.', NULL, NULL, '117', NULL, 0),
(26, 'tt1155076', 'The Karate Kid', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/l13emtM0cz0HyJeWSxb6ml5RvzT.jpg', '2010-06-10', 0, 'https://openload.co/f/KKon1p9KAp0/The_Karate_Kid_2010\r', NULL, 3, 'https://www.youtube.com/embed/2SmmxvHLsKk', 'HD', 6.1, '12-year-old Dre Parker could have been the most popular kid in Detroit, but his mother\'s latest career move has landed him in China. Dre immediately falls for his classmate Mei Ying but the cultural differences make such a friendship impossible. Even worse, Dre\'s feelings make him an enemy of the class bully, Cheng. With no friends in a strange land, Dre has nowhere to turn but maintenance man Mr. Han, who is a kung fu master. As Han teaches Dre that kung fu is not about punches and parries, but maturity and calm, Dre realizes that facing down the bullies will be the fight of his life.', NULL, NULL, '140', NULL, 0),
(27, 'tt1320253', 'The Expendables', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/y2qJoYxOhzyidsA60Mqn29H38Lk.jpg', '2010-08-03', 0, 'https://openload.co/f/v_U0nVdCs-8/The_Expendables_2010\r', NULL, 3, 'https://www.youtube.com/embed/C6RU5y2fU6s', 'HD', 6, 'Barney Ross leads a band of highly skilled mercenaries including knife enthusiast Lee Christmas, a martial arts expert, heavy weapons specialist, demolitionist, and a loose-cannon sniper. When the group is commissioned by the mysterious Mr. Church to assassinate the dictator of a small South American island, Barney and Lee visit the remote locale to scout out their opposition and discover the true nature of the conflict engulfing the city.', NULL, NULL, '103', NULL, 0),
(28, 'tt1037705', 'The Book of Eli', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/qL3FnEug9DyBcaBXVb0oT3DJMJu.jpg', '2010-01-14', 0, 'https://openload.co/f/iKQYVzPCdDI/The.Book.of.Elii.2010.mkv.mp4\r', NULL, 3, 'https://www.youtube.com/embed/yAQcwKY0Dik', 'HD', 6.6, 'A post-apocalyptic tale, in which a lone man fights his way across America in order to protect a sacred book that holds the secrets to saving humankind.', NULL, NULL, '118', NULL, 0),
(29, 'tt0938283', 'The Last Airbender', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/zgwRTYWEEPivTwjB9S03HtmMcbM.jpg', '2010-06-30', 0, 'https://openload.co/f/-eUx9wqiHu8/The.Last.Airbender.2010.720p.BRRip.X264.AC3-PLAYNOW.mp4\r', NULL, 3, 'https://www.youtube.com/embed/-egQ79OrYCs', 'HD', 4.7, 'The story follows the adventures of Aang, a young successor to a long line of Avatars, who must put his childhood ways aside and stop the Fire Nation from enslaving the Water, Earth and Air nations.', NULL, NULL, '103', NULL, 0),
(30, 'tt0473075', 'Prince of Persia: The Sands of Time', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/dtjz9Z6MTsuFyrzqdwVB4dgHENA.jpg', '2010-05-19', 0, 'https://openload.co/f/gVm6WClYe1M/Prince_of_Persia_2010\r', NULL, 3, 'https://www.youtube.com/embed/bZ7Li5w2I-k', 'HD', 6.2, 'A rogue prince reluctantly joins forces with a mysterious princess and together, they race against dark forces to safeguard an ancient dagger capable of releasing the Sands of Time â€“ gift from the gods that can reverse time and allow its possessor to rule the world.', NULL, NULL, '116', NULL, 0),
(31, 'tt1591095', 'Insidious', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/urAaOffVaxQzRJ8X5Z0oDqOWAjl.jpg', '2010-09-13', 0, 'https://openload.co/f/DZ-Lg23gYFM/Insidious_2010\r', NULL, 3, 'https://www.youtube.com/embed/E1YbOMDI59k', 'HD', 6.7, 'A family discovers that dark spirits have invaded their home after their son inexplicably falls into an endless sleep. When they reach out to a professional for help, they learn things are a lot more personal than they thought.', NULL, NULL, '103', NULL, 0),
(32, 'tt1120985', 'Blue Valentine', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/r8l15gDSQMupjKXEyXkOMkRpcsn.jpg', '2010-12-27', 0, 'https://openload.co/f/r5kXHHl1l6M/Blue.Valentine.720p.x264.YIFY.mkv.mp4\r', NULL, 3, 'https://www.youtube.com/embed/3oiY7W7nDeE', 'HD', 6.9, 'Dean and Cindy live a quiet life in a modest neighborhood. They appear to have the world at their feet at the outset of the relationship. However, his lack of ambition and her retreat into self-absorption cause potentially irreversible cracks in their marriage.', NULL, NULL, '112', NULL, 0),
(33, 'tt1261945', 'Sex and the City 2', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/pU8sKmxS5NorwjQc4OzW0zsoggA.jpg', '2010-05-26', 0, 'qNq69jbAhec', NULL, 3, 'https://www.youtube.com/embed/Djz0q-GeboM', 'HD', 5.4, 'Carrie, Charlotte, and Miranda are all married now, but they\'re still up for a little fun in the sun. When Samantha gets the chance to visit one of the most extravagant vacation destinations on the planet and offers to bring them all along, they surmise that a women-only retreat may be the perfect excuse to eschew their responsibilities and remember what life was like before they decided to settle down.', NULL, NULL, '146', NULL, 0),
(34, 'tt1220634', 'Resident Evil: Afterlife', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/wEgqGu01JtIyxXhYrr4hsgds1uZ.jpg', '2010-09-09', 0, 'https://openload.co/f/Mr6glO4bY6w/Resident_Evil_Afterlife_2010\r', NULL, 3, 'https://www.youtube.com/embed/9dc5iiT0f1s', 'HD', 5.8, 'In a world ravaged by a virus infection, turning its victims into the Undead, Alice continues on her journey to find survivors and lead them to safety. Her deadly battle with the Umbrella Corporation reaches new heights, but Alice gets some unexpected help from an old friend. A new lead that promises a safe haven from the Undead takes them to Los Angeles, but when they arrive the city is overrun by thousands of Undead - and Alice and her comrades are about to step into a deadly trap.', NULL, NULL, '97', NULL, 0),
(35, 'tt1023114', 'The Way Back', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/5lPRuICgmMmdrBr6qMNl80psEGo.jpg', '2010-09-03', 0, 'https://openload.co/f/ev5ViWSAqzk/The.Way.Back.2010.720p.x264.BOKUTOX.YIFY.mp4\r', NULL, 3, 'https://www.youtube.com/embed/87kezJTpyMI', 'HD', 6.8, 'Peter Weir\'s follow-up to Master &amp; Commander (2003) is the stark &amp; brilliant The Way Back, which takes on the theme of man\'s struggle for freedom. At the dawn of WWII, several men escape from a Russian gulag. The film details their perilous &amp; uncertain journey to freedom, as they cross deserts, mountains, &amp; several nations.', NULL, NULL, '133', NULL, 0),
(36, 'tt2382396', 'Joe', 2014, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/bvcLnoffCm67n7pZPVdJ6pluJsi.jpg', '2014-04-11', 0, 'https://openload.co/f/rEC-pBJaOAk/Winter%27s.Bone.2010720p.BRRip.H.264.AAC.mkv.mp4\r', NULL, 3, 'https://www.youtube.com/embed/3WPLVEUx5AU', 'HD', 6.6, 'The rough-hewn boss of a lumber crew courts trouble when he steps in to protect the youngest member of his team from an abusive father.', NULL, NULL, '118', NULL, 0),
(37, 'tt1242432', 'I Spit on Your Grave', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/7hA8FfUAwBJut4MSIhR6eCF8li7.jpg', '2010-06-17', 0, 'https://openload.co/f/xpLI9RzkI60/I_Spit_On_Your_Grave_2010\r', NULL, 3, 'https://www.youtube.com/embed/oOxSvFKrHew', 'HD', 6.2, 'A remake of the 1979 controversial cult classic, I Spit on Your Grave retells the horrific tale of writer Jennifer Hills who takes a retreat from the city to a charming cabin in the woods to start on her next book. But Jennifer\'s presence in the small town attracts the attention of a few morally deprived locals led by Johnny, the town\'s service station owner, his two co-workers, Andy and Stanley, who along with their socially and mentally challenged friend Matthew, set out one night to teach this city girl a lesson.', NULL, NULL, '108', NULL, 0),
(38, 'tt0944835', 'Salt', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/956xMjH4sPoqimqoLOP6AI19mjm.jpg', '2010-07-21', 0, 'https://openload.co/f/T_9ROGxt5ww/Salt_2010\r', NULL, 3, 'https://www.youtube.com/embed/QZ40WlshNwU', 'HD', 6.2, 'As a CIA officer, Evelyn Salt swore an oath to duty, honor and country. Her loyalty will be tested when a defector accuses her of being a Russian spy. Salt goes on the run, using all her skills and years of experience as a covert operative to elude capture. Salt\'s efforts to prove her innocence only serve to cast doubt on her motives, as the hunt to uncover the truth behind her identity continues and the question remains: \"Who is Salt?\"', NULL, NULL, '100', NULL, 0),
(39, 'tt1424381', 'Predators', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/5G6kook6pUGrGZ99ciD4a7L1CIs.jpg', '2010-07-03', 0, 'https://openload.co/f/D97MXbaGCVU/Predators_2010\r', NULL, 3, 'https://www.youtube.com/embed/1Er_EeF2Ozc', 'HD', 6, 'A mercenary reluctantly leads a motley crew of warriors who soon come to realize they\'ve been captured and deposited on an alien planet by an unknown nemesis. With the exception of a peculiar physician, they are all cold-blooded killers, convicts, death squad members... hunters who have now become the hunted.', NULL, NULL, '107', NULL, 0),
(40, 'tt1179891', 'My Bloody Valentine', 2009, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/lDGSEqem6XDtuXrBIM3jjXIPjZk.jpg', '2009-01-16', 0, 'https://openload.co/f/ECgvzcDm_xQ/Valentine%27s.Day.2010.720p.BrRip.264.YIFY.mp4\r', NULL, 3, 'https://www.youtube.com/embed/mPMRDqMWKQ4', 'HD', 5.2, 'Ten years ago, a tragedy changed the town of Harmony forever. Tom Hanniger, an inexperienced coal miner, caused an accident in the tunnels that trapped and killed five men and sent the only survivor, Harry Warden, into a permanent coma. But Harry Warden wanted revenge. Exactly one year later, on Valentineâ€™s Day, he woke upâ€¦and brutally murdered twenty-two people with a pickaxe before being killed.', NULL, NULL, '101', NULL, 0),
(41, 'tt1193631', 'Step Up 3D', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/q8Pm7UpAqDdxo1Xnt29EHHAl2u2.jpg', '2010-08-04', 0, 'https://openload.co/f/o2MGLlZ2ebU/Step.Up.3D.2010.720p.x264.YIFY.mp4\r', NULL, 3, 'https://www.youtube.com/embed/TZe1Qz2twbE', 'HD', 6.6, 'A tight-knit group of New York City street dancers, including Luke and Natalie, team up with NYU freshman Moose, and find themselves pitted against the world\'s best hip hop dancers in a high-stakes showdown that will change their lives forever.', NULL, NULL, '107', NULL, 0),
(42, 'tt1243957', 'The Tourist', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/jfLFF0Qa9jAoTYQIKaSWmMWcKZd.jpg', '2010-12-08', 0, 'https://openload.co/f/MnnuyoAwFvg/The_Tourist_2010_720p_Bluray_x264_anoXmous.mp4\r', NULL, 3, 'https://www.youtube.com/embed/GrqpKEOF8uw', 'HD', 6, 'American tourist Frank (Johnny Depp) meets mysterious British woman Elsie (Angelina Jolie) on the train to Venice. Romance seems to bud, but there\'s more to her than meets the eye. Remake of the 2005 French film \"Anthony Zimmer\", written and directed by JÃ©rÃ´me Salle.', NULL, NULL, '103', NULL, 0),
(43, 'tt0780653', 'The Wolfman', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/igiSz8bGHE0BegZ8xvlokAQgAk3.jpg', '2010-02-11', 0, 'https://openload.co/f/6KpitaJJGLU/The.Wolfman.2010.720p.x264.aac.mkv.mp4\r', NULL, 3, 'https://www.youtube.com/embed/NS3nTkvvHi8', 'HD', 5.5, 'Lawrence Talbot, an American man on a visit to Victorian London to make amends with his estranged father, gets bitten by a werewolf and, after a moonlight transformation, leaves him with a savage hunger for flesh.', NULL, NULL, '102', NULL, 0),
(44, 'tt0963966', 'The Sorcerer\'s Apprentice', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/ZNSrRpdJ6FjMy4Iu6JpRTJ5Q87.jpg', '2010-07-13', 0, 'https://openload.co/f/kLUHBdqCm8I/The.Sorcerers.Apprentice.2010.720p.mkv.mp4\r', NULL, 3, 'https://www.youtube.com/embed/v2uV0_1C4UM', 'HD', 5.8, 'Balthazar Blake is a master sorcerer in modern-day Manhattan trying to defend the city from his arch-nemesis, Maxim Horvath. Balthazar can\'t do it alone, so he recruits Dave Stutler, a seemingly average guy who demonstrates hidden potential, as his reluctant protÃ©gÃ©. The sorcerer gives his unwilling accomplice a crash course in the art and science of magic, and together, these unlikely partners work to stop the forces of darkness.', NULL, NULL, '109', NULL, 0),
(45, 'tt1325004', 'The Twilight Saga: Eclipse', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/fBXJGA6WNran1RL4CPKisaPI6UY.jpg', '2010-06-23', 0, 'https://openload.co/f/S3AABeISNGU/The_Twilight_Saga_Eclipse_2010.mp4\r', NULL, 3, 'https://www.youtube.com/embed/hZxJzhIPTTg', 'HD', 5.8, 'Bella once again finds herself surrounded by danger as Seattle is ravaged by a string of mysterious killings and a malicious vampire continues her quest for revenge. In the midst of it all, she is forced to choose between her love for Edward and her friendship with Jacob, knowing that her decision has the potential to ignite the ageless struggle between vampire and werewolf. With her graduation quickly approaching, Bella is confronted with the most important decision of her life.', NULL, NULL, '124', NULL, 0),
(46, 'tt1231587', 'Hot Tub Time Machine', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/zERxFAXiQdPAfsumIIcBFH8fOVt.jpg', '2010-03-26', 0, 'https://openload.co/f/YHmG8aEdRP0/Hot_Tub_Time_Machine_2010\r', NULL, 3, 'https://www.youtube.com/embed/HszVQgs4e_c', 'HD', 6, 'A malfunctioning time machine at a ski resort takes a man back to 1986 with his two friends and nephew, where they must relive a fateful night and not change anything to make sure the nephew is born.', NULL, NULL, '101', NULL, 1),
(47, 'tt1038919', 'The Bounty Hunter', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/lAuOwnlWrBOOiWGpS3wqhfF4CRx.jpg', '2010-03-16', 0, 'https://openload.co/f/OTvVkgefNyo/The_Bounty_Hunter_2010\r', NULL, 3, 'https://www.youtube.com/embed/tOGRLdlWHT4', 'HD', 5.5, 'Milo Boyd is a bounty hunter whose latest gig is rather satisfying, as he finds out that the bail-skipper he must chase down is his own ex-wife, Nicole -- but she has no intention of getting nabbed without a fight. Complicating matters, Nicole\'s wannabe-boyfriend, Stewart, joins the chase.', NULL, NULL, '110', NULL, 0),
(48, 'tt0947810', 'Green Zone', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/7O3IkeHvp1sq5yMN0O8FsTee1C0.jpg', '2010-03-11', 0, 'https://openload.co/f/Ul9KHUiDEac/Green.Zone.2010.720p.BRRip.mp4\r', NULL, 3, 'https://www.youtube.com/embed/e3KJ21TLKVE', 'HD', 6.3, 'During the U.S.-led occupation of Baghdad in 2003, Chief Warrant Officer Roy Miller and his team of Army inspectors were dispatched to find weapons of mass destruction believed to be stockpiled in the Iraqi desert. Rocketing from one booby-trapped and treacherous site to the next, the men search for deadly chemical agents but stumble instead upon an elaborate cover-up that threatens to invert the purpose of their mission.', NULL, NULL, '115', NULL, 0),
(49, 'tt1075747', 'Jonah Hex', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/fhpl0NYcwQ8oUmur30L0ANOay4L.jpg', '2010-06-18', 0, 'https://openload.co/f/MGxWIIQFgWo/Jonah.Hex.2010.720p.BrRip.x264.mp4\r', NULL, 3, 'https://www.youtube.com/embed/3Ppqbnhp7PI', 'HD', 4.4, 'Gunsliger Jonah Hex (Josh Brolin) is appointed by President Ulysses Grant to track down terrorist Quentin Turnbull (John Malkovich), a former Confederate officer determined on unleashing hell on earth. Jonah not only secures freedom by accepting this task, he also gets revenge on the man who slayed his wife and child. Megan Fox plays a prostitute as well as Jonah Hex\'s love interst in the film.', NULL, NULL, '80', NULL, 0),
(50, 'tt1477076', 'Saw 3D', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/mBQX4TCfDEk2a5mvu0Z0PFeFoUp.jpg', '2010-10-21', 0, 'https://openload.co/f/Yg9TsqIAY-c/Saw.VII.2010.mp4\r', NULL, 3, 'https://www.youtube.com/embed/xC7yCELivB8', 'HD', 5.8, 'As a deadly battle rages over Jigsaw\'s brutal legacy, a group of Jigsaw survivors gathers to seek the support of self-help guru and fellow survivor Bobby Dagen, a man whose own dark secrets unleash a new wave of terror.', NULL, NULL, '90', NULL, 0),
(51, 'tt0892791', 'Shrek Forever After', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/enqVwbUu6OX5LHq0fuk7sLT35zy.jpg', '2010-05-16', 0, 'https://openload.co/f/83J0L6rJVds/Shrek.4.2010.720p.BluRay.x264.YIFY.mp4\r', NULL, 3, 'https://www.youtube.com/embed/WV8bbMxPBdc', 'HD', 6, 'A bored and domesticated Shrek pacts with deal-maker Rumpelstiltskin to get back to feeling like a real ogre again, but when he\'s duped and sent to a twisted version of Far Far Awayâ€”where Rumpelstiltskin is king, ogres are hunted, and he and Fiona have never metâ€”he sets out to restore his world and reclaim his true love.', NULL, NULL, '93', NULL, 0),
(52, 'tt2267998', 'Gone Girl', 2014, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/gdiLTof3rbPDAmPaCf4g6op46bj.jpg', '2014-10-01', 0, 'qlBcUJ1kjT0', NULL, 3, 'https://www.youtube.com/embed/JU9PdgB3rsk', 'HD', 7.9, 'With his wife\'s disappearance having become the focus of an intense media circus, a man sees the spotlight turned on him when it\'s suspected that he may not be innocent.', NULL, NULL, '145', 'https://thumb.oloadcdn.net/splash/qlBcUJ1kjT0/lsIALI7OT_A.jpg', 0),
(53, 'tt2802144', 'Kingsman: The Secret Service', 2014, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/8x7ej0LnHdKUqilNNJXYOeyB6L9.jpg', '2014-06-27', 0, 'CTsvBiWwbNw', NULL, 3, 'https://www.youtube.com/embed/kl8F-8tR8to', 'HD', 7.6, 'The story of a super-secret spy organization that recruits an unrefined but promising street kid into the agency\'s ultra-competitive training program just as a global threat emerges from a twisted tech genius.', NULL, NULL, '130', 'https://thumb.oloadcdn.net/splash/CTsvBiWwbNw/VxS-JuIwBYA.jpg', 0),
(54, 'tt1288558', 'Evil Dead', 2013, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/z56CMoFXOhq4zieKMsokLaiB4qE.jpg', '2013-04-05', 0, 'kePeWlNu0UU', NULL, 3, 'https://www.youtube.com/embed/FKFDkpHCQz4', 'HD', 6.4, 'Evil Dead, the fourth installment of the Evil Dead franchise, serving as both a reboot and as a loose continuation of the series, features Mia, a young woman struggling with sobriety, heads to a remote cabin with a group of friends where the discovery of a Book of the Dead unwittingly summon up dormant demons which possess the youngsters one by one.', NULL, NULL, '91', 'https://thumb.oloadcdn.net/splash/kePeWlNu0UU/wVHUP9lSbPw.jpg', 0),
(55, 'tt0120689', 'The Green Mile', 1999, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/3yJUlOtVa09CYJocwBU8eAryja0.jpg', '1999-12-10', 0, 'uQlo6Z0b0V4', NULL, 3, 'https://www.youtube.com/embed/ctRK-4Vt7dA', 'HD', 8.2, 'A supernatural tale set on death row in a Southern prison, where gentle giant John Coffey possesses the mysterious power to heal people\'s ailments. When the cellblock\'s head guard, Paul Edgecomb, recognizes Coffey\'s miraculous gift, he tries desperately to help stave off the condemned man\'s execution.', NULL, NULL, '189', 'https://thumb.oloadcdn.net/splash/uQlo6Z0b0V4/Vk3vG7QrA-U.jpg', 0),
(56, 'tt0218922', 'Original Sin', 2001, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/i8FEQy5IWAqOzXm4uDHy2r3Swym.jpg', '2001-08-03', 0, 'vmojTB4Rrao', NULL, 3, 'https://www.youtube.com/embed/', 'HD', 5.8, 'A young man is plunged into a life of subterfuge, deceit and mistaken identity in pursuit of a femme fatale whose heart is never quite within his grasp', NULL, NULL, '118', 'https://thumb.oloadcdn.net/splash/vmojTB4Rrao/arEY4-0-88A.jpg', 0),
(57, 'tt0183790', 'A Knight\'s Tale', 2001, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/1ZUpkMivvlF0ZoyXQzHF4El3izv.jpg', '2001-05-11', 0, 'wxKhw_w5Cl0', NULL, 3, 'https://www.youtube.com/embed/yw7PQDsCC4Y', 'HD', 6.6, 'William Thatcher, a peasant, is sent to apprentice with a Knight named Hector as a young boy. Urged by his father to \"change his Stars\", he assumes Sir Hector\'s place in a tournament when Hector dies in the middle of it. He wins. With the other apprentices, he trains and assumes the title of Sir Ulrich von Lichtenstein.', NULL, NULL, '132', NULL, 0),
(58, 'tt1117563', 'Batman: Gotham Knight', 2008, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/eIAhXROHG8t3QQ7qU0HfZgL5XFf.jpg', '2008-07-03', 0, '3rbXKc_3j3E', NULL, 3, 'https://www.youtube.com/embed/0beX8b4zX5E', 'HD', 6.7, 'Explore Bruce Wayne\'s transition from his beginning as a tormented vigilantee to The Dark Knight of a crumbling metropolis with six distinct chapters but intended to be viewed as a whole.', NULL, NULL, '75', 'https://thumb.oloadcdn.net/splash/3rbXKc_3j3E/nnZR70FvbHk.jpg', 0),
(59, 'tt0402057', 'Beowulf & Grendel', 2005, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/rvM310iHr34K0josqCmRVUev7FP.jpg', '2005-09-14', 0, 'fnKuPR5iPbk', NULL, 3, 'https://www.youtube.com/embed/TGlSAtb-SDw', 'HD', 6.2, 'The blood-soaked tale of a Norse warrior\'s battle against the great and murderous troll, Grendel. Heads will roll. Out of allegiance to the King Hrothgar, the much respected Lord of the Danes, Beowulf leads a troop of warriors across the sea to rid a village of the marauding monster.', NULL, NULL, '103', 'https://thumb.oloadcdn.net/splash/fnKuPR5iPbk/G7sabOg-ozo.jpg', 0),
(60, 'tt0481141', 'No Reservations', 2007, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/qBVLHY48fwij7T8iViqpAG7fU2q.jpg', '2007-07-25', 0, 'RZYkhXlysvI', NULL, 3, 'https://www.youtube.com/embed/2-5RJTf0-Jk', 'HD', 6, 'Master chef Kate Armstrong runs her life and her kitchen with intimidating intensity. However, a recipe for disaster may be in the works when she becomes the guardian of her young niece while crossing forks with the brash sous-chef who just joined her staff. Though romance blooms in the face of rivalry, Kate needs to look outside the kitchen to find true happiness.', NULL, NULL, '104', 'https://thumb.oloadcdn.net/splash/RZYkhXlysvI/-cpnTRdj3qI.jpg', 0),
(61, 'tt2381991', 'The Huntsman: Winter\'s War', 2016, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/b77l5vmp6PYsc98LE6Uf1mXtmHh.jpg', '2016-04-06', 0, 'bjwZoIEAc5U', NULL, 3, 'https://www.youtube.com/embed/V9cPxenX1-0', 'HD', 5.9, 'As two evil sisters prepare to conquer the land, two renegades - Eric the Huntsman - who aided Snow White in defeating Ravenna in Snowwhite and the Huntsman, and his forbidden lover, Sara, set out to stop them.', NULL, NULL, '114', 'https://thumb.oloadcdn.net/splash/bjwZoIEAc5U/WF9-2MK40Lc.jpg', 0),
(62, 'tt0384537', 'Silent Hill', 2006, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/q1Y14VSFA4As5pWRFQ51yrqyG4M.jpg', '2006-04-21', 0, 'D3F8g1rbWjA', NULL, 3, 'https://www.youtube.com/embed/WWMGZe6iucw', 'HD', 6.3, 'The eerie and deserted ghost town of Silent Hill draws a young mother desperate to find a cure for her only child\'s illness. Unable to accept the doctor\'s diagnosis that her daughter should be permanently institutionalized for psychiatric care, Rose flees with her child, heading for the abandoned town in search of answers â€“ and ignoring the protests of her husband. It\'s soon clear this place is unlike anywhere she\'s ever been. It\'s smothered by fog, inhabited by a variety of strange beings and periodically overcome by a living \'darkness\' that literally transforms everything it touches. As Rose searches for her little girl, she begins to learn the history of the strange town and realizes that her daughter is just a pawn in a larger game.', NULL, NULL, '125', 'https://thumb.oloadcdn.net/splash/D3F8g1rbWjA/fSua2_08YSw.jpg', 0),
(63, 'tt0486551', 'Beerfest', 2006, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/2u4eGTXiY0yDhDahJEJ0DUb16hx.jpg', '2006-08-25', 0, 'x9fbaW_5OB8', NULL, 3, 'https://www.youtube.com/embed/4ZBJxhjqUgs', 'HD', 5.8, 'During a trip to Germany to scatter their grandfather\'s ashes, German-American brothers Todd and Jan discover Beerfest, the secret Olympics of downing stout, and want to enter the contest to defend their family\'s beer-guzzling honor. Their Old Country cousins sneer at the Yanks\' chances, prompting the siblings to return to America to prepare for a showdown the following year.', NULL, NULL, '110', 'https://thumb.oloadcdn.net/splash/x9fbaW_5OB8/fzlwEB8hfAg.jpg', 0),
(64, 'tt0247745', 'Super Troopers', 2001, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/b1SZHbN37wlYzCV9BqloYLHNxFA.jpg', '2001-01-18', 0, 'S5tafWo1UsU', NULL, 3, 'https://www.youtube.com/embed/v4AE-ZoZarc', 'HD', 6.6, 'Five bored, occasionally high and always ineffective Vermont state troopers must prove their worth to the governor or lose their jobs. After stumbling on a drug ring, they plan to make a bust, but a rival police force is out to steal the glory.', NULL, NULL, '100', 'https://thumb.oloadcdn.net/splash/S5tafWo1UsU/Rq0VIoWXltA.jpg', 0),
(65, 'tt0456554', 'Grandma\'s Boy', 2006, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/9Z0Q9uIH4il75dfPVqFhVKljfY.jpg', '2006-01-06', 0, 'xGyyzutlRsI', NULL, 3, 'https://www.youtube.com/embed/sEA_g1UK64k', 'HD', 6.6, 'Even though he\'s 35, Alex acts more like he\'s 13, spending his days as the world\'s oldest video game tester and his evenings developing the next big Xbox game. But he gets kicked out of his apartment and is forced to move in with his grandmother.', NULL, NULL, '94', 'https://thumb.oloadcdn.net/splash/xGyyzutlRsI/EhrUXkOZLNU.jpg', 0),
(66, 'tt1213663', 'The World\'s End', 2013, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/9DBKqINsayQRZU0Ry0HFjeps5XJ.jpg', '2013-07-18', 0, 'yy3aiC2iJCY', NULL, 3, 'https://www.youtube.com/embed/n__1Y-N5tQk', 'HD', 6.6, 'Five friends who reunite in an attempt to top their epic pub crawl from 20 years earlier unwittingly become humankind\'s only hope for survival.', NULL, NULL, '109', 'https://thumb.oloadcdn.net/splash/yy3aiC2iJCY/tuq3O3DTn5w.jpg', 0),
(67, 'tt1503769', 'Collapse', 2009, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/cP3bB1fDAWPXZ7HIo29MuYqNgpE.jpg', '2009-09-12', 0, '567s_ojPUws', NULL, 3, 'https://www.youtube.com/embed/', 'HD', 7.7, 'From the acclaimed director of American Movie, the documentary follows former Los Angeles police officer turned independent reporter Michael Ruppert. He recounts his career as a radical thinker and spells out his apocalyptic vision of the future, spanning the crises in economics, energy, environment and more.', NULL, NULL, '82', 'https://thumb.oloadcdn.net/splash/567s_ojPUws/FBAGxWo9hlk.jpg', 0),
(68, 'tt2558022', 'Bunker of the Dead', 2016, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/At1o5gVj8YjiheJ9mCKtnDawRWR.jpg', '2016-02-18', 0, 'trFtjT0bm3c', NULL, 3, 'https://www.youtube.com/embed/', 'HD', 4, 'After Using the instructions found in an old Jewish diary,  Markus and Thomas hope to gain access to a WW II underground military base formerly codenamed: Cerusit. It was used by the Nazis as a secret research institute and is shrouded in rumors to this day about the lost gold of the Third Reich. The entrance to the cave system, however, lies right within the restricted area of a U.S. military base.. the first of many problems Markus and Thomas will have to face.', NULL, NULL, '76', 'https://thumb.oloadcdn.net/splash/trFtjT0bm3c/iJpesTZEB8Q.jpg', 1),
(69, 'tt0475290', 'Hail, Caesar!', 2016, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/rzMYZYphL9qrbBClQq1Jc0zPMmY.jpg', '2016-02-05', 0, 'YLVQF6QPhJA', NULL, 3, 'https://www.youtube.com/embed/kMqeoW3XRa0', 'HD', 5.7, 'Tells the comedic tale of Eddie Mannix, a fixer who worked for the Hollywood studios in the 1950s. The story finds him at work when a star mysteriously disappears in the middle of filming.', NULL, NULL, '106', 'https://thumb.oloadcdn.net/splash/YLVQF6QPhJA/AFTxwyFqBoo.jpg', 0),
(70, 'tt0430304', 'Little Man', 2006, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/v22zmdXFW44P9l06cH8WCJTYxox.jpg', '2006-08-31', 0, 'dyH4jjUlWK0', NULL, 3, 'https://www.youtube.com/embed/x9STUnqrE_c', 'HD', 5.2, 'After leaving the prison, the dwarf criminal Calvin Sims joins to his moron brother Percy to steal an expensive huge diamond in a jewelry for the mobster Walken. They are chased by the police, and Calvin hides the stone in the purse of the executive Vanessa Edwards, whose husband Darryl Edwards wants to have a baby. Percy convinces Calvin to dress like a baby and be left in front of the Edwards\'s house to get inside the house and retrieve the diamond. Darryl and Vanessa keep Calvin for the weekend and decide to adopt him, while Walken threatens Darryl to get the stone back.', NULL, NULL, '98', 'https://thumb.oloadcdn.net/splash/dyH4jjUlWK0/7lTvxu-1eN8.jpg', 0),
(71, 'tt2217859', 'Louder Than Bombs', 2015, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/rnbs1o9UXScWCP2VqScZqPr4IwN.jpg', '2015-05-18', 0, 'XhbjvCbr63I', NULL, 3, 'https://www.youtube.com/embed/4I1l_J9QuVk', 'HD', 6.6, 'Three years after his wife, acclaimed photographer Isabelle Reed, dies in a car crash, Gene keeps everyday life going with his shy teenage son, Conrad. A planned exhibition of Isabelleâ€™s photographs prompts Gene\'s older son, Jonah, to return to the house he grew up in - and for the first time in a very long time, the father and the two brothers are living under the same roof.', NULL, NULL, '109', 'https://thumb.oloadcdn.net/splash/XhbjvCbr63I/sxtIzLwvRgU.jpg', 0),
(72, 'tt2091935', 'Mr. Right', 2016, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/y1VT2NoBOx3aC2exhkyN9AGUkMR.jpg', '2016-02-29', 0, 'ue26vS6wj-Q', NULL, 3, 'https://www.youtube.com/embed/FVxchAORxEk', 'HD', 6.3, 'A girl falls for the \"perfect\" guy, who happens to have a very fatal flaw: he\'s a hitman on the run from the crime cartels who employ him.', NULL, NULL, '95', 'https://thumb.oloadcdn.net/splash/ue26vS6wj-Q/yK0Vtc63SSo.jpg', 1),
(73, 'tt0100339', 'æ©Ÿå‹•è­¦å¯Ÿãƒ‘ãƒˆãƒ¬ã‚¤ãƒãƒ¼ åŠ‡å ´ç‰ˆ', 1989, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/uX58ae3iJNdLjPsI2xIgA0sERpM.jpg', '1989-01-01', 0, 'rYWUcnCxxok', NULL, 3, 'https://www.youtube.com/embed/', 'HD', 6.4, 'A mysterious suicide and a series of unmanned robot run-aways sparks off a Police investigation into the suspicious software on-board thousands of industrial robots around Tokyo.', NULL, NULL, '100', 'https://thumb.oloadcdn.net/splash/rYWUcnCxxok/BhDeK_e5itI.jpg', 0),
(74, 'tt0088128', 'Sixteen Candles', 1984, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/rzTrRQg5ek47Yl0Vfenc69r9gOd.jpg', '1984-05-04', 0, 'Ku3UtprbEBk', NULL, 3, 'https://www.youtube.com/embed/pnF4IyCSNS4', 'HD', 6.9, 'Samantha\'s life is going downhill fast. The sixteen-year-old has a crush on the most popular boy in school, and the geekiest boy in school has a crush on her. Her sister\'s getting married, and with all the excitement the rest of her family forgets her birthday! Add all this to a pair of horrendously embarrassing grandparents, a foreign exchange student named Long Duk Dong, and we have the makings of a hilarious journey into young womanhood.', NULL, NULL, '93', 'https://thumb.oloadcdn.net/splash/Ku3UtprbEBk/SlWVXRKHaPc.jpg', 0),
(75, 'tt0105151', 'The Player', 1992, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/g20oPyKrX317mRpOV5skpdQqf3p.jpg', '1992-04-03', 0, 'cyBLbml-FJQ', NULL, 3, 'https://www.youtube.com/embed/HpDDTS08wPs', 'HD', 6.9, 'A Hollywood studio executive is being sent death threats by a writer whose script he rejected - but which one?', NULL, NULL, '124', 'https://thumb.oloadcdn.net/splash/cyBLbml-FJQ/8hAUvs9vuz0.jpg', 0),
(76, 'tt0111309', 'The Sum of Us', 1994, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/rAQAUf6HHUyoT2Ii6jUmzMu5Ppf.jpg', '1994-07-12', 0, '5BbZG7MhcUU', NULL, 3, 'https://www.youtube.com/embed/rBGjUQJ_1Dc', 'HD', 5.9, '\"The Sum of Us\" is an Aussie story about a father and a son both searching for love and sharing an unconventional bond. Harry, the father, is the caring and open-minded \"mate\" that borders on annoyance. His son Jeff unsuccessfully searches for love, with the unwanted guidance of his father.', NULL, NULL, '100', 'https://thumb.oloadcdn.net/splash/5BbZG7MhcUU/DC_WLqsTIIA.jpg', 0),
(77, 'tt0102043', 'Hired to Kill', 1990, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/7u42URPMywYJPK5T0rLgSMianoN.jpg', '1990-11-10', 0, 'RCelpnykS6I', NULL, 3, 'https://www.youtube.com/embed/0ySw1wC391k', 'HD', 5.1, 'A fashion photographer and seven models travel to a South American island fortress, ostensibly to do a fashion shoot. In reality, the photographer is a mercenary and their job is to free an imprisoned rebel leader', NULL, NULL, '91', 'https://thumb.oloadcdn.net/splash/RCelpnykS6I/12x3XgMIKDQ.jpg', 0),
(78, 'tt1680051', 'Alyce Kills', 2011, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/xb9TePlFyj2XQqZECsokiIW2avE.jpg', '2011-09-14', 0, 'T6BBucHwqng', NULL, 3, 'https://www.youtube.com/embed/-ZVtWYaPiTg', 'HD', 5.6, 'After accidentally knocking her best friend off a roof, Alyce is haunted by guilt and delves into a brutal nightmare wonderland of sex, drugs and violence, her mind tearing itself apartâ€¦ along with anyone else who gets in her way.', NULL, NULL, '90', 'https://thumb.oloadcdn.net/splash/T6BBucHwqng/eksR051-gs8.jpg', 0);
INSERT INTO `flixymovies` (`id`, `imdbID`, `name`, `Year`, `pic`, `Released`, `olstatus`, `openload`, `streamango`, `Status`, `trailer`, `Quality`, `Rating`, `Plot`, `Actors`, `Genre`, `Runtime`, `splash`, `views`) VALUES
(79, 'tt2260254', 'The Other Side of the Mirror', 2016, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/9JL7C90I1HxS0UKMCSXaQCWkrMv.jpg', '2016-05-03', 0, 'Ro1eEghqQ_A', NULL, 3, 'https://www.youtube.com/embed/1yA9fJlgR0o', 'HD', 4.2, 'In 1905, amidst the largest drug epidemic in American history, a teenage Alice has just moved to the Pacific Northwest. She follows a mysterious man down a rabbit hole, leading her into Wonderland; a dark and curious world inhabited by characters from turn-of-the-century America and the Pacific Northwest.', NULL, NULL, '123', 'https://thumb.oloadcdn.net/splash/Ro1eEghqQ_A/S2hIQWDNe9A.jpg', 1),
(80, 'tt4438848', 'Neighbors 2: Sorority Rising', 2016, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/bWUeJHbKIyT306WtJFRHoSzX9nk.jpg', '2016-05-04', 0, 'ZOV3ACtHsOI', NULL, 3, 'https://www.youtube.com/embed/X2i9Zz_AqTg', 'HD', 5.6, 'A sorority moves in next door to the home of Mac and Kelly Radner who have a young child. The Radner\'s enlist their former nemeses from the fraternity to help battle the raucous sisters.', NULL, NULL, '91', 'https://thumb.oloadcdn.net/splash/ZOV3ACtHsOI/Br7qUGGFsUA.jpg', 1),
(81, 'tt1462758', 'Buried', 2010, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/pa4rzk8Hfx6i6Y99Ybjt4TCS3DW.jpg', '2010-09-24', 0, 'zKD-80fOl3Q', NULL, 3, 'https://www.youtube.com/embed/4CCXSG1WSwY', 'HD', 6.6, 'Paul is a U.S. truck driver working in Iraq. After an attack by a group of Iraqis he wakes to find he is buried alive inside a coffin. With only a lighter and a cell phone it\'s a race against time to escape this claustrophobic death trap.', NULL, NULL, '94', 'https://thumb.oloadcdn.net/splash/zKD-80fOl3Q/8z8QRDgdzO0.jpg', 0),
(82, 'tt4005402', 'Colonia', 2016, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/buEhROtfCEFNhjPWdJVgeVnrThj.jpg', '2016-02-18', 0, 'wGJgFzCJNhs', NULL, 3, 'https://www.youtube.com/embed/SiVAGDTmlpY', 'HD', 7.3, 'A young woman\'s desperate search for her abducted boyfriend that draws her into the infamous Colonia Dignidad, a sect nobody ever escaped from.', NULL, NULL, '120', 'https://thumb.oloadcdn.net/splash/wGJgFzCJNhs/6LICz4D0MKg.jpg', 0),
(83, 'tt2294449', '22 Jump Street', 2014, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/gNlV5FhDZ1PjxSv2aqTPS30GEon.jpg', '2014-06-05', 0, 'chG2e6f4kWI', NULL, 3, 'https://www.youtube.com/embed/i3_gKCX4Cik', 'HD', 7, 'After making their way through high school (twice), big changes are in store for officers Schmidt and Jenko when they go deep undercover at a local college. But when Jenko meets a kindred spirit on the football team, and Schmidt infiltrates the bohemian art major scene, they begin to question their partnership. Now they don\'t have to just crack the case - they have to figure out if they can have a mature relationship. If these two overgrown adolescents can grow from freshmen into real men, college might be the best thing that ever happened to them.', NULL, NULL, '112', 'https://thumb.oloadcdn.net/splash/chG2e6f4kWI/I9xBX93omSQ.jpg', 0),
(84, 'tt0105399', 'åŒé¾™ä¼š', 1992, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/vl7aI8rWk9MoRp9j6VagrRZFxRM.jpg', '1992-01-15', 0, '2p_j1v47nSI', NULL, 3, 'https://www.youtube.com/embed/2sNVVr4AfMU', 'HD', 6.2, 'Twins, separated at birth, end up as a Hong Kong gangster and a New York concert pianist. When the pianist travels to Hong Kong for a concert, the two inevitably get mistaken for each other.', NULL, NULL, '89', 'https://thumb.oloadcdn.net/splash/2p_j1v47nSI/vBoopiLoWDE.jpg', 0),
(85, 'tt1226271', 'The Damned United', 2009, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/h9xozWgif2RpKbJ7T6M0VSrAguV.jpg', '2009-03-27', 0, 'gmqsHmWkhNg', NULL, 3, 'https://www.youtube.com/embed/PUKKbJdZdSo', 'HD', 7.2, 'Taking over Leeds United, Brian Clough\'s abrasive approach and his clear dislike of the players\' dirty style of play make it certain there is going to be friction. Glimpses of his earlier career help explain both his hostility to previous manager Don Revie and how much he is missing right-hand man Peter Taylor', NULL, NULL, '97', 'https://thumb.oloadcdn.net/splash/gmqsHmWkhNg/NxBJDTzNAig.jpg', 0),
(86, 'tt0481536', 'Harold & Kumar Escape from Guantanamo Bay', 2008, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/hfyCKWEegVSEM40WTLGdJWAVqNU.jpg', '2008-04-25', 0, 'ej0i_nT249Q', NULL, 3, 'https://www.youtube.com/embed/Omt3tpgKDsA', 'HD', 6.2, 'Having satisfied their urge for White Castle, Harold and Kumar jump on a plane to catch up with Harold\'s love interest, who\'s headed for the Netherlands. But the pair must change their plans when Kumar is accused of being a terrorist. Rob Corddry also stars in this wild comedy sequel that follows the hapless stoners\' misadventures as they try to avoid being captured by the Department of Homeland Security.', NULL, NULL, '107', 'https://thumb.oloadcdn.net/splash/ej0i_nT249Q/YTUOHgqZKps.jpg', 0),
(87, 'tt4763168', 'Kindergarten Cop 2', 2016, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/1bnUlEiZcnJvr9znpl8HbruErIb.jpg', '2016-05-17', 0, '_ya3nxH0_4c', NULL, 3, 'https://www.youtube.com/embed/PFZajujTqoE', 'HD', 4.5, 'Assigned to recover sensitive stolen data, a gruff FBI agent goes undercover as a kindergarten teacher, but the school\'s liberal, politically correct environment is more than he bargained for.', NULL, NULL, '100', 'https://thumb.oloadcdn.net/splash/_ya3nxH0_4c/qaW9TVc_H84.jpg', 0),
(88, 'tt4616014', 'Jarhead 3: The Siege', 2016, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/dMSGHERrKfbKqRwhQNb2oqYJvO6.jpg', '2016-01-20', 0, 'EOHCuOfSayo', NULL, 3, 'https://www.youtube.com/embed/', 'HD', 5.3, 'Corporal Evan Albright (Charlie Weber) joined the elite Marine Corps Security Guards to save the world and see some action-not necessarily in that order. But his first assignment, protecting a U. S. Embassy in a seemingly safe Middle Eastern capitol, relegates his unit to wrangling \"gate groupies\" protesting outside the compound and honing their marksmanship by playing video games. So Albright and his team are caught off guard when well-armed and well-trained militants launch a surprise attack aimed at killing an informant in the embassy. Heavily out-gunned, they will have to muster all the courage and fire power they can as their once routine assignment spirals into all-out war.', NULL, NULL, '95', 'https://thumb.oloadcdn.net/splash/EOHCuOfSayo/hgMKrRdaOuI.jpg', 0),
(89, 'tt2702724', 'The Boss', 2016, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/w8smnnIVlEeFwD4Z0pKPC3RN2xT.jpg', '2016-04-07', 0, 'TQagAPXGRqA', NULL, 3, 'https://www.youtube.com/embed/V8GAMZqJqjs', 'HD', 5.4, 'A titan of industry is sent to prison after she\'s caught for insider trading. When she emerges ready to rebrand herself as America\'s latest sweetheart, not everyone she screwed over is so quick to forgive and forget.', NULL, NULL, '91', 'https://thumb.oloadcdn.net/splash/TQagAPXGRqA/ocMVZUEFDoE.jpg', 0),
(90, 'tt2093991', 'Elvis & Nixon', 2016, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/367GHvPaAkK7uPZR1AhZwULnu0x.jpg', '2016-04-22', 0, '7VYUydmdlVE', NULL, 3, 'https://www.youtube.com/embed/o9x3Z6b0Z1g', 'HD', 6.1, 'In 1970, a few days before Christmas, Elvis Presley showed up on the White House lawn seeking to be deputized into the Bureau of Narcotics and Dangerous Drugs by the President himself.', NULL, NULL, '87', 'https://thumb.oloadcdn.net/splash/7VYUydmdlVE/ZFhn4aBPCDE.jpg', 0),
(91, 'tt3774802', 'Pandemic', 2016, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/o5Zt3nVlDJZLgeRgrhglgymDJym.jpg', '2016-02-26', 0, 'pE4wxPNQ30w', NULL, 3, 'https://www.youtube.com/embed/SllHJHiSwpo', 'HD', 4.2, 'In the near future, a virus of epic proportions has overtaken the planet. There are more infected than uninfected, and humanity is losing its grip on survival. Its only hope is finding a cure and keeping the infected contained. Lauren is a doctor who, after the fall of New York, arrives in Los Angeles to lead the hunt for uncontaminated civilian survivors. But nothing can prepare her crack team for the blood-soaked mayhem they are about to witness as they head into the Californian mean streets where everything is considered a trapâ€¦ From director John (THE SCRIBBLER) Suits, a boundary-crashing, game-changing science fiction thriller featuring non-stop action from a first person shooter perspective, putting the audience in the middle of every fight whilst feeling in control of every punch thrown and shot fired. Welcome to the new model of immersive action thriller for the hardcore video game generation.', NULL, NULL, '91', 'https://thumb.oloadcdn.net/splash/pE4wxPNQ30w/bbRBwzcDtXA.jpg', 0),
(92, 'tt2978102', 'Term Life', 2016, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/cxIj8Cp1iYKTNGwCVAk2pBXmP8h.jpg', '2016-04-29', 0, 'rdFKap2xSv0', NULL, 3, 'https://www.youtube.com/embed/WgNWtyKHods', 'HD', 5.8, 'If Nick Barrow can stay alive for 21 days, he\'ll die happy. Everyone Nick knows wants him dead; Mob bosses, contract killers, and dirty cops. Performing the last act of a desperate man, Nick takes out a million dollar insurance policy on himself, payable to his estranged daughter. The problem? The policy doesn\'t take effect for 21 days. Nick knows they\'ll be lucky to be alive for twenty-one hours.', NULL, NULL, '93', 'https://thumb.oloadcdn.net/splash/rdFKap2xSv0/rh0LIEUpdlk.jpg', 0),
(93, 'tt1291150', 'Teenage Mutant Ninja Turtles', 2014, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/oDL2ryJ0sV2bmjgshVgJb3qzvwp.jpg', '2014-08-07', 0, 'xs2692VtuwE', NULL, 3, 'https://www.youtube.com/embed/dwXFsrp6WBs', 'HD', 5.8, 'The city needs heroes. Darkness has settled over New York City as Shredder and his evil Foot Clan have an iron grip on everything from the police to the politicians. The future is grim until four unlikely outcast brothers rise from the sewers and discover their destiny as Teenage Mutant Ninja Turtles. The Turtles must work with fearless reporter April and her wise-cracking cameraman Vern Fenwick to save the city and unravel Shredder\'s diabolical plan.', NULL, NULL, '101', 'https://thumb.oloadcdn.net/splash/xs2692VtuwE/KeLrdGtSwV8.jpg', 0),
(94, 'tt0847212', 'Mother\'s Day Massacre', 2007, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/p2qgNbgfJF3m9sPP1sMyOHfSUIC.jpg', '2007-01-06', 0, 'jypBDqvMgF8', NULL, 3, 'https://www.youtube.com/embed/jzOk8pPbepM', 'HD', 5.7, 'Cheap thrills! Itâ€™s bargain basement horror time: a young manâ€™s search for his estranged mother leads him to a terrifying encounter with â€˜Pineysâ€™, backwoods hillbillies with a taste for violence.', NULL, NULL, '77', 'https://thumb.oloadcdn.net/splash/jypBDqvMgF8/75LVxM35wZI.jpg', 0),
(95, 'tt4289566', 'Sugarbabies', 2015, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/aFJ1J8trK8qv2FzXhwuN4NQETzx.jpg', '2015-01-01', 0, 'nmm2W7QqQnI', NULL, 3, 'https://www.youtube.com/embed/bcbT3U2tslc', 'HD', 6.1, 'Persuaded by her roommate, a college student makes an arrangement with an older man who pays for her companionship.', NULL, NULL, '91', 'https://thumb.oloadcdn.net/splash/nmm2W7QqQnI/CCJYiGo7JZ4.jpg', 0),
(96, 'tt4887842', 'The Invoking 2', 2015, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/8r4PNQDZ0QY8I0hSqQU5ZAt7SY1.jpg', '2015-10-06', 0, 'zTkGfZ-nsrE', NULL, 3, 'https://www.youtube.com/embed/tBE9JpFRueA', 'HD', 3.4, '\"Although hundreds of disturbing paranormal events occur every year, most of these chilling encounters go unreported - until now. Bear witness as hapless victims experience the unspeakable terror of confronting demonic forces, murderous poltergeists and other evil entities that are dead set on claiming their souls. Descend into an abyss of waking nightmares as these bloodthirsty, malevolent spirits seek to possess their prey and drag them-kicking and screaming-down to hell.\"', NULL, NULL, '83', 'https://thumb.oloadcdn.net/splash/zTkGfZ-nsrE/LQCdZFFDJmU.jpg', 3),
(97, 'tt5344794', 'Sniper: Special Ops', 2016, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/xOQwXYnnl5E5tTuGUTvCX3OIq2B.jpg', '2016-04-03', 0, 'MKmn4ytWhD8', NULL, 3, 'https://www.youtube.com/embed/Ar-5PAj-YOc', 'HD', 5.1, 'A Special Ops military Force, led by expert sniper Sergeant Jake Chandler, are sent to a remote Afghan village to extract an American congressman being held by the Taliban.', NULL, NULL, '84', 'https://thumb.oloadcdn.net/splash/MKmn4ytWhD8/GPMiR_764Dk.jpg', 1),
(98, 'tt0317740', 'The Italian Job', 2003, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/jNRCNX1uDiUf6mteFxOBn30wL8Q.jpg', '2003-05-30', 0, '66IXDyjeA6Eq', NULL, 3, 'https://www.youtube.com/embed/YT9VyEbUkCo', 'HD', 6.6, 'Charlie Croker pulled off the crime of a lifetime. The one thing that he didn\'t plan on was being double-crossed. Along with a drop-dead gorgeous safecracker, Croker and his team take off to re-steal the loot and end up in a pulse-pounding, pedal-to-the-metal chase that careens up, down, above and below the streets of Los Angeles.', NULL, NULL, '110', 'https://thumb.oloadcdn.net/splash/66IXDyjeA6E/8T8PaV88ooc.jpg', 0),
(99, 'tt3371366', 'Transformers: The Last Knight', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/f8Ng1Sgb3VLiSwAvrfKeQPzvlfr.jpg', '2017-06-21', 0, '', NULL, 3, 'https://www.youtube.com/embed/sKyLJI91EM0', 'HD', 6.2, 'Autobots and Decepticons are at war, with humans on the sidelines. Optimus Prime is gone. The key to saving our future lies buried in the secrets of the past, in the hidden history of Transformers on Earth.', NULL, NULL, '149', NULL, 0),
(100, 'tt4481414', 'Gifted', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/7YB2YrMwIm1g8FyZtlvmVDfRnAT.jpg', '2017-04-12', 0, '', NULL, 3, 'https://www.youtube.com/embed/x7CAjpdRaXU', 'HD', 7.8, 'Frank, a single man raising his child prodigy niece Mary, is drawn into a custody battle with his mother.', NULL, NULL, '101', NULL, 0),
(101, 'tt5884234', 'First Kill', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/1VQga6QhKCpa2nnGIIDNS8mKhOI.jpg', '2017-07-21', 0, '', NULL, 3, 'https://www.youtube.com/embed/t719hhEdWJc', 'HD', 0, 'A police chief tries to solve a kidnapping that involves a bank robber holding a young boy hostage.', NULL, NULL, '0', NULL, 0),
(102, 'tt4291600', 'Lady Macbeth', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/ab6JQbHMtNz09Dysp2Ehwq18Iha.jpg', '2017-04-12', 0, '', NULL, 3, 'https://www.youtube.com/embed/2Z0N8ULhuUA', 'HD', 6.9, 'The passionate affair of a young woman trapped in a marriage of convenience unleashes a maelstrom of murder and mayhem on a country estate.', NULL, NULL, '89', NULL, 0),
(103, 'tt5737862', 'Landline', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/l2KS8eeoWtm2ei8UYpafGhJVjrZ.jpg', '2017-07-21', 0, '', NULL, 3, 'https://www.youtube.com/embed/H_Cn1OleCXI', 'HD', 0, 'A teenager living with her sister and parents in Manhattan during the 1990s discovers that her father is having an affair.', NULL, NULL, '93', NULL, 0),
(104, 'tt6397762', 'April\'s Daughter', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/6P5UguqWttTri8iQWlCdYMOChVb.jpg', '2017-07-26', 0, '', NULL, 3, '', 'HD', 0, 'Valeria is 17 and pregnant. She lives in Puerto Vallarta with Clara, her half sister. Valeria has not wanted her long-absent mother, April, to find out about her pregnancy, but due to the economic strain and the overwhelming responsibility of having a baby in the house, Clara decides to call their mother. April arrives, willing to her daughters, but we soon understand why Valeria had wanted her to stay away.', NULL, NULL, '93', NULL, 0),
(105, 'tt5797756', 'Barrage', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/i3PPlHAPDgj4qDhRoD3aGMQ6IMm.jpg', '2017-07-19', 0, '', NULL, 3, '', 'HD', 0, 'After ten years abroad, Catherine returns to Luxembourg, to catch up with her daughter Alba, brought up by Catherineâ€™s mother Elisabeth. Alba is cold and distant with this stranger showing up unexpectedly in her life, so is Elisabeth, keen to keep her protÃ©gÃ©e to herself.  One day, Catherine cannot take it anymore. She kidnaps Alba and takes her on a trip to a lake up North. Begins an unsettling journey into the puzzling world of motherly love, only to find out that sometimes the true opponent is yourself.', NULL, NULL, '112', NULL, 0),
(106, 'tt6314690', 'Les As de la Jungle', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/3CJIyae8aXJ41kl23JGKVOsaBRi.jpg', '2017-07-26', 0, '', NULL, 3, '', 'HD', 0, NULL, NULL, NULL, '0', NULL, 0),
(107, 'tt6648404', 'mon mon mon MONSTERS', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/8fnbYS15h5XDe82UcTj9Owd139p.jpg', '2017-07-28', 0, '', NULL, 3, 'https://www.youtube.com/embed/fAqHWGAF7t8', 'HD', 0, 'Shu-wei is an unpopular student who is a constant target for the school bullies, but he ends up joining forces with them when they encounter two demonic sisters who feast on human flesh in the middle of the night. The gang manage to capture one, but the other demon is angry and starts killing anyone wearing a school uniform...', NULL, NULL, '110', NULL, 0),
(108, 'tt4425200', 'John Wick: Chapter 2', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/zkXnKIwX5pYorKJp2fjFSfNyKT0.jpg', '2017-02-08', 0, '', NULL, 3, 'https://www.youtube.com/embed/LZrX9mffH8Y', 'HD', 6.6, 'John Wick is forced out of retirement by a former associate looking to seize control of a shadowy international assassinsâ€™ guild. Bound by a blood oath to aid him, Wick travels to Rome and does battle against some of the worldâ€™s most dangerous killers.', NULL, NULL, '122', NULL, 0),
(109, 'tt5462602', 'The Big Sick', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/qquEFkFbQX1i8Bal260EgGCnZ0f.jpg', '2017-06-23', 0, '', NULL, 3, 'https://www.youtube.com/embed/PJmpSMRQhhs', 'HD', 7.9, 'A couple deals with their cultural differences as their relationship grows.', NULL, NULL, '119', NULL, 0),
(110, 'tt6513406', 'Wu Kong', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/srdxQhvnLVnZjjUF7UwVBtbyvXj.jpg', '2017-07-13', 0, '', NULL, 3, 'https://www.youtube.com/embed/EpFQaYv8MV4', 'HD', 10, 'The story tells the story of Sun Wukong (Eddie Peng) and Erlang Shen (Shawn Yue), who comes to the Immortal Mountain to cultivate their skills. They gained friendship, experienced love and ultimately betrayal, growing throughout their life journey.', NULL, NULL, '130', NULL, 0),
(111, 'tt4986134', 'Rebel in the Rye', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/v1vLMzrH4S8l1bagbHb99OTNJzj.jpg', '2017-07-13', 0, '', NULL, 3, '', 'HD', 0, 'The life of celebrated but reclusive author, J.D. Salinger, who gained worldwide fame with the publication of his novel, The Catcher in the Rye.', NULL, NULL, '106', NULL, 0),
(112, 'tt1691152', 'David Lynch: The Art Life', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/pgtQceTZKylB2B237t18h2pOXpE.jpg', '2017-02-15', 0, '', NULL, 3, 'https://www.youtube.com/embed/BVgQ8yAdLbI', 'HD', 7.2, 'An intimate journey through the formative years of [Lynchâ€™s] life. From his idyllic upbringing in small town America to the dark streets of Philadelphia, we follow Lynch as he traces the events that have helped to shape one of cinemaâ€™s most enigmatic directors. David Lynch: The Art Life infuses Lynchâ€™s own art, music and early films, shining a light into the dark corners of his unique world, giving audiences a better understanding of the man and the artist.', NULL, NULL, '90', NULL, 0),
(113, 'tt6021860', 'Sales Gosses', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/g6rRyDnXHmD24BlgzxSgYLAzDr5.jpg', '2017-07-19', 0, '', NULL, 3, '', 'HD', 0, NULL, NULL, NULL, '', NULL, 0),
(114, 'tt6333086', 'Menashe', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/11jQ1dsdefwAZIilbvgr0lMyhKN.jpg', '2017-07-28', 0, '', NULL, 3, 'https://www.youtube.com/embed/83UoZcdX__Y', 'HD', 0, 'Within Brooklynâ€™s ultra-orthodox Jewish community, a widower battles for custody of his son.', NULL, NULL, '82', NULL, 0),
(115, 'tt6096266', 'Defiant Lives', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/vXJzXbRRxucT4Z3N9Xh6LVrFlG4.jpg', '2017-07-17', 0, '', NULL, 3, '', 'HD', 0, 'The rise of the disability rights movement in the USA, UK and Australia.', NULL, NULL, '84', NULL, 0),
(116, 'tt5129604', 'Brava', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/oo8EQ0JTUMA7l5b1iq9F0VGcOzQ.jpg', '2017-07-07', 0, '', NULL, 3, '', 'HD', 0, NULL, NULL, NULL, '', NULL, 0),
(117, 'tt5914996', 'No Game No Life: Zero', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/dIUhR5aocsI6liiQaQLYk65XZQu.jpg', '2017-07-15', 0, '', NULL, 3, '', 'HD', 0, 'The official Twitter account for the No Game No Life series announced that a movie adaptation of the light novel was revealed during the MF Bunko J Summer School Festival 2016 event. The movie will adapt sixth volume of the light novel.', NULL, NULL, '0', NULL, 0),
(118, 'tt2345759', 'The Mummy', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/zxkY8byBnCsXodEYpK8tmwEGXBI.jpg', '2017-06-06', 0, '', NULL, 3, 'https://www.youtube.com/embed/IjHgzkQM2Sg', 'HD', 5.2, 'Though safely entombed in a crypt deep beneath the unforgiving desert, an ancient queen whose destiny was unjustly taken from her is awakened in our current day, bringing with her malevolence grown over millennia, and terrors that defy human comprehension.', NULL, NULL, '110', NULL, 0),
(119, 'tt1935194', 'Overdrive', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/a0hTRjis1cxwmjOuBaS7WdDG3dj.jpg', '2017-06-29', 0, '', NULL, 3, 'https://www.youtube.com/embed/NyQUe80IXt8', 'HD', 9, 'Master car thieves square off against French gangsters in the South of France with money, women and lives all on the line.', NULL, NULL, '0', NULL, 0),
(120, 'tt3823116', 'A Stork\'s Journey', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/9igentB9QsaIWeIbbGQkk48FFpA.jpg', '2017-05-11', 0, '', NULL, 3, '', 'HD', 5.1, 'Orphaned at birth and raised by storks, the teenage sparrow Richard believes he is one of them. But when the time comes to migrate to Africa, his stork family is forced to reveal his true identity and leave him behind in the forest, since he is not a migratory bird and would not survive the journey. Determined to prove he is a stork after all, Richard ventures south on his own. But only with the help of Olga, an eccentric owl with an imaginary friend and Kiki, a narcissistic, disco-singing parakeet, does he stand a chance to reach his goal and learn to accept who he really is.', NULL, NULL, '85', NULL, 0),
(121, 'tt4464394', 'The Gracefield Incident', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/zo3xX2JNo6Khy4VG3NwCwsyq3Gt.jpg', '2017-07-21', 0, '', NULL, 3, 'https://www.youtube.com/embed/z4LRvzyHN9Q', 'HD', 0, 'On August 16, 2013, the Supreme Court mandated the CIA to declassify files that had been kept secret for the past 75 years. Visual records of documented paranormal events were released to the public. The following incident took place in Gracefield, Quebec.', NULL, NULL, '95', NULL, 0),
(122, 'tt6922038', 'Catastrophe', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/dHpUB2lajzmGX2pkKtmS4OPC7SL.jpg', '2017-07-07', 0, '', NULL, 3, '', 'HD', 0, 'When a sweet and innocent parakeet is found inexplicably dead in her cage, all fingers seem to point towards cat Rodney. To prove his innocence, he desperately tries to put the blame on others, with disastrous consequences.', NULL, NULL, '2', NULL, 0),
(123, 'tt6709826', 'Bronx Gothic', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/jxPjQNbosJmfhDrKXaFBGOHc1PA.jpg', '2017-07-12', 0, '', NULL, 3, '', 'HD', 0, 'From director Andrew Rossi (Page One: Inside the New York Times, The First Monday in May) comes an electrifying portrait of writer and performer Okwui Okpokwasili and her acclaimed one-woman show, Bronx Gothic. Rooted in memories of her childhood, Okwui â€“ whoâ€™s worked with conceptual artists like Ralph Lemon and Julie Taymor â€“ fuses dance, song, drama and comedy to create a mesmerizing space in which audiences can engage with a story about two 12-year-old black girls coming of age in the 1980s. With intimate vÃ©ritÃ© access to Okwui and her audiences off the stage, Bronx Gothic allows for unparalleled insight into her creative process as well as the complex social issues embodied in it.', NULL, NULL, '91', NULL, 0),
(124, 'tt5955898', 'El cerro de los buitres', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/sRlr4tT7bfpwMLcM1kqHKo1fcOu.jpg', '2017-07-25', 0, '', NULL, 3, '', 'HD', 0, 'Follows the story of a young revolutionary man caught in the excitement and tragedy of living two different lives, as an undercover agent and as revolutionary.', NULL, NULL, '0', NULL, 0),
(125, 'tt5954462', 'LumiÃ¨reÂ ! L\'aventure commence', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/oxkOCb7Cu9NBPaQj1MxrvntboBf.jpg', '2017-01-25', 0, '', NULL, 3, '', 'HD', 8.3, 'LumiÃ¨re! reintroduces some of cinema\'s foundational moments through gorgeous restored prints of the work of the LumiÃ¨re Brothers.', NULL, NULL, '90', NULL, 0),
(126, 'tt6151226', 'Cause of Death: Unknown', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2', '2017-03-24', 0, '', NULL, 3, '', 'HD', 0, 'A 2017 documentary.', NULL, NULL, '85', NULL, 0),
(127, 'tt6502580', 'Chunkzz', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/h9w7jXWFHOiucj9OLiCs0RbZzH5.jpg', '2017-07-07', 0, '', NULL, 3, '', 'HD', 0, 'Chunkzz is an upcoming Malayalam Comedy movie Written and directed by Omar Lulu .Starring Balu Varghese in lead role.this film is produced by Vyshak RajanThe screenplay is written by Aneesh Hameed and Sanoop Thykoodam.', NULL, NULL, '0', NULL, 0),
(128, 'tt3890160', 'Baby Driver', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/mBkOBaxjr7IBHGuQS3sSFeyYTM.jpg', '2017-06-28', 0, '', NULL, 3, 'https://www.youtube.com/embed/z2z857RSfhk', 'HD', 6.6, 'After being coerced into working for a crime boss, a young getaway driver finds himself taking part in a heist doomed to fail.', NULL, NULL, '113', NULL, 0),
(129, 'tt2239822', 'Valerian and the City of a Thousand Planets', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/5ov7TYjasIaXDg8Jm2UeZA2aZtg.jpg', '2017-07-20', 0, '', NULL, 3, 'https://www.youtube.com/embed/1CWfrJL-hC0', 'HD', 0, 'In the 28th century, Valerian and Laureline are special operatives charged with keeping order throughout the human territories. On assignment from the Minister of Defense, the two undertake a mission to Alpha, an ever-expanding metropolis where species from across the universe have converged over centuries to share knowledge, intelligence, and cultures. At the center of Alpha is a mysterious dark force which threatens the peaceful existence of the City of a Thousand Planets, and Valerian and Laureline must race to identify the menace and safeguard not just Alpha, but the future of the universe.', NULL, NULL, '137', NULL, 0),
(130, 'tt4411596', 'My Cousin Rachel', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/yz9Pnx5tbpZOHWsCD0LtoTCgmLi.jpg', '2017-06-08', 0, '', NULL, 3, 'https://www.youtube.com/embed/8NfQ7o_rCC0', 'HD', 6.3, 'A young Englishman plots revenge against his mysterious, beautiful cousin, believing that she murdered his guardian. But his feelings become complicated as he finds himself falling under the beguiling spell of her charms.', NULL, NULL, '106', NULL, 0),
(131, 'tt4129428', 'Jagga Jasoos', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/aAvF8e5w4xETbx8TUesCnVFJqSF.jpg', '2017-07-14', 0, '', NULL, 3, 'https://www.youtube.com/embed/xtm48kJwL1I', 'HD', 8.2, 'A gifted teenage detective searches for his missing father with his ladylove assistant.', NULL, NULL, '180', NULL, 0),
(132, 'tt5226512', 'Feed', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/vg11iLI2cyluDO48Zc8CzXzNV9t.jpg', '2017-07-18', 0, '', NULL, 3, '', 'HD', 0, 'Olivia and Matthew Grey, 18-year-old twins born into a world of privilege and high expectations. There are almost no boundaries between them; even their dreams are connected.', NULL, NULL, '0', NULL, 0),
(133, 'tt4977530', 'Viceroy\'s House', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/qKd2pxSsJBPxotVEtpb8qeQblVs.jpg', '2017-03-03', 0, '', NULL, 3, 'https://www.youtube.com/embed/id_ZyNdvXKQ', 'HD', 6.2, 'In 1947, Lord Mountbatten assumes the post of last Viceroy, charged with handing India back to its people, living upstairs at the house which was the home of British rulers, whilst 500 Hindu, Muslim and Sikh servants lived downstairs.', NULL, NULL, '106', NULL, 0),
(134, 'tt6616800', 'Villain', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/vNhn5YSLY9MF9UnczyRIM8Sno7C.jpg', '2017-07-21', 0, '', NULL, 3, 'https://www.youtube.com/embed/geygZ49EF_8', 'HD', 0, 'Villain is an upcoming Indian Malayalam-language thriller film written and directed by B. Unnikrishnan and produced by Rockline Venkatesh. The film stars Mohanlal in the lead role', NULL, NULL, '0', NULL, 0),
(135, 'tt3737944', 'The Winter', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/j1XN58Sqh4UFWTiPp0f3QfT03Iq.jpg', '2017-06-28', 0, '', NULL, 3, '', 'HD', 0, 'After years of working on an \'estancia\' in Patagonia, the Old Foreman is forced to retire and a younge\'r man takes his place. The change is difficult and challenging for both men. Each one must survive the oncoming winter.', NULL, NULL, '95', NULL, 0),
(136, 'tt5937296', 'Glory', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/pA9BzH0y6PvW1zJ9ceiizRbprQU.jpg', '2017-03-02', 0, '', NULL, 3, 'https://www.youtube.com/embed/a0Yulv2_BK4', 'HD', 6, 'When Tsanko Petrov, a railroad worker, finds millions of lev on the train tracks, he decides to turn the entire amount over to the police. Grateful, the state rewards him with a new wristwatchâ€¦ which soon stops working. Meanwhile, Julia Staikova, the head of PR for the Ministry of Transport, loses his old watch. Here starts Petrovâ€™s desperate struggle to get back not only his old watch, but his dignity.', NULL, NULL, '101', NULL, 0),
(137, 'tt3469046', 'Despicable Me 3', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/5qcUGqWoWhEsoQwNUrtf3y3fcWn.jpg', '2017-06-29', 0, '', NULL, 3, 'https://www.youtube.com/embed/6DBi41reeF0', 'HD', 6, 'Gru and his wife Lucy must stop former \'80s child star Balthazar Bratt from achieving world domination.', NULL, NULL, '96', NULL, 0),
(138, 'tt7080040', 'Our Time Will Come', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/yC0Xd0QfkjTMnwL5vZc0NPR7hMc.jpg', '2017-07-01', 0, '', NULL, 3, 'https://www.youtube.com/embed/5BNECsr3ADk', 'HD', 0, 'Set in the 1940s, the story tells the story of a legendary woman \"Fang Gu\" (Zhou Xun), who is one of the key figures during the Japanese occupation of Hong Kong. It will also portrayal the fight and struggle for freedom and independence by youths of the resistance groups.', NULL, NULL, '', NULL, 0),
(139, 'tt1718924', 'A Family Man', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/612avgKpukTYLmrXLZmip2Hjwmv.jpg', '2017-05-18', 0, '', NULL, 3, 'https://www.youtube.com/embed/zH50qtIyxiI', 'HD', 7.1, 'Dane Jensen is a driven, Chicago-based headhunter, working at a cut-throat job placement firm. When Dane\'s boss announces his retirement, he pits Dane against Lynn Vogel, Dane\'s equally driven but polar-opposite rival at the firm, in a battle for control over the company.', NULL, NULL, '108', NULL, 0),
(140, 'tt6441072', 'Embrasse-moi !', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/vQE4YLnUQVc9c0ILGxM5xXnHohm.jpg', '2017-07-05', 0, '', NULL, 3, '', 'HD', 6, NULL, NULL, NULL, '', NULL, 0),
(141, 'tt4429194', 'Paris Can Wait', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/jLzfHNU1JldQpC5TNEvQMnWQxx6.jpg', '2017-02-09', 0, '', NULL, 3, 'https://www.youtube.com/embed/HTrT6QSqnGs', 'HD', 6.9, 'A woman at a crossroads traveling to Cannes along with her successful film producer husband, finds herself on a two-day road trip with his business associate. What follows is a carefree journey replete with diversions involving picturesque sites, fine food and wine, humor, wisdom and romance - reawakening Anne\'s senses and a new lust for life.', NULL, NULL, '92', NULL, 0),
(142, 'tt6177382', 'Mind Over Matter', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2', '2017-07-15', 0, '', NULL, 3, '', 'HD', 0, 'A young man born with Cerebral Palsy battles a paralyzed left hand, bullies and stereotypes about the disabled to defy the odds and make it as a rock and roll guitarist. Ultimately, sharing the stage with the very band that inspired him to start (or to achieve the impossible).', NULL, NULL, '93', NULL, 0),
(143, 'tt6323858', 'Her Sketchbook', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/jNGArYOANjBeblsZAqbA0fUfvp.jpg', '2017-07-15', 0, '', NULL, 3, 'https://www.youtube.com/embed/NjHkzDP-XWo', 'HD', 0, 'Mami rarely leaves home since she dropped out from middle school. She has been escaping reality by immersing herself in copying her favorite manga and illustrations in her room full of books and comics. Her father, Eisuke, worries a lot about her ever since he divorced with her mother, Mika.', NULL, NULL, '0', NULL, 0),
(144, 'tt7003278', 'Earth 2.0', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/1mTSAg1lcU1p8s7Yov9FlEZfkHn.jpg', '2017-06-10', 0, '', NULL, 3, '', 'HD', 0, 'Anja and Ahmo find themselves on Earth 2.0, only to discover that things there are much the same as they were on Earth.', NULL, NULL, '12', NULL, 0),
(145, 'tt4966046', 'Awaken the Shadowman', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/s7o8AwPkmpHvtTP6qG2lSfwdW1U.jpg', '2017-07-21', 0, '', NULL, 3, 'https://www.youtube.com/embed/0qt55_DiQ-k', 'HD', 0, 'After the mysterious disappearance of their mother, estranged brothers reunite and discover an unknown supernatural force.', NULL, NULL, '90', NULL, 0),
(146, 'tt2250912', 'Spider-Man: Homecoming', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/ApYhuwBWzl29Oxe9JJsgL7qILbD.jpg', '2017-07-06', 0, '-kyVS10ma4U', NULL, 3, 'https://www.youtube.com/embed/lCkVr1n1eCA', 'CAM', 7.4, 'Following the events of Captain America: Civil War, Peter Parker, with the help of his mentor Tony Stark, tries to balance his life as an ordinary high school student in Queens, New York City, with fighting crime as his superhero alter ego Spider-Man as a new threat, the Vulture, emerges.', NULL, NULL, '133', 'https://thumb.oloadcdn.net/splash/-kyVS10ma4U/adnqMStNzO0.jpg', 1),
(147, 'tt2406566', 'Atomic Blonde', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/kV9R5h0Yct1kR8Hf8sJ1nX0Vz4x.jpg', '2017-07-27', 0, '', NULL, 3, 'https://www.youtube.com/embed/HNKX2Ymfhbg', 'HD', 0, 'An undercover MI6 agent is sent to Berlin during the Cold War to investigate the murder of a fellow agent and recover a missing list of double agents.', NULL, NULL, '115', NULL, 0),
(148, 'tt3896738', 'Hounds of Love', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/b4niGo8hpxIpmAZsGkpZK4mXiDo.jpg', '2017-05-12', 0, '', NULL, 3, 'https://www.youtube.com/embed/uWZFHEz37Jc', 'HD', 5.9, 'Vicki Maloney is randomly abducted from a suburban street by a disturbed couple. As she observes the dynamic between her captors she quickly realises she must drive a wedge between them if she is to survive.', NULL, NULL, '108', NULL, 0),
(149, 'tt5993456', 'Mission Pays Basque', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/smizNL3SsCSohya0wtPVSlIr03Q.jpg', '2017-07-12', 0, '', NULL, 3, '', 'HD', 0, 'Sibylle, a young Parisian with long teeth, intends to shine in her new job by buying a hardware store in the Basque Country to set up a supermarket. She imagines that she has \"rolled\" the old owner but the latter is under curatorship. Sibylle must therefore deal with Ramon, the nephew, to recover his money and sign as soon as possible. Otherwise, the ejection seat is assured. She will soon realize that the Basques do not intend to let themselves be made by a Parisian, however pretty she is.', NULL, NULL, '0', NULL, 0),
(150, 'tt5715410', 'The Son of Bigfoot', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/6stbYJynPLfXfe4kNLUNk9mcoD.jpg', '2017-07-27', 0, '', NULL, 3, '', 'HD', 0, 'Teenage outsider Adam sets out on an epic and daring quest to uncover the mystery behind his long-lost dad, only to find out that he is none other than the legendary Bigfoot! He has been hiding deep in the forest for years to protect himself and his family from HairCo., a giant corporation eager to run scientific experiments with his special DNA. As father and son start making up for lost time after the boy\'s initial disbelief, Adam soon discovers that he too is gifted with superpowers beyond his imagination. But little do they know, HairCo. is on their tail as Adam\'s traces have led them to Bigfoot!', NULL, NULL, '0', NULL, 0),
(151, 'tt5645536', 'Love And Other Cults', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/oZEy9TGfLKKrLS5CbmJz40pnVyd.jpg', '2017-07-15', 0, '', NULL, 3, 'https://www.youtube.com/embed/f9TmRa9lncY', 'HD', 0, 'Born in a small town in Japan, a young girl named Ai is sent to a cult commune by her religious maniac mother and lives there for seven long years. After the cult is exposed by the police, Ai starts a new stage of life, going to a normal school for the first time, but she canâ€™t find her place to fit in there. Ai drops out from the school and society, spending her life living with a rock-bottom delinquent family full of gangsters and call girls. In a strange twist of fate she finds herself back in a new and normal life, living with a middle-class family, but her troubled life continues to follow her into more deep and seedy paths.', NULL, NULL, '95', NULL, 0),
(152, 'tt5991290', 'Passade', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/6Aa3GWLYLMVRqLtEPpWM6Zi3GGj.jpg', '2017-07-26', 0, '', NULL, 3, '', 'HD', 0, NULL, NULL, NULL, '', NULL, 0),
(153, 'tt5731132', 'Ancien and the Magic Tablet', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/32hcz1e2uxjUYQLeYim4VFBHn2O.jpg', '2017-03-18', 0, '', NULL, 3, '', 'HD', 4.5, 'Hirune Hime is set in the Kojima region of Okayama Prefecture in the year 2020, two days after the scheduled Tokyo Olympics. Kokone lives together with her father, who is always working on modifying cars. As she begins to investigate a strange dream has been seeing over and over, Kokone learns a secret about her family.', NULL, NULL, '0', NULL, 0),
(154, 'tt5695668', 'Personal Affairs', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/cOqMaeQ8XeOvUAkXcYx0QVn8IM6.jpg', '2017-03-01', 0, '', NULL, 3, '', 'HD', 0, 'In Nazareth, an old couple lives wearily to the rhythm of the daily routine. On the other side of the border, in Ramallah, their son Tarek wishes to remain an eternal bachelor, their daughter is about to give birth while her husband lands a movie role and the grandmother loses her head... Between check-points and dreams, frivolity and politics, some want to leave, others want to stay but all have personal affairs to resolve.', NULL, NULL, '90', NULL, 0),
(155, 'tt3450958', 'War for the Planet of the Apes', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/y52mjaCLoJJzxfcDDlksKDngiDx.jpg', '2017-07-13', 0, '', NULL, 3, 'https://www.youtube.com/embed/UEP1Mk6Un98', 'HD', 6, 'Caesar and his apes are forced into a deadly conflict with an army of humans led by a ruthless Colonel. After the apes suffer unimaginable losses, Caesar wrestles with his darker instincts and begins his own mythic quest to avenge his kind. As the journey finally brings them face to face, Caesar and the Colonel are pitted against each other in an epic battle that will determine the fate of both their species and the future of the planet.', NULL, NULL, '142', NULL, 0),
(156, 'tt2091256', 'Captain Underpants: The First Epic Movie', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/AjHZIkzhPXrRNE4VSLVWx6dirK9.jpg', '2017-06-01', 0, '', NULL, 3, 'https://www.youtube.com/embed/VDm_2m-Hg6c', 'HD', 7, 'Two mischievous kids hypnotize their mean elementary school principal and turn him into their comic book creation, the kind-hearted and elastic-banded Captain Underpants.', NULL, NULL, '89', NULL, 0),
(157, 'tt6334884', 'Hickok', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/nZdifMku2bcZKZCu7xyrVEU09Le.jpg', '2017-07-07', 0, '', NULL, 3, 'https://www.youtube.com/embed/ZJVcI6tw5jo', 'HD', 5.3, 'Legendary Lawman and Gunslinger Wild Bill Hickok, is tasked with taming the wildest cow-town in the West, wile delivering his own brand of frontier Justice and infamous gunfighter\'s reputation as the fastest draw in the West is put to the test', NULL, NULL, '88', NULL, 0),
(158, 'tt7035152', 'Ash Flower', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/wooijEYRolrcq42T5u5udz3PARg.jpg', '2017-07-06', 0, '', NULL, 3, '', 'HD', 0, 'A girl of about ten carries a little suitcase when arriving at an intercity bus terminal. She was abandoned by her mother a week ago. Not knowing what to do and why her mother left her, she decides to find a man who might be her father and who she has never met before.', NULL, NULL, '125', NULL, 0),
(159, 'tt5695764', 'Inversion', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/1HlKaSZFhTauA9Ih1x9oJkNFQSo.jpg', '2017-07-19', 0, '', NULL, 3, '', 'HD', 0, 'Tehran\'s air pollution has reached maximum levels because of thermal inversion. Unmarried 30-something Niloofar lives with her aged mother, and stays busy with her alterations shop. When doctors insist that her mother must leave smoggy Tehran for her respiratory health, Niloofarâ€™s brother and family elders decide that she must also move away to accompany her mother. Now Niloofar is torn between family loyalty and living her own life. As the youngest she has always obeyed their orders. Can she stand up for herself this time?', NULL, NULL, '84', NULL, 0),
(160, 'tt5286432', 'Berlin Falling', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/rgF0SCuWCHjArhIorI6NQMrZaw5.jpg', '2017-07-13', 0, '', NULL, 3, '', 'HD', 0, NULL, NULL, NULL, '', NULL, 0),
(161, 'tt3575198', 'Computers & Imagination', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2', '2017-07-20', 0, '', NULL, 3, '', 'HD', 0, 'Kyle an NYC photographer uses his craft to fulfill his sexual desires while searching for Love while experiencing Heartache', NULL, NULL, '0', NULL, 0),
(162, 'tt5458566', 'Small Town Killers', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/gI3aRlBnMEPdujNUUhn4IkUQLHi.jpg', '2017-01-12', 0, '', NULL, 3, '', 'HD', 6, 'The two tradesmen Ib and Edward are tired of their lifeless marriages and dream of living the good life from the stash of money they\'ve earned moonlighting for years. After a huge fight with their wives the two men get drunk and hire a Russian contract killer to do a hit on their spouses. But they have badly underestimated their wives, and this becomes the start of an absurd journey where Ib and Edward to their own horror end at the top of a kill list.', NULL, NULL, '90', NULL, 0),
(163, 'tt5639354', 'A Fantastic Woman', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/aAcDJwPUadJrN6M42CwNEdRp0qS.jpg', '2017-01-25', 0, '', NULL, 3, 'https://www.youtube.com/embed/SgDhpy9Z-NM', 'HD', 8, 'A waitress and singer struggles to recover from the death of her boyfriend.', NULL, NULL, '104', NULL, 0),
(164, 'tt3606752', 'Cars 3', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/fyy1nDC8wm553FCiBDojkJmKLCs.jpg', '2017-06-15', 0, '', NULL, 3, 'https://www.youtube.com/embed/U1eQYk74R1g', 'HD', 6.3, 'Blindsided by a new generation of blazing-fast racers, the legendary Lightning McQueen is suddenly pushed out of the sport he loves. To get back in the game, he will need the help of an eager young race technician with her own plan to win, inspiration from the late Fabulous Hudson Hornet, and a few unexpected turns. Proving that #95 isn\'t through yet will test the heart of a champion on Piston Cup Racingâ€™s biggest stage!', NULL, NULL, '109', NULL, 0),
(165, 'tt1661275', 'Their Finest', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/tF0dhJfxzOjvkybJVfNPuuj5ClH.jpg', '2017-04-06', 0, '', NULL, 3, 'https://www.youtube.com/embed/mMh1EvAQOQ8', 'HD', 7.3, 'A British film crew attempts to boost morale during World War II by making a propaganda film after the Blitzkrieg.', NULL, NULL, '117', NULL, 0),
(166, 'tt2658538', 'Austin Found', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/mQrwpYJ7qsAb0rjIoD117aaJPfI.jpg', '2017-07-07', 0, '', NULL, 3, 'https://www.youtube.com/embed/PFx-BpbRDTk', 'HD', 8, 'A woman who is fed up with her mundane lifestyle hatches a scheme to make her family instant celebrities by having her ex-boyfriend kidnap her 11-year-old daughter for a month.', NULL, NULL, '95', NULL, 0),
(167, 'tt5540188', 'The Nile Hilton Incident', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/3VEbB2jfUiZo85vpdLmQgIT7tk1.jpg', '2017-07-12', 0, '', NULL, 3, '', 'HD', 8.3, 'In Cairo, weeks before the 2011 revolution, Police Detective Noredin is working in the infamous Kasr el-Nil Police Station when he is handed the case of a murdered singer. He soon realizes that the investigation concerns the power elite, close to the Presidentâ€™s inner circle.', NULL, NULL, '106', NULL, 0),
(168, 'tt5803528', '100 % Coco', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/97BiBplu9GlyroROK5nGQi5hyj9.jpg', '2017-07-06', 0, '', NULL, 3, '', 'HD', 4.8, '13 year old Coco loves fashion and has her own unique style. She dreams of becoming a famous fashion icon. But on her first day of school she is made fun of and everyone calls her \'Coco the Clown\'. Devastated, she decides to leave her vintage dresses, panther boots and striped socks at home and to dress herself more unnoticeable. Her anonymously kept fashion vlog as Style Tiger is a huge hit however. Is there still a chance that she will become a style icon? And does Bruno, that quirky boy she knows from school and secretly likes, finally notice her? Slowly Coco learns that she just has to be 100% herself!', NULL, NULL, '88', NULL, 0),
(169, 'tt5805470', 'Gintama', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/kZwyyazISEgJXVkhre00Kj7pvlf.jpg', '2017-07-14', 0, '', NULL, 3, 'https://www.youtube.com/embed/-znuivrIXT8', 'HD', 0, 'In an era where aliens have invaded and taken over feudal Tokyo, a young samurai finds work however he can.', NULL, NULL, '0', NULL, 0),
(170, 'tt6078840', 'I Want to Eat Your Pancreas', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/5emCkcATPFCp5SQsI8vUnMkBZZM.jpg', '2017-07-28', 0, '', NULL, 3, '', 'HD', 0, 'I (Takumi Kitamura) am a high school student. I happen to find a diary by my classmate Sakura Yamauchi (Minami Hamabe) that reveals she is suffering from a pancreatic disease. I spent time with Sakura, but she dies.  12 years later, due to Sakuraâ€™s words, I (Shun Oguri) am now a high school teacher at the same school where I graduated from. While I talk with my student, I remember several months I spent with Sakura. Meanwhile, Kyoko (Keiko Kitagawa), who was Sakuraâ€™s friend, is soon to marry. Kyoko also recalls the days she spent with me and Sakura.', NULL, NULL, '0', NULL, 0),
(171, 'tt6462506', 'The Day After', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/WmCxeF53CGuztnfmUh4OxyMRVi.jpg', '2017-06-07', 0, '', NULL, 3, '', 'HD', 0, 'It is Areumâ€™s first day of work at a small publisher. Her boss Bongwan loved and recently broke up with the woman who previously worked there. Today too, the married Bongwan leaves home in the dark morning and sets off to work.  The memories of the woman who left weigh down on him.  That day Bongwanâ€™s wife finds a love note, bursts into the office, and mistakes Areum for the woman who left.', NULL, NULL, '92', NULL, 0),
(172, 'tt5050876', 'A Hustler\'s Diary', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/ch2ompqofLrmyAMPvdKj07Pxhns.jpg', '2017-01-06', 0, '', NULL, 3, '', 'HD', 0, 'Metin is a small time hustler in a Stockholm suburb, filled with big dreams but bogged down by the circular world of street crime. Things change when his secret diary becomes a hot item for a posh book publisher, and Metin must decide if he\'s really up to the challenge that is leaving his criminal life behind.', NULL, NULL, '96', NULL, 0),
(173, 'tt2932536', '47 Meters Down', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/A9mUCcgPziYqK1JlckhEWvZ2z8y.jpg', '2017-06-15', 0, '', NULL, 3, 'https://www.youtube.com/embed/LBmBcASLdK8', 'HD', 4.6, 'Two sisters on Mexican vacation are trapped in a shark observation cage at the bottom of the ocean, with oxygen running low and great whites circling nearby, they have less than an hour of air left to figure out how to get to the surface.', NULL, NULL, '89', NULL, 0),
(174, 'tt4299300', 'My Blind Date with Life', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/uneNYyni8wSflliNVOrz7VOnMHu.jpg', '2017-01-26', 0, '', NULL, 3, '', 'HD', 7.3, 'An ambitious young man struggles to achieve his dream of becoming an employee in a Munich luxury hotel despite being strongly visually impaired.', NULL, NULL, '111', NULL, 0),
(175, 'tt5256722', 'He Even Has Your Eyes', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/5reyB96i5ufQwE8Kc4BN3wZIgZ2.jpg', '2017-01-18', 0, '', NULL, 3, '', 'HD', 6.4, 'Paul is married to Sali. Everything would be fine if they could get a child. Until the day when Sali receives the call that they have been waiting for so long: their adoption file is approved. He is adorable, he is 6 months old, his name is Benjamin. He is blond with blue eyes and white. They - are black.', NULL, NULL, '95', NULL, 0);
INSERT INTO `flixymovies` (`id`, `imdbID`, `name`, `Year`, `pic`, `Released`, `olstatus`, `openload`, `streamango`, `Status`, `trailer`, `Quality`, `Rating`, `Plot`, `Actors`, `Genre`, `Runtime`, `splash`, `views`) VALUES
(176, 'tt5804948', 'With Open Arms', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/zpWrwPzdLi46XP6nG97FABNyF9W.jpg', '2017-04-05', 0, '', NULL, 3, 'https://www.youtube.com/embed/K1X5fvNrK3A', 'HD', 5.2, 'Jean-Ã‰tienne Fougerole is an intellectual bohemian who released his new novel \"In Open Arms\" and calling the wealthiest people to welcome home the families in need. While he promotes his book during a televised debate, his opponent criticized him for not applying what he himself advocates. While stuck, Jean-Ã‰tienne Fougerole accepts the challenge, for fear of being discredited. The same evening, a family of Roma rings the door of his Marnes-la-Coquette villa and the writer feels obliged to house them.', NULL, NULL, '92', NULL, 0),
(177, 'tt6336356', 'Mary and the Witch\'s Flower', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/tp04p44YQMUrgEnxIK8Sad8A2CI.jpg', '2017-07-08', 0, '', NULL, 3, 'https://www.youtube.com/embed/XrN9QZLmJ0E', 'HD', 0, 'Mary and the Witch\'s Flower is an upcoming Japanese anime fantasy film directed by Hiromasa Yonebayashi and produced by Studio Ponoc, based on The Little Broomstick by Mary Stewart. This is Studio Ponoc\'s first feature film. The film tells a story of a young girl named Mary who discovers that she has a mysterious power to become a witch which lasts for one night only.', NULL, NULL, '120', NULL, 0),
(178, 'tt5897108', 'Project Ghazi', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/1VmDLGQrhd0k1oNdgvPaSoXEJhP.jpg', '2017-07-14', 0, '', NULL, 3, 'https://www.youtube.com/embed/qR3MXqTfg9Q', 'HD', 1, 'Pakistanâ€™s first superhero film Project Ghazi project has been a hot topic in the entertainment industry of Pakistan ever since the project was announced last year. Humayun Saeed, Shehryar Munawar and Syra Shahroz will be seen in lead roles.', NULL, NULL, '0', NULL, 0),
(179, 'tt5231916', 'Scribe', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/tlYsLrYH1Qc1xDiv3H5EkUP6Wuc.jpg', '2017-01-11', 0, '', NULL, 3, '', 'HD', 5.9, '', NULL, NULL, '88', NULL, 0),
(180, 'tt5791536', 'My Happy Family', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/c2KhyDAS9dC5JVPvd1L9j4nyRrP.jpg', '2017-05-10', 0, '', NULL, 3, '', 'HD', 0, 'Tbilisi, Georgia, 2016: In a patriarchal society, an ordinary Georgian family lives with three generations under one roof. All are shocked when 52-year-old Manana decides to move out from her parentsâ€™ home and live alone. Without her family and her husband, a journey into the unknown begins.', NULL, NULL, '119', NULL, 0),
(181, 'tt6149818', 'Quest', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/5qzVA8DcP1niBDLeYYFFRVyxCjn.jpg', '2017-07-26', 0, '', NULL, 3, '', 'HD', 0, 'For over a decade, this portrait of a North Philadelphia family and the creative sanctuary offered by their home music studio was filmed with vÃ©ritÃ© intimacy. The family\'s 10-year journey is an illumination of race and class in America, and it\'s a testament to love, healing and hope.', NULL, NULL, '110', NULL, 0),
(182, 'tt4481514', 'The House', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/hkmWkVgirei79bwds23Hr0lWSXS.jpg', '2017-06-29', 0, '', NULL, 3, 'https://www.youtube.com/embed/tlhPYN3gXog', 'HD', 6.1, 'A dad convinces his friends to start an illegal casino in his basement after he and his wife spend their daughter\'s college fund.', NULL, NULL, '88', NULL, 0),
(183, 'tt1648190', 'The Dark Tower', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/i9GUSgddIqrroubiLsvvMRYyRy0.jpg', '2017-07-27', 0, '', NULL, 3, 'https://www.youtube.com/embed/GjwfqXTebIY', 'HD', 0, 'The last Gunslinger, Roland Deschain, has been locked in an eternal battle with Walter Oâ€™Dim, also known as the Man in Black, determined to prevent him from toppling the Dark Tower, which holds the universe together. With the fate of the worlds at stake, good and evil will collide in the ultimate battle as only Roland can defend the Tower from the Man in Black.', NULL, NULL, '0', NULL, 0),
(184, 'tt3567666', 'Stratton', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/6HyEx5AxxDwWdhUowXPr1hXjM1z.jpg', '2017-07-28', 0, '', NULL, 3, 'https://www.youtube.com/embed/bDBPPUWiJOg', 'HD', 0, 'A British Special Boat Service commando tracks down an international terrorist cell.', NULL, NULL, '94', NULL, 0),
(185, 'tt4123650', 'Alterscape', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/3AOKsvtqRPsSQaMnBmOPi4VNuVU.jpg', '2017-07-15', 0, '', NULL, 3, '', 'HD', 0, 'After a failed suicide attempt, a young man coping with loss and depression, submits to a series of trials that fine-tune human emotions, but his unique reaction to the tests send him on a journey that transcends both physical and perceived reality.', NULL, NULL, '90', NULL, 0),
(186, 'tt6139974', 'Bad Dads', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2', '2017-07-14', 0, '', NULL, 3, '', 'HD', 0, 'Plot unknown. A spin-off from the 2016 comedy, \'Bad Moms.\'', NULL, NULL, '0', NULL, 0),
(187, 'tt5311972', 'Ostwind 3 - Aufbruch nach Ora', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2', '2017-07-27', 0, '', NULL, 3, '', 'HD', 0, NULL, NULL, NULL, '', NULL, 0),
(188, 'tt6347492', 'Alley Cat', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/w3P68nCVTlEOPCKddYNjRIp8pLC.jpg', '2017-07-15', 0, '', NULL, 3, '', 'HD', 0, 'A depressed ex-boxer (Yosuke Kubozuka) self-medicating for the injuries that ended his career finds a stray cat that brightens his day, but she is snatched up by a comical grungy mechanic (Kenji Furuya). Calling each other by the competing names they gave the cat, Maru and Lily, the odd pair is abruptly sucked into a political conspiracy with Saeko (Yui Ichikawa) and her son at the center. Witnessing another injustice in which the economic and social elite prey on the vulnerable, the two strays set off on the road to redemption, or at least peaceful cat ownership. (Fun fact: in addition to acting together in Alley Cat, Yosuke Kubozuka and Kenji â€œKJâ€ Furuya released the single â€œSoul Shipâ€ in 2017. Kubozuka performs as Manji Line and Furuya in the band Dragon Ash.) -JAPAN CUTS: Festival of New Japanese Film', NULL, NULL, '129', NULL, 0),
(189, 'tt5233558', 'The Death and Life of Marsha P. Johnson', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/omEz3bNE8mHyQGOWQHcmAgLFXZM.jpg', '2017-07-27', 0, '', NULL, 3, '', 'HD', 0, 'Describing herself as a \'street queen,\' Johnson was a tireless voice for LGBT pride since the days of Stonewall. Her death in 1992 was declared a suicide by the NYPD, but friends never accepted that version of events. This documentary is a celebration of Johnson\'s activism and legacy. And an attempt to solve the mystery of her demise.', NULL, NULL, '105', NULL, 0),
(190, 'tt6333060', 'Icarus', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/o4mwVivOJrEChag4yWI2D1BUpsx.jpg', '2017-07-26', 0, '', NULL, 3, '', 'HD', 0, 'While investigating the furtive world of illegal doping in sports, director Bryan Fogel connects with renegade Russian scientist Dr. Grigory Rodchenkovâ€”a pillar of his countryâ€™s â€œanti-dopingâ€ program. Over dozens of Skype calls, urine samples, and badly administered hormone injections, Fogel and Rodchenkov grow closer despite shocking allegations that place Rodchenkov at the center of Russiaâ€™s state-sponsored Olympic doping program.', NULL, NULL, '110', NULL, 0),
(191, 'tt2062700', 'Song to Song', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/rEvtGhNY2DQ4L8Ma6rpMhL6IbKM.jpg', '2017-03-17', 0, '', NULL, 3, 'https://www.youtube.com/embed/O4SrVkj84zc', 'HD', 5.7, 'In this modern love story set against the Austin, Texas music scene, two entangled couples â€” struggling songwriters Faye and BV, and music mogul Cook and the waitress whom he ensnares â€” chase success through a rock â€˜nâ€™ roll landscape of seduction and betrayal.', NULL, NULL, '129', NULL, 0),
(192, 'tt4799050', 'Rough Night', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/ttC00xcQ5UIO04kU8y0h5OAIYYJ.jpg', '2017-06-15', 0, '', NULL, 3, 'https://www.youtube.com/embed/oIB4jNYeRY4', 'HD', 5.3, 'Five best friends from college reunite 10 years later for a wild bachelorette weekend in Miami. Their hard partying takes a hilariously dark turn when they accidentally kill a male stripper. Amidst the craziness of trying to cover it up, they\'re ultimately brought closer together when it matters most.', NULL, NULL, '101', NULL, 0),
(193, 'tt1082807', 'The Belko Experiment', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/faJK0dP3S92kQoKtO4LZMjy41kf.jpg', '2017-03-17', 0, '', NULL, 3, 'https://www.youtube.com/embed/qZNfwayNLL0', 'HD', 6.2, 'In a twisted social experiment, a group of 80 Americans are locked in their high-rise corporate office in BogotÃ¡, Colombia and ordered by an unknown voice coming from the company\'s intercom system to participate in a deadly game of kill or be killed.', NULL, NULL, '88', NULL, 0),
(194, 'tt1935897', 'Amityville: The Awakening', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/d2BmX5YJchuDdnI3UL0mj3PtmaG.jpg', '2017-06-30', 0, '', NULL, 3, 'https://www.youtube.com/embed/pCN3Ouo-Hys', 'HD', 7.4, 'Belle, her little sister, and her comatose twin brother move into a new house with their single mother Joan in order to save money to help pay for her brother\'s expensive healthcare. But when strange phenomena begin to occur in the house including the miraculous recovery of her brother, Belle begins to suspect her Mother isn\'t telling her everything and soon realizes they just moved into the infamous Amityville house.', NULL, NULL, '85', NULL, 0),
(195, 'tt5013056', 'Dunkirk', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/bOXBV303Fgkzn2K4FeKDc0O31q4.jpg', '2017-07-19', 0, '', NULL, 3, 'https://www.youtube.com/embed/PC460OxDNhc', 'HD', 0, 'Miraculous evacuation of Allied soldiers from Belgium, Britain, Canada, and France, who were cut off and surrounded by the German army from the beaches and harbor of Dunkirk, France, between May 26- June 04, 1940, during Battle of France in World War II.', NULL, NULL, '106', NULL, 0),
(196, 'tt1691916', 'Before I Fall', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/eowzonDJMCuNXoJGVkP9Z7oCmiM.jpg', '2017-03-03', 0, '', NULL, 3, 'https://www.youtube.com/embed/q3Zyy4ZXegE', 'HD', 6.4, 'Samantha Kingston has everything. Then, everything changes. After one fateful night, she wakes up with no future at all. Trapped into reliving the same day over and over, she begins to question just how perfect her life really was.', NULL, NULL, '98', NULL, 0),
(197, 'tt4633690', 'Shot Caller', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/3qau1l7STZaYHu9xslG4Jd9UHBR.jpg', '2017-07-13', 0, '', NULL, 3, '', 'HD', 6.8, 'A newly released prison gangster is forced by the leaders of his gang to orchestrate a major crime with a brutal rival gang on the streets of Southern California.', NULL, NULL, '0', NULL, 0),
(198, 'tt4287320', 'The Circle', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/bQVqd5rWrx5GbXhJNuvKy4Viz6j.jpg', '2017-04-27', 0, '', NULL, 3, 'https://www.youtube.com/embed/QUlr8Am4zQ0', 'HD', 5.2, 'A young tech worker takes a job at a powerful Internet corporation, quickly rises up the company\'s ranks, and soon finds herself in a perilous situation concerning privacy, surveillance and freedom. She comes to learn that her decisions and actions will determine the future of humanity.', NULL, NULL, '110', NULL, 0),
(199, 'tt5592248', 'The Beguiled', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/yCt9jWUFNpWLSiNpShZr9JFF9oD.jpg', '2017-06-23', 0, '', NULL, 3, 'https://www.youtube.com/embed/_r_FSRbuZ9Y', 'HD', 6.1, 'During the Civil War, at a Southern girlsâ€™ boarding school, young women take in an injured enemy soldier. As they provide refuge and tend to his wounds, the house is taken over with sexual tension and dangerous rivalries, and taboos are broken in an unexpected turn of events.', NULL, NULL, '91', NULL, 0),
(200, 'tt5322012', 'Wish Upon', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/u0vnocj57vJt5DHoBEqUOD1G4SU.jpg', '2017-06-30', 0, '', NULL, 3, 'https://www.youtube.com/embed/VZjBpt5mCCA', 'HD', 4, 'A teenage girl discovers a box with magical powers, but those powers comes with a deadly price.', NULL, NULL, '90', NULL, 0),
(201, 'tt4877122', 'The Emoji Movie', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/rrH6RXTszMlSgQDHKKNW6zLyCdE.jpg', '2017-07-28', 0, '', NULL, 3, 'https://www.youtube.com/embed/KFv5bgIOlg4', 'HD', 0, 'Gene, a muli-expressional emoji, sets out on a journey to become a normal emoji.', NULL, NULL, '0', NULL, 0),
(202, 'tt3564472', 'Girls Trip', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/lyKXdhDQwIgKsema88cg1yHJYP.jpg', '2017-07-21', 0, '', NULL, 3, 'https://www.youtube.com/embed/2A3TcTcnYFQ', 'HD', 0, 'Four girlfriends take a trip to New Orleans for an annual festival and, along the way, rediscover their wild sides and strengthen the bonds of sisterhood.', NULL, NULL, '122', NULL, 0),
(203, 'tt2486862', 'Battle Scars', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/s6hNCCH0TFS2aTscSkQozOWKX6f.jpg', '2017-07-14', 0, '', NULL, 3, 'https://www.youtube.com/embed/BVXAA-Pv2f4', 'HD', 0, 'Every Marine has a story, some they\'re proud of, other they\'d rather forget. After coming home from his tour of duty in Afghanistan suffering from PTSD, Luke Stephens wants nothing more than to forget. What happens when our soldiers come back to Another Stateside?', NULL, NULL, '97', NULL, 0),
(204, 'tt6099284', '40 Years of Rocky: The Birth of a Classic', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/weI7ZQ6whXhBHmdKFg38vH1LaOa.jpg', '2017-07-29', 0, '', NULL, 3, '', 'HD', 0, 'Sylvester Stallone and John G. Avildsen narrate behind-the-scenes footage from the making of \"Rocky\" to mark the film\'s 40th anniversary.', NULL, NULL, '60', NULL, 0),
(205, 'tt6213996', 'Ada for Mayor', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2', '2017-07-26', 0, '', NULL, 3, '', 'HD', 0, 'A 2016 documentary.', NULL, NULL, '88', NULL, 0),
(206, 'tt5044746', 'The Bentley Effect', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/w97PD32h5vV7c9w3V2yuGqmug2o.jpg', '2017-07-15', 0, '', NULL, 3, 'https://www.youtube.com/embed/CLnRD3neUFo', 'HD', 0, 'When the Northern Rivers of NSW community found their home being threatened by gas field industrialization, a critical mass of citizens from all walks of life responded to the call.Told through the eyes of the \"Protectors\" over a five-year period and inter-cut with fresh insight from some of the world\'s leading social commentators, this feature documentary captures and celebrates what is described as the non-violent \'Eureka Stockade\' of our time.', NULL, NULL, '85', NULL, 0),
(207, 'tt5723550', 'INAATE/SE/ [it shines a certain way. to a certain place/it flies. falls./]', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2', '2017-07-27', 0, '', NULL, 3, '', 'HD', 0, 'With this first feature, Adam and Zack Khalil reinvent the historical narrative in the form of a kaleidoscopic and conjecture-rich essay. Their work makes their community, in the Upper Peninsula of Michigan, resonate with the ancient Ojibway prophecy of the seven fires, a premonition of the arrival of white people. Using video effects, animation and off-kilter editing, and combining testimonials, experimental sequences and performances, the filmmakers brilliantly demolish linear filmic narrative â€“ not to mention the Western concept of history. Overflowing with inventiveness, INAATE/SE/ revives traditional spirituality and culture, confronts an obfuscated colonial reality, and works toward building a modern identity.', NULL, NULL, '71', NULL, 0),
(208, 'tt1933667', 'The Wizard of Lies', 2017, 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/8k4lJ4e0hvDQtKpItXmmPdw69sO.jpg', '2017-05-20', 0, 'IPLwfXRTPlEa', NULL, 3, 'https://www.youtube.com/embed/jo_CYCumZgI', 'HD', 6.6, 'A look behind the scenes at Bernie Madoff\'s massive Ponzi scheme, how it was perpetrated on the public and the trail of destruction it left in its wake, both for the victims and Madoff\'s family.', NULL, NULL, '133', NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `flixyreports`
--

CREATE TABLE `flixyreports` (
  `id` int(11) NOT NULL,
  `epid` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `text` longtext NOT NULL,
  `time` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `flixyreports`
--

INSERT INTO `flixyreports` (`id`, `epid`, `email`, `text`, `time`, `status`) VALUES
(1, 208, '', 'The Wizard of Lies Movie ', 1499018721, 1),
(2, 208, '', 'The Wizard of Lies Movie ', 1499018922, 1),
(3, 208, '', 'The Wizard of Lies Movie ', 1499018972, 1),
(4, 208, '', 'The Wizard of Lies Movie ', 1499019276, 1),
(5, 208, '', 'The Wizard of Lies Movie ', 1499019276, 1),
(6, 208, '', 'The Wizard of Lies Movie ', 1499019297, 1),
(7, 208, '', 'The Wizard of Lies Movie ', 1499019498, 1),
(8, 208, '', 'The Wizard of Lies Movie ', 1499019508, 1),
(9, 208, '', 'The Wizard of Lies Movie ', 1499019886, 1),
(10, 208, '', 'The Wizard of Lies Movie ', 1499020114, 1),
(11, 208, '', 'The Wizard of Lies Movie ', 1499020114, 1),
(12, 208, '', 'The Wizard of Lies Movie ', 1499020131, 1),
(13, 208, '', 'The Wizard of Lies Movie ', 1499020131, 1),
(14, 208, '', 'The Wizard of Lies Movie ', 1499020166, 1),
(15, 1, '', 'Robin Hood Movie ', 1499026898, 1),
(16, 2, '', 'Bereavement Movie ', 1499026898, 1),
(17, 3, '', 'Inception Movie ', 1499026898, 1),
(18, 4, '', 'Shutter Island Movie ', 1499026899, 1),
(19, 5, '', 'Scott Pilgrim vs. the World Movie ', 1499026899, 1),
(20, 6, '', 'Leap Year Movie ', 1499026899, 1),
(21, 7, '', 'Kick-Ass Movie ', 1499026899, 1),
(22, 8, '', 'The Social Network Movie ', 1499026899, 1),
(23, 9, '', 'Black Swan Movie ', 1499026899, 1),
(24, 10, '', 'Clash of the Titans Movie ', 1499026900, 1),
(25, 11, '', 'TRON: Legacy Movie ', 1499026900, 1),
(26, 12, '', 'Iron Man 2 Movie ', 1499026900, 1),
(27, 13, '', 'Alice in Wonderland Movie ', 1499026900, 1),
(28, 14, '', 'Harry Potter and the Deathly Hallows: Part 1 Movie ', 1499026900, 1),
(29, 15, '', 'True Grit Movie ', 1499026900, 1),
(30, 16, '', 'The Fighter Movie ', 1499026900, 1),
(31, 17, '', 'The King\'s Speech Movie ', 1499026900, 1),
(32, 18, '', 'Grown Ups Movie ', 1499026900, 1),
(33, 19, '', 'Easy A Movie ', 1499026900, 1),
(34, 20, '', 'Despicable Me Movie ', 1499026900, 1),
(35, 21, '', 'Legion Movie ', 1499026900, 1),
(36, 22, '', 'The Town Movie ', 1499026901, 1),
(37, 23, '', '127 Hours Movie ', 1499026901, 1),
(38, 24, '', 'How to Train Your Dragon Movie ', 1499026901, 1),
(39, 25, '', 'The A-Team Movie ', 1499026901, 1),
(40, 26, '', 'The Karate Kid Movie ', 1499026901, 1),
(41, 27, '', 'The Expendables Movie ', 1499026901, 1),
(42, 28, '', 'The Book of Eli Movie ', 1499026901, 1),
(43, 29, '', 'The Last Airbender Movie ', 1499026901, 1),
(44, 30, '', 'Prince of Persia: The Sands of Time Movie ', 1499026901, 1),
(45, 31, '', 'Insidious Movie ', 1499026901, 1),
(46, 32, '', 'Blue Valentine Movie ', 1499026901, 1),
(47, 33, '', 'Sex and the City 2 Movie ', 1499026901, 1),
(48, 34, '', 'Resident Evil: Afterlife Movie ', 1499026901, 1),
(49, 35, '', 'The Way Back Movie ', 1499026901, 1),
(50, 36, '', 'Joe Movie ', 1499026901, 1),
(51, 37, '', 'I Spit on Your Grave Movie ', 1499026902, 1),
(52, 38, '', 'Salt Movie ', 1499026902, 1),
(53, 39, '', 'Predators Movie ', 1499026902, 1),
(54, 40, '', 'My Bloody Valentine Movie ', 1499026902, 1),
(55, 41, '', 'Step Up 3D Movie ', 1499026902, 1),
(56, 42, '', 'The Tourist Movie ', 1499026902, 1),
(57, 43, '', 'The Wolfman Movie ', 1499026902, 1),
(58, 44, '', 'The Sorcerer\'s Apprentice Movie ', 1499026902, 1),
(59, 45, '', 'The Twilight Saga: Eclipse Movie ', 1499026902, 1),
(60, 46, '', 'Hot Tub Time Machine Movie ', 1499026902, 1),
(61, 47, '', 'The Bounty Hunter Movie ', 1499026902, 1),
(62, 48, '', 'Green Zone Movie ', 1499026902, 1),
(63, 49, '', 'Jonah Hex Movie ', 1499026902, 1),
(64, 50, '', 'Saw 3D Movie ', 1499026902, 1),
(65, 51, '', 'Shrek Forever After Movie ', 1499026903, 1),
(66, 57, '', 'A Knight\'s Tale Movie ', 1499026903, 1),
(67, 208, '', 'The Wizard of Lies Movie ', 1499026903, 1),
(68, 1, '', 'Robin Hood Movie ', 1499026925, 1),
(69, 2, '', 'Bereavement Movie ', 1499026925, 1),
(70, 3, '', 'Inception Movie ', 1499026925, 1),
(71, 4, '', 'Shutter Island Movie ', 1499026925, 1),
(72, 5, '', 'Scott Pilgrim vs. the World Movie ', 1499026925, 1),
(73, 6, '', 'Leap Year Movie ', 1499026925, 1),
(74, 7, '', 'Kick-Ass Movie ', 1499026925, 1),
(75, 8, '', 'The Social Network Movie ', 1499026926, 1),
(76, 9, '', 'Black Swan Movie ', 1499026926, 1),
(77, 10, '', 'Clash of the Titans Movie ', 1499026926, 1),
(78, 11, '', 'TRON: Legacy Movie ', 1499026926, 1),
(79, 12, '', 'Iron Man 2 Movie ', 1499026926, 1),
(80, 13, '', 'Alice in Wonderland Movie ', 1499026926, 1),
(81, 14, '', 'Harry Potter and the Deathly Hallows: Part 1 Movie ', 1499026926, 1),
(82, 15, '', 'True Grit Movie ', 1499026926, 1),
(83, 16, '', 'The Fighter Movie ', 1499026926, 1),
(84, 17, '', 'The King\'s Speech Movie ', 1499026926, 1),
(85, 18, '', 'Grown Ups Movie ', 1499026926, 1),
(86, 19, '', 'Easy A Movie ', 1499026926, 1),
(87, 20, '', 'Despicable Me Movie ', 1499026926, 1),
(88, 21, '', 'Legion Movie ', 1499026926, 1),
(89, 22, '', 'The Town Movie ', 1499026927, 1),
(90, 23, '', '127 Hours Movie ', 1499026927, 1),
(91, 24, '', 'How to Train Your Dragon Movie ', 1499026927, 1),
(92, 25, '', 'The A-Team Movie ', 1499026927, 1),
(93, 26, '', 'The Karate Kid Movie ', 1499026927, 1),
(94, 27, '', 'The Expendables Movie ', 1499026927, 1),
(95, 28, '', 'The Book of Eli Movie ', 1499026927, 1),
(96, 29, '', 'The Last Airbender Movie ', 1499026927, 1),
(97, 30, '', 'Prince of Persia: The Sands of Time Movie ', 1499026927, 1),
(98, 31, '', 'Insidious Movie ', 1499026927, 1),
(99, 32, '', 'Blue Valentine Movie ', 1499026927, 1),
(100, 33, '', 'Sex and the City 2 Movie ', 1499026927, 1),
(101, 34, '', 'Resident Evil: Afterlife Movie ', 1499026927, 1),
(102, 35, '', 'The Way Back Movie ', 1499026927, 1),
(103, 36, '', 'Joe Movie ', 1499026927, 1),
(104, 37, '', 'I Spit on Your Grave Movie ', 1499026927, 1),
(105, 38, '', 'Salt Movie ', 1499026927, 1),
(106, 39, '', 'Predators Movie ', 1499026927, 1),
(107, 40, '', 'My Bloody Valentine Movie ', 1499026928, 1),
(108, 41, '', 'Step Up 3D Movie ', 1499026928, 1),
(109, 42, '', 'The Tourist Movie ', 1499026928, 1),
(110, 43, '', 'The Wolfman Movie ', 1499026928, 1),
(111, 44, '', 'The Sorcerer\'s Apprentice Movie ', 1499026928, 1),
(112, 45, '', 'The Twilight Saga: Eclipse Movie ', 1499026928, 1),
(113, 46, '', 'Hot Tub Time Machine Movie ', 1499026928, 1),
(114, 47, '', 'The Bounty Hunter Movie ', 1499026928, 1),
(115, 48, '', 'Green Zone Movie ', 1499026928, 1),
(116, 49, '', 'Jonah Hex Movie ', 1499026928, 1),
(117, 50, '', 'Saw 3D Movie ', 1499026928, 1),
(118, 51, '', 'Shrek Forever After Movie ', 1499026928, 1),
(119, 57, '', 'A Knight\'s Tale Movie ', 1499026928, 1),
(120, 208, '', 'The Wizard of Lies Movie ', 1499026928, 1),
(121, 1, '', 'Robin Hood Movie ', 1499026962, 1),
(122, 1, '', 'Robin Hood Movie ', 1499026970, 1),
(123, 1, '', 'Robin Hood Movie ', 1499026970, 1),
(124, 1, '', 'Robin Hood Movie ', 1499027599, 1),
(125, 2, '', 'Bereavement Movie ', 1499027599, 1),
(126, 3, '', 'Inception Movie ', 1499027599, 1),
(127, 4, '', 'Shutter Island Movie ', 1499027599, 1),
(128, 5, '', 'Scott Pilgrim vs. the World Movie ', 1499027599, 1),
(129, 6, '', 'Leap Year Movie ', 1499027599, 1),
(130, 7, '', 'Kick-Ass Movie ', 1499027599, 1),
(131, 8, '', 'The Social Network Movie ', 1499027599, 1),
(132, 9, '', 'Black Swan Movie ', 1499027599, 1),
(133, 10, '', 'Clash of the Titans Movie ', 1499027599, 1),
(134, 11, '', 'TRON: Legacy Movie ', 1499027599, 1),
(135, 12, '', 'Iron Man 2 Movie ', 1499027599, 1),
(136, 13, '', 'Alice in Wonderland Movie ', 1499027599, 1),
(137, 14, '', 'Harry Potter and the Deathly Hallows: Part 1 Movie ', 1499027599, 1),
(138, 15, '', 'True Grit Movie ', 1499027600, 1),
(139, 16, '', 'The Fighter Movie ', 1499027600, 1),
(140, 17, '', 'The King\'s Speech Movie ', 1499027600, 1),
(141, 18, '', 'Grown Ups Movie ', 1499027600, 1),
(142, 19, '', 'Easy A Movie ', 1499027600, 1),
(143, 20, '', 'Despicable Me Movie ', 1499027600, 1),
(144, 21, '', 'Legion Movie ', 1499027600, 1),
(145, 22, '', 'The Town Movie ', 1499027600, 1),
(146, 23, '', '127 Hours Movie ', 1499027600, 1),
(147, 24, '', 'How to Train Your Dragon Movie ', 1499027600, 1),
(148, 25, '', 'The A-Team Movie ', 1499027600, 1),
(149, 26, '', 'The Karate Kid Movie ', 1499027600, 1),
(150, 27, '', 'The Expendables Movie ', 1499027600, 1),
(151, 28, '', 'The Book of Eli Movie ', 1499027600, 1),
(152, 29, '', 'The Last Airbender Movie ', 1499027600, 1),
(153, 30, '', 'Prince of Persia: The Sands of Time Movie ', 1499027600, 1),
(154, 31, '', 'Insidious Movie ', 1499027600, 1),
(155, 32, '', 'Blue Valentine Movie ', 1499027600, 1),
(156, 33, '', 'Sex and the City 2 Movie ', 1499027600, 1),
(157, 34, '', 'Resident Evil: Afterlife Movie ', 1499027600, 1),
(158, 35, '', 'The Way Back Movie ', 1499027600, 1),
(159, 36, '', 'Joe Movie ', 1499027600, 1),
(160, 37, '', 'I Spit on Your Grave Movie ', 1499027601, 1),
(161, 38, '', 'Salt Movie ', 1499027601, 1),
(162, 39, '', 'Predators Movie ', 1499027601, 1),
(163, 40, '', 'My Bloody Valentine Movie ', 1499027601, 1),
(164, 41, '', 'Step Up 3D Movie ', 1499027601, 1),
(165, 42, '', 'The Tourist Movie ', 1499027601, 1),
(166, 43, '', 'The Wolfman Movie ', 1499027601, 1),
(167, 44, '', 'The Sorcerer\'s Apprentice Movie ', 1499027601, 1),
(168, 45, '', 'The Twilight Saga: Eclipse Movie ', 1499027601, 1),
(169, 46, '', 'Hot Tub Time Machine Movie ', 1499027601, 1),
(170, 47, '', 'The Bounty Hunter Movie ', 1499027601, 1),
(171, 48, '', 'Green Zone Movie ', 1499027601, 1),
(172, 49, '', 'Jonah Hex Movie ', 1499027601, 1),
(173, 50, '', 'Saw 3D Movie ', 1499027601, 1),
(174, 51, '', 'Shrek Forever After Movie ', 1499027601, 1),
(175, 57, '', 'A Knight\'s Tale Movie ', 1499027607, 1),
(176, 99, '', 'Transformers: The Last Knight Movie ', 1499027642, 1),
(177, 100, '', 'Gifted Movie ', 1499027642, 1),
(178, 101, '', 'First Kill Movie ', 1499027642, 1),
(179, 102, '', 'Lady Macbeth Movie ', 1499027642, 1),
(180, 103, '', 'Landline Movie ', 1499027642, 1),
(181, 104, '', 'April\'s Daughter Movie ', 1499027642, 1),
(182, 105, '', 'Barrage Movie ', 1499027642, 1),
(183, 106, '', 'Les As de la Jungle Movie ', 1499027642, 1),
(184, 107, '', 'mon mon mon MONSTERS Movie ', 1499027643, 1),
(185, 108, '', 'John Wick: Chapter 2 Movie ', 1499027643, 1),
(186, 109, '', 'The Big Sick Movie ', 1499027643, 1),
(187, 110, '', 'Wu Kong Movie ', 1499027643, 1),
(188, 111, '', 'Rebel in the Rye Movie ', 1499027643, 1),
(189, 112, '', 'David Lynch: The Art Life Movie ', 1499027643, 1),
(190, 113, '', 'Sales Gosses Movie ', 1499027643, 1),
(191, 114, '', 'Menashe Movie ', 1499027643, 1),
(192, 115, '', 'Defiant Lives Movie ', 1499027643, 1),
(193, 116, '', 'Brava Movie ', 1499027643, 1),
(194, 117, '', 'No Game No Life: Zero Movie ', 1499027643, 1),
(195, 118, '', 'The Mummy Movie ', 1499027643, 1),
(196, 119, '', 'Overdrive Movie ', 1499027643, 1),
(197, 120, '', 'A Stork\'s Journey Movie ', 1499027643, 1),
(198, 121, '', 'The Gracefield Incident Movie ', 1499027643, 1),
(199, 122, '', 'Catastrophe Movie ', 1499027643, 1),
(200, 123, '', 'Bronx Gothic Movie ', 1499027643, 1),
(201, 124, '', 'El cerro de los buitres Movie ', 1499027643, 1),
(202, 125, '', 'LumiÃ¨reÂ ! L\'aventure commence Movie ', 1499027643, 1),
(203, 126, '', 'Cause of Death: Unknown Movie ', 1499027643, 1),
(204, 127, '', 'Chunkzz Movie ', 1499027644, 1),
(205, 128, '', 'Baby Driver Movie ', 1499027644, 1),
(206, 129, '', 'Valerian and the City of a Thousand Planets Movie ', 1499027644, 1),
(207, 130, '', 'My Cousin Rachel Movie ', 1499027644, 1),
(208, 131, '', 'Jagga Jasoos Movie ', 1499027644, 1),
(209, 132, '', 'Feed Movie ', 1499027644, 1),
(210, 133, '', 'Viceroy\'s House Movie ', 1499027644, 1),
(211, 134, '', 'Villain Movie ', 1499027644, 1),
(212, 135, '', 'The Winter Movie ', 1499027644, 1),
(213, 136, '', 'Glory Movie ', 1499027644, 1),
(214, 137, '', 'Despicable Me 3 Movie ', 1499027644, 1),
(215, 138, '', 'Our Time Will Come Movie ', 1499027644, 1),
(216, 139, '', 'A Family Man Movie ', 1499027644, 1),
(217, 140, '', 'Embrasse-moi ! Movie ', 1499027644, 1),
(218, 141, '', 'Paris Can Wait Movie ', 1499027645, 1),
(219, 142, '', 'Mind Over Matter Movie ', 1499027645, 1),
(220, 143, '', 'Her Sketchbook Movie ', 1499027645, 1),
(221, 144, '', 'Earth 2.0 Movie ', 1499027645, 1),
(222, 145, '', 'Awaken the Shadowman Movie ', 1499027645, 1),
(223, 146, '', 'Spider-Man: Homecoming Movie ', 1499027645, 1),
(224, 147, '', 'Atomic Blonde Movie ', 1499027645, 1),
(225, 148, '', 'Hounds of Love Movie ', 1499027645, 1),
(226, 149, '', 'Mission Pays Basque Movie ', 1499027645, 1),
(227, 150, '', 'The Son of Bigfoot Movie ', 1499027645, 1),
(228, 151, '', 'Love And Other Cults Movie ', 1499027645, 1),
(229, 152, '', 'Passade Movie ', 1499027645, 1),
(230, 153, '', 'Ancien and the Magic Tablet Movie ', 1499027645, 1),
(231, 154, '', 'Personal Affairs Movie ', 1499027645, 1),
(232, 155, '', 'War for the Planet of the Apes Movie ', 1499027645, 1),
(233, 156, '', 'Captain Underpants: The First Epic Movie Movie ', 1499027645, 1),
(234, 157, '', 'Hickok Movie ', 1499027645, 1),
(235, 158, '', 'Ash Flower Movie ', 1499027646, 1),
(236, 159, '', 'Inversion Movie ', 1499027646, 1),
(237, 160, '', 'Berlin Falling Movie ', 1499027646, 1),
(238, 161, '', 'Computers & Imagination Movie ', 1499027646, 1),
(239, 162, '', 'Small Town Killers Movie ', 1499027646, 1),
(240, 163, '', 'A Fantastic Woman Movie ', 1499027646, 1),
(241, 164, '', 'Cars 3 Movie ', 1499027646, 1),
(242, 165, '', 'Their Finest Movie ', 1499027646, 1),
(243, 166, '', 'Austin Found Movie ', 1499027646, 1),
(244, 167, '', 'The Nile Hilton Incident Movie ', 1499027646, 1),
(245, 168, '', '100 % Coco Movie ', 1499027646, 1),
(246, 169, '', 'Gintama Movie ', 1499027647, 1),
(247, 170, '', 'I Want to Eat Your Pancreas Movie ', 1499027647, 1),
(248, 171, '', 'The Day After Movie ', 1499027647, 1),
(249, 172, '', 'A Hustler\'s Diary Movie ', 1499027647, 1),
(250, 173, '', '47 Meters Down Movie ', 1499027647, 1),
(251, 174, '', 'My Blind Date with Life Movie ', 1499027647, 1),
(252, 175, '', 'He Even Has Your Eyes Movie ', 1499027647, 1),
(253, 176, '', 'With Open Arms Movie ', 1499027647, 1),
(254, 177, '', 'Mary and the Witch\'s Flower Movie ', 1499027647, 1),
(255, 178, '', 'Project Ghazi Movie ', 1499027647, 1),
(256, 179, '', 'Scribe Movie ', 1499027647, 1),
(257, 180, '', 'My Happy Family Movie ', 1499027647, 1),
(258, 181, '', 'Quest Movie ', 1499027647, 1),
(259, 182, '', 'The House Movie ', 1499027648, 1),
(260, 183, '', 'The Dark Tower Movie ', 1499027648, 1),
(261, 184, '', 'Stratton Movie ', 1499027648, 1),
(262, 185, '', 'Alterscape Movie ', 1499027648, 1),
(263, 186, '', 'Bad Dads Movie ', 1499027648, 1),
(264, 187, '', 'Ostwind 3 - Aufbruch nach Ora Movie ', 1499027648, 1),
(265, 188, '', 'Alley Cat Movie ', 1499027649, 1),
(266, 189, '', 'The Death and Life of Marsha P. Johnson Movie ', 1499027649, 1),
(267, 190, '', 'Icarus Movie ', 1499027649, 1),
(268, 191, '', 'Song to Song Movie ', 1499027649, 1),
(269, 192, '', 'Rough Night Movie ', 1499027649, 1),
(270, 193, '', 'The Belko Experiment Movie ', 1499027649, 1),
(271, 194, '', 'Amityville: The Awakening Movie ', 1499027649, 1),
(272, 195, '', 'Dunkirk Movie ', 1499027649, 1),
(273, 196, '', 'Before I Fall Movie ', 1499027649, 1),
(274, 197, '', 'Shot Caller Movie ', 1499027649, 1),
(275, 198, '', 'The Circle Movie ', 1499027650, 1),
(276, 199, '', 'The Beguiled Movie ', 1499027650, 1),
(277, 200, '', 'Wish Upon Movie ', 1499027650, 1),
(278, 201, '', 'The Emoji Movie Movie ', 1499027650, 1),
(279, 202, '', 'Girls Trip Movie ', 1499027650, 1),
(280, 203, '', 'Battle Scars Movie ', 1499027650, 1),
(281, 204, '', '40 Years of Rocky: The Birth of a Classic Movie ', 1499027650, 1),
(282, 205, '', 'Ada for Mayor Movie ', 1499027650, 1),
(283, 206, '', 'The Bentley Effect Movie ', 1499027651, 1),
(284, 207, '', 'INAATE/SE/ [it shines a certain way. to a certain place/it flies. falls./] Movie ', 1499027651, 1),
(285, 208, '', 'The Wizard of Lies Movie ', 1499027651, 1),
(286, 1, '', 'Robin Hood Movie ', 1499027753, 1),
(287, 2, '', 'Bereavement Movie ', 1499027753, 1),
(288, 3, '', 'Inception Movie ', 1499027753, 1),
(289, 4, '', 'Shutter Island Movie ', 1499027753, 1),
(290, 5, '', 'Scott Pilgrim vs. the World Movie ', 1499027753, 1),
(291, 6, '', 'Leap Year Movie ', 1499027753, 1),
(292, 7, '', 'Kick-Ass Movie ', 1499027753, 1),
(293, 8, '', 'The Social Network Movie ', 1499027753, 1),
(294, 9, '', 'Black Swan Movie ', 1499027753, 1),
(295, 10, '', 'Clash of the Titans Movie ', 1499027753, 1),
(296, 11, '', 'TRON: Legacy Movie ', 1499027753, 1),
(297, 12, '', 'Iron Man 2 Movie ', 1499027754, 1),
(298, 13, '', 'Alice in Wonderland Movie ', 1499027754, 1),
(299, 14, '', 'Harry Potter and the Deathly Hallows: Part 1 Movie ', 1499027754, 1),
(300, 15, '', 'True Grit Movie ', 1499027754, 1),
(301, 16, '', 'The Fighter Movie ', 1499027754, 1),
(302, 17, '', 'The King\'s Speech Movie ', 1499027754, 1),
(303, 18, '', 'Grown Ups Movie ', 1499027754, 1),
(304, 19, '', 'Easy A Movie ', 1499027754, 1),
(305, 20, '', 'Despicable Me Movie ', 1499027754, 1),
(306, 21, '', 'Legion Movie ', 1499027754, 1),
(307, 22, '', 'The Town Movie ', 1499027754, 1),
(308, 23, '', '127 Hours Movie ', 1499027754, 1),
(309, 24, '', 'How to Train Your Dragon Movie ', 1499027754, 1),
(310, 25, '', 'The A-Team Movie ', 1499027754, 1),
(311, 26, '', 'The Karate Kid Movie ', 1499027754, 1),
(312, 27, '', 'The Expendables Movie ', 1499027754, 1),
(313, 28, '', 'The Book of Eli Movie ', 1499027754, 1),
(314, 29, '', 'The Last Airbender Movie ', 1499027754, 1),
(315, 30, '', 'Prince of Persia: The Sands of Time Movie ', 1499027754, 1),
(316, 31, '', 'Insidious Movie ', 1499027754, 1),
(317, 32, '', 'Blue Valentine Movie ', 1499027754, 1),
(318, 33, '', 'Sex and the City 2 Movie ', 1499027754, 1),
(319, 34, '', 'Resident Evil: Afterlife Movie ', 1499027754, 1),
(320, 35, '', 'The Way Back Movie ', 1499027754, 1),
(321, 36, '', 'Joe Movie ', 1499027754, 1),
(322, 37, '', 'I Spit on Your Grave Movie ', 1499027755, 1),
(323, 38, '', 'Salt Movie ', 1499027755, 1),
(324, 39, '', 'Predators Movie ', 1499027755, 1),
(325, 40, '', 'My Bloody Valentine Movie ', 1499027755, 1),
(326, 41, '', 'Step Up 3D Movie ', 1499027755, 1),
(327, 42, '', 'The Tourist Movie ', 1499027755, 1),
(328, 43, '', 'The Wolfman Movie ', 1499027755, 1),
(329, 44, '', 'The Sorcerer\'s Apprentice Movie ', 1499027755, 1),
(330, 45, '', 'The Twilight Saga: Eclipse Movie ', 1499027755, 1),
(331, 46, '', 'Hot Tub Time Machine Movie ', 1499027755, 1),
(332, 47, '', 'The Bounty Hunter Movie ', 1499027755, 1),
(333, 48, '', 'Green Zone Movie ', 1499027755, 1),
(334, 49, '', 'Jonah Hex Movie ', 1499027755, 1),
(335, 50, '', 'Saw 3D Movie ', 1499027755, 1),
(336, 51, '', 'Shrek Forever After Movie ', 1499027755, 1),
(337, 57, '', 'A Knight\'s Tale Movie ', 1499027759, 1),
(338, 208, '', 'The Wizard of Lies Movie ', 1499027783, 1),
(339, 208, '', 'The Wizard of Lies Movie ', 1499184362, 1),
(340, 2, '', 'Bereavement Movie ', 1499184363, 1),
(341, 2, '', 'Bereavement Movie ', 1499184366, 1),
(342, 2, '', 'Bereavement Movie ', 1499184366, 1),
(343, 208, '', 'The Wizard of Lies Movie ', 1499184377, 1),
(344, 208, '', 'The Wizard of Lies Movie ', 1499184377, 1),
(345, 208, '', 'The Wizard of Lies Movie ', 1499737546, 1),
(346, 46, '', 'Hot Tub Time Machine Movie ', 1499819463, 1),
(347, 46, '', 'Hot Tub Time Machine Movie ', 1499819504, 1),
(348, 6, '', 'Leap Year Movie ', 1499819512, 1),
(349, 6, '', 'Leap Year Movie ', 1499819512, 1),
(350, 10, '', 'Clash of the Titans Movie ', 1499941998, 1),
(351, 208, '', 'The Wizard of Lies Movie ', 1499942333, 1),
(352, 10, '', 'Clash of the Titans Movie ', 1499942337, 1),
(353, 46, '', 'Hot Tub Time Machine Movie ', 1499942693, 1);

-- --------------------------------------------------------

--
-- Structure de la table `flixyseries`
--

CREATE TABLE `flixyseries` (
  `id` int(11) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'Ended',
  `description` text NOT NULL,
  `keywords` text NOT NULL,
  `name` varchar(255) NOT NULL,
  `pic` varchar(255) NOT NULL,
  `date` int(11) NOT NULL,
  `background` varchar(255) NOT NULL,
  `views` int(11) NOT NULL,
  `seasons` int(11) NOT NULL DEFAULT '1',
  `category` int(11) NOT NULL,
  `trailer` int(11) NOT NULL DEFAULT '0',
  `trailerembed` longtext NOT NULL,
  `releaseday` varchar(255) NOT NULL DEFAULT 'NONE',
  `change` varchar(255) NOT NULL,
  `TMDid` int(11) NOT NULL,
  `imdbID` varchar(255) NOT NULL,
  `dayofrelease` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `flixyseries`
--

INSERT INTO `flixyseries` (`id`, `status`, `description`, `keywords`, `name`, `pic`, `date`, `background`, `views`, `seasons`, `category`, `trailer`, `trailerembed`, `releaseday`, `change`, `TMDid`, `imdbID`, `dayofrelease`) VALUES
(1, 'Returning Series', 'Teresa flees Mexico after her drug-runner boyfriend is murdered. Settling in Spain, she looks to become the country\'s reigning drug smuggler and to avenge her lover\'s murder.', '', 'Queen of the South', 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/a4O7aYNwHpo1x7yYGyKTHrR9B7L.jpg', 2017, '', 0, 2, 0, 0, '', 'Thursday', '', 66676, 'tt1064899', '2016-06-23'),
(2, 'Returning Series', 'After a teenage girl\'s perplexing suicide, a classmate receives a series of tapes that unravel the mystery of her tragic choice.', '', '13 Reasons Why', 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/gpULvrgvq1Qidu8EpWevrRUfVhw.jpg', 2017, '', 0, 1, 0, 0, '', 'Friday', '', 66788, 'tt1837492', '2017-03-31'),
(3, 'Returning Series', 'The true story of Colombia\'s infamously violent and powerful drug cartels.', '', 'Narcos', 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/4LYKIdVJQkHZz3Grpcix4AD7zJt.jpg', 2017, '', 0, 3, 0, 0, '', 'Friday', '', 63351, 'tt2707408', '2015-08-28'),
(4, 'Returning Series', 'A contemporary and culturally resonant drama about a young programmer, Elliot, who suffers from a debilitating anti-social disorder and decides that he can only connect to people by hacking them. He wields his skills as a weapon to protect the people that he cares about. Elliot will find himself in the intersection between a cybersecurity firm he works for and the underworld organizations that are recruiting him to bring down corporate America.', '', 'Mr. Robot', 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/esN3gWb1P091xExLddD2nh4zmi3.jpg', 2017, '', 0, 2, 0, 0, '', 'Wednesday', '', 62560, 'tt4158110', '2015-05-27'),
(5, 'Returning Series', 'A contemporary and culturally resonant drama about a young programmer, Elliot, who suffers from a debilitating anti-social disorder and decides that he can only connect to people by hacking them. He wields his skills as a weapon to protect the people that he cares about. Elliot will find himself in the intersection between a cybersecurity firm he works for and the underworld organizations that are recruiting him to bring down corporate America.', '', 'Game of Thrones', 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/esN3gWb1P091xExLddD2nh4zmi3.jpg', 2017, '', 0, 7, 0, 0, '', 'Sunday', '', 1399, 'tt0944947', '2011-04-17'),
(6, 'Ended', 'An English teacher travels back in time to prevent the Kennedy assassination, but discovers he is attached to the life he has made in a bygone era.', 'assassination,based on novel,time travel,alternate history', '11.22.63', 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/N0BjnG960GEIFgMx40BjpwyUAc.jpg', 2017, '', 0, 1, 0, 0, '', 'Monday', '', 64464, 'tt2879552', '2016-02-15'),
(7, 'Ended', 'Dexter is an American television drama series. The series centers on Dexter Morgan, a blood spatter pattern analyst for \'Miami Metro Police Department\' who also leads a secret life as a serial killer, hunting down criminals who have slipped through the cracks of justice.', '', 'Dexter', 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/ydmfheI5cJ4NrgcupDEwk8I8y5q.jpg', 2017, '', 0, 8, 0, 0, '', 'Sunday', '', 1405, 'tt0773262', '2006-10-01');

-- --------------------------------------------------------

--
-- Structure de la table `flixyseries_cats`
--

CREATE TABLE `flixyseries_cats` (
  `id` int(11) NOT NULL,
  `serieid` int(11) NOT NULL,
  `catid` int(11) NOT NULL,
  `movieid` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `flixyseries_cats`
--

INSERT INTO `flixyseries_cats` (`id`, `serieid`, `catid`, `movieid`, `type`) VALUES
(1, 1, 80, 0, 0),
(2, 2, 18, 0, 0),
(3, 3, 80, 0, 0),
(4, 3, 18, 0, 0),
(5, 4, 80, 0, 0),
(6, 4, 18, 0, 0),
(10, 6, 18, 0, 0),
(11, 6, 10765, 0, 0),
(12, 5, 10759, 0, 0),
(13, 5, 18, 0, 0),
(14, 5, 10765, 0, 0),
(15, 7, 18, 0, 0),
(16, 7, 9648, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `flixyservers`
--

CREATE TABLE `flixyservers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `episodeid` int(11) NOT NULL,
  `date` int(11) DEFAULT NULL,
  `server` int(11) NOT NULL DEFAULT '1',
  `embed` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `flixyservers`
--

INSERT INTO `flixyservers` (`id`, `name`, `episodeid`, `date`, `server`, `embed`) VALUES
(1, NULL, 136, NULL, 1, 'https'),
(2, NULL, 136, NULL, 2, 'https://streamango.com'),
(3, NULL, 136, NULL, 3, 'https://streamango.com/embed/'),
(4, NULL, 136, NULL, 4, 'https://streamango.com/embed/cksdcmtabqsotffm/'),
(5, NULL, 136, NULL, 5, 'Game_of_Thrones_S06E10_720p_HDTV_x265_ShAaNiG_mkv_mp4');

-- --------------------------------------------------------

--
-- Structure de la table `flixysettings`
--

CREATE TABLE `flixysettings` (
  `flixy` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `flixysuggest`
--

CREATE TABLE `flixysuggest` (
  `id` int(11) NOT NULL,
  `search` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `seggestion` varchar(255) NOT NULL,
  `email` text NOT NULL,
  `time` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `flixysuggest`
--

INSERT INTO `flixysuggest` (`id`, `search`, `ip`, `seggestion`, `email`, `time`, `status`) VALUES
(1, 'Moonlight', '127.0.0.1', '', '', '1497163026', 0),
(2, 'Moonlight', '127.0.0.1', '', '', '1497163115', 0),
(3, 'Moonlight', '127.0.0.1', '', '', '1497163178', 0),
(4, '???????????? ????????????????????????????????????', '54.219.195.41', '', '', '1497225083', 0),
(5, '????????????|?????????????????????????????? ?????????????????????', '54.219.195.41', '', '', '1497225096', 0),
(6, 'south park', '188.25.254.219', '', '', '1497227655', 0),
(7, 'dark matter', '196.1.139.161', '', '', '1497227793', 0),
(8, 'the originals', '196.1.139.161', '', '', '1497227829', 0),
(9, 'orginals', '196.1.139.161', '', '', '1497227909', 0),
(10, 'narcos', '196.1.139.161', '', '', '1497227939', 0),
(11, 'Farewell ceremony held for Japanese-born pandas to return to China', '54.219.40.75', '', '', '1497228246', 0),
(12, '?????????????????????????????????????????????????????????????????????????????????????', '54.176.118.92', '', '', '1497228889', 0),
(13, '2017?06?05? ??60? (???)', '206.99.94.123', '', '', '1497229077', 0),
(14, '2017?06?05? ???? (????)', '54.219.23.167', '', '', '1497231174', 0),
(15, 'get ', '41.250.117.37', '', '', '1497231256', 0),
(16, '???????????????????????', '206.99.94.19', '', '', '1497231908', 0),
(17, '??????“??”15????“????”??', '206.99.94.19', '', '', '1497232591', 0),
(18, '2017???06???05??? ???????????? (??????????????????)', '54.193.152.241', '', '', '1497232913', 0),
(19, '??????????????????????????????????????????????????????????????????', '54.193.206.237', '', '', '1497233125', 0),
(20, '??????????????????20170602', '54.193.152.241', '', '', '1497233387', 0),
(21, 'China Report 2017.06.05 (16:30)', '54.241.53.243', '', '', '1497233776', 0),
(22, 'AKB48SHOW20170603', '54.219.195.41', '', '', '1497234352', 0),
(23, 'Insider attack kills 6 police, wounds 5 in S. Afghanistan', '54.219.23.167', '', '', '1497235703', 0),
(24, 'Game of thrones', '127.0.0.1', '', '', '1497546831', 0),
(25, 'Game of thrones season 1', '127.0.0.1', '', '', '1497549629', 0),
(26, 'Game of thrones season 1', '127.0.0.1', '', '', '1497549684', 0),
(27, 'Game of thrones season 1', '127.0.0.1', '', '', '1497549758', 0),
(28, 'Game of thrones season 1', '127.0.0.1', '', '', '1497549815', 0),
(29, 'Game of thrones season 1', '127.0.0.1', '', '', '1497549860', 0),
(30, 'Game of thrones season 1', '127.0.0.1', '', '', '1497549893', 0),
(31, 'Game of thrones season 1', '127.0.0.1', '', '', '1497549944', 0),
(32, 'Game of thrones season 1', '127.0.0.1', '', '', '1497549962', 0),
(33, 'Game of thrones season 1', '127.0.0.1', '', '', '1497550047', 0),
(34, 'Game of thrones', '127.0.0.1', '', '', '1497550115', 0),
(35, 'Game of thrones', '127.0.0.1', '', '', '1497550426', 0),
(36, 'Game of thrones', '127.0.0.1', '', '', '1497550472', 0),
(37, 'Game of thrones', '127.0.0.1', '', '', '1497550946', 0),
(38, 'Game of thrones', '127.0.0.1', '', '', '1497551237', 0),
(39, 'Game of thrones', '127.0.0.1', '', '', '1497551274', 0),
(40, 'game of thrones', '127.0.0.1', '', '', '1497551280', 0),
(41, 'game', '127.0.0.1', '', '', '1497551286', 0),
(42, 'game', '127.0.0.1', '', '', '1497551660', 0),
(43, 'game', '127.0.0.1', '', '', '1497551687', 0),
(44, 'game of thrones', '127.0.0.1', '', '', '1497551702', 0),
(45, 'game of thrones', '127.0.0.1', '', '', '1497551824', 0),
(46, 'game of thrones', '127.0.0.1', '', '', '1497551964', 0),
(47, 'Game of thrones', '127.0.0.1', '', '', '1497551996', 0),
(48, 'Game of thrones', '127.0.0.1', '', '', '1497552069', 0),
(49, 'House of cards', '127.0.0.1', '', '', '1497552081', 0),
(50, 'game', '127.0.0.1', '', '', '1497552086', 0),
(51, 'Game of thrones season 1', '127.0.0.1', '', '', '1497552096', 0),
(52, '\'', '127.0.0.1', '', '', '1497552120', 0),
(53, '\'', '127.0.0.1', '', '', '1497552261', 0),
(54, 'house', '127.0.0.1', '', '', '1497552273', 0),
(55, 'house', '127.0.0.1', '', '', '1497552326', 0),
(56, 'house', '127.0.0.1', '', '', '1497552332', 0),
(57, 'house', '127.0.0.1', '', '', '1497552385', 0),
(58, 'Game of thrones season 1 episode 13', '127.0.0.1', '', '', '1497552397', 0),
(59, '\'', '127.0.0.1', '', '', '1497553043', 0),
(60, '\'', '127.0.0.1', '', '', '1497553075', 0),
(61, '\'', '127.0.0.1', '', '', '1497553110', 0),
(62, '\'', '127.0.0.1', '', '', '1497553214', 0),
(63, '\'', '127.0.0.1', '', '', '1497553244', 0),
(64, '\'', '127.0.0.1', '', '', '1497553487', 0),
(65, '\'', '127.0.0.1', '', '', '1497553499', 0),
(66, 'Game of thrones season 1 episode 13', '127.0.0.1', '', '', '1497553926', 0),
(67, 'Game of thrones season 1 episode 13', '127.0.0.1', '', '', '1497554293', 0),
(68, 'Game of thrones season 1 episode 13', '127.0.0.1', '', '', '1497554636', 0),
(69, 'Game of thrones season 1 episode 13', '127.0.0.1', '', '', '1497554918', 0),
(70, 'Game of thrones season 1 episode 13', '127.0.0.1', '', '', '1497555025', 0),
(71, 'Game of thrones season 1 episode 13', '127.0.0.1', '', '', '1497555031', 0),
(72, '\'', '127.0.0.1', '', '', '1497555039', 0),
(73, '\'', '127.0.0.1', '', '', '1497555116', 0),
(74, '\'', '127.0.0.1', '', '', '1497555304', 0),
(75, '\'', '127.0.0.1', '', '', '1497555330', 0),
(76, '\'', '127.0.0.1', '', '', '1497555372', 0),
(77, '\'', '127.0.0.1', '', '', '1497555495', 0),
(78, '\'', '127.0.0.1', '', '', '1497555552', 0),
(79, '\'', '127.0.0.1', '', '', '1497555565', 0),
(80, '\'', '127.0.0.1', '', '', '1497555582', 0),
(81, '\'', '127.0.0.1', '', '', '1497555608', 0),
(82, '\'', '127.0.0.1', '', '', '1497555646', 0),
(83, '\'', '127.0.0.1', '', '', '1497555682', 0),
(84, '\'', '127.0.0.1', '', '', '1497555693', 0),
(85, '\'', '127.0.0.1', '', '', '1497555747', 0),
(86, '\'', '127.0.0.1', '', '', '1497555937', 0),
(87, '\'', '127.0.0.1', '', '', '1497555952', 0),
(88, '\'', '127.0.0.1', '', '', '1497555997', 0),
(89, '\'', '127.0.0.1', '', '', '1497556030', 0),
(90, '\'', '127.0.0.1', '', '', '1497556040', 0),
(91, '\'', '127.0.0.1', '', '', '1497556053', 0),
(92, '\'', '127.0.0.1', '', '', '1497556162', 0),
(93, '\'', '127.0.0.1', '', '', '1497556184', 0),
(94, '\'', '127.0.0.1', '', '', '1497556192', 0),
(95, '\'', '127.0.0.1', '', '', '1497556213', 0),
(96, '\'', '127.0.0.1', '', '', '1497556224', 0),
(97, '\'', '127.0.0.1', '', '', '1497556241', 0),
(98, '\'', '127.0.0.1', '', '', '1497556260', 0),
(99, '\'', '127.0.0.1', '', '', '1497556328', 0),
(100, '\'', '127.0.0.1', '', '', '1497556337', 0),
(101, '\'', '127.0.0.1', '', '', '1497556357', 0),
(102, '\'', '127.0.0.1', '', '', '1497556398', 0),
(103, '\'', '127.0.0.1', '', '', '1497556410', 0),
(104, '\'', '127.0.0.1', '', '', '1497556432', 0),
(105, 'game', '127.0.0.1', '', '', '1497556441', 0),
(106, 'house', '127.0.0.1', '', '', '1497556446', 0),
(107, 'the', '127.0.0.1', '', '', '1497556451', 0),
(108, 'house', '127.0.0.1', '', '', '1497556477', 0),
(109, 'the', '127.0.0.1', '', '', '1497556483', 0),
(110, 'of', '127.0.0.1', '', '', '1497556487', 0),
(111, 'of', '127.0.0.1', '', '', '1497556509', 0),
(112, 'of', '127.0.0.1', '', '', '1497557705', 0),
(113, 'of', '127.0.0.1', '', '', '1497557719', 0),
(114, 'of', '127.0.0.1', '', '', '1497557752', 0),
(115, '\'', '127.0.0.1', '', '', '1497557760', 0),
(116, 'Game of thrones season 1 episode 13', '127.0.0.1', '', '', '1497557768', 0),
(117, 'Game of thrones season 1 episode 13', '127.0.0.1', '', '', '1497557792', 0),
(118, 'Game of thrones season 1 episode 13', '127.0.0.1', '', '', '1497557807', 0),
(119, '\'', '127.0.0.1', '', '', '1497557813', 0),
(120, 'the', '127.0.0.1', '', '', '1497557819', 0),
(121, 'of', '127.0.0.1', '', '', '1497557829', 0),
(122, 'of', '127.0.0.1', '', '', '1497557882', 0),
(123, 'the', '127.0.0.1', '', '', '1497557900', 0),
(124, 'and', '127.0.0.1', '', '', '1497557904', 0),
(125, 'Game of thrones season 1 episode 13', '127.0.0.1', '', '', '1497557934', 0),
(126, 'Game of thrones season 1 episode 13', '127.0.0.1', '', '', '1497557971', 0),
(127, 'Game of thrones season 1 episode 13', '127.0.0.1', '', '', '1497558007', 0),
(128, 'Game of thrones season 1 episode 13', '127.0.0.1', '', '', '1497558019', 0),
(129, 'Game of thrones season 1 episode 13', '127.0.0.1', '', '', '1497558030', 0),
(130, 'of', '127.0.0.1', '', '', '1497558033', 0),
(131, 'the', '127.0.0.1', '', '', '1497558042', 0),
(132, 'Game of thrones season 1 episode 13', '127.0.0.1', '', '', '1497558065', 1),
(133, 'Game of thrones season 1 episode 13', '127.0.0.1', '', '', '1497558080', 1),
(134, 'keepo', '127.0.0.1', 'keepo', 'hamza2life@gmail.com', '1498690622', 1),
(135, 'the kappa', '127.0.0.1', '', '', '1499943960', 0),
(136, 'the kappa', '127.0.0.1', '', '', '1499943981', 0),
(137, 'game of', '127.0.0.1', '', '', '1499944050', 0),
(138, 'game of  thrones', '127.0.0.1', '', '', '1499944095', 0),
(139, 'game of', '127.0.0.1', '', '', '1499944101', 0),
(140, 'game of', '127.0.0.1', '', '', '1499944125', 0),
(141, 'Game of thrones', '127.0.0.1', '', '', '1499944140', 0),
(142, 'Game of thrones', '127.0.0.1', '', '', '1499944160', 0),
(143, 'of', '127.0.0.1', '', '', '1499944169', 0),
(144, ' ?????????', '127.0.0.1', '', '', '1500052297', 0),
(145, '????', '127.0.0.1', '', '', '1500052376', 0),
(146, '????', '127.0.0.1', '', '', '1500052545', 0),
(147, '????', '127.0.0.1', '', '', '1500052769', 0),
(148, 'array(PDO::MYSQL_ATTR_INIT_COMMAND => \'SET NAMES utf8\')', '127.0.0.1', '', '', '1500052930', 0),
(149, 'M?t Danh: K? Toán', '127.0.0.1', '', '', '1500052938', 0),
(150, 'M?t Danh: K? Toán', '127.0.0.1', '', '', '1500053082', 0);

-- --------------------------------------------------------

--
-- Structure de la table `flixytodo`
--

CREATE TABLE `flixytodo` (
  `id` int(11) NOT NULL,
  `text` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `flixytodo`
--

INSERT INTO `flixytodo` (`id`, `text`, `status`) VALUES
(1, 'fsdf', 1);

-- --------------------------------------------------------

--
-- Structure de la table `flixyviewslog`
--

CREATE TABLE `flixyviewslog` (
  `ip` varchar(255) NOT NULL,
  `serieid` int(11) NOT NULL,
  `epid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `flixyviewslog`
--

INSERT INTO `flixyviewslog` (`ip`, `serieid`, `epid`) VALUES
('127.0.0.1', 1, 0),
('127.0.0.1', 2, 0),
('66.249.65.54', 2, 0),
('92.30.136.49', 2, 0),
('154.49.205.169', 16, 0),
('89.200.240.32', 22, 0),
('89.200.240.193', 22, 0),
('154.49.204.68', 23, 0),
('89.200.240.197', 16, 0),
('89.200.240.135', 25, 0),
('89.200.240.102', 25, 0),
('66.249.65.55', 4, 0),
('89.200.240.151', 15, 0),
('89.200.240.157', 20, 0),
('89.200.240.20', 22, 0),
('89.200.240.193', 16, 0),
('89.200.240.54', 16, 0),
('89.200.240.135', 22, 0),
('64.150.217.168', 7, 0),
('64.150.217.168', 17, 0),
('89.200.240.42', 22, 0),
('89.200.240.124', 23, 0),
('89.200.240.112', 16, 0),
('89.200.240.108', 25, 0),
('154.49.204.78', 25, 0),
('76.188.10.239', 29, 0),
('84.201.133.57', 13, 0),
('141.8.143.217', 24, 0),
('66.249.65.55', 2, 0),
('66.249.65.54', 23, 0),
('80.60.235.9', 12, 0),
('89.200.240.44', 22, 0),
('89.200.240.96', 20, 0),
('66.249.65.55', 30, 0),
('89.200.240.104', 16, 0),
('154.49.204.69', 16, 0),
('89.200.240.163', 22, 0),
('66.249.65.54', 4, 0),
('89.200.240.149', 22, 0),
('89.200.240.120', 23, 0),
('89.200.240.8', 16, 0),
('89.200.240.131', 25, 0),
('89.200.240.92', 25, 0),
('66.249.65.54', 5, 0),
('74.192.72.159', 2, 0),
('66.249.65.55', 19, 0),
('139.168.47.172', 29, 0),
('24.253.108.128', 3, 0),
('99.106.28.179', 22, 0),
('89.200.240.12', 15, 0),
('100.43.91.1', 1, 0),
('196.1.139.161', 32, 0),
('84.201.133.27', 5, 0),
('66.249.65.54', 9, 0),
('84.201.133.17', 25, 0),
('66.249.65.54', 19, 0),
('216.246.233.44', 26, 0),
('31.204.159.220', 1, 0),
('89.200.240.8', 22, 0),
('154.49.205.170', 20, 0),
('73.61.18.36', 18, 0),
('73.61.18.36', 20, 0),
('89.200.240.92', 16, 0),
('89.200.240.42', 16, 0),
('89.200.240.48', 22, 0),
('5.255.250.37', 17, 0),
('89.200.240.169', 22, 0),
('89.200.240.165', 23, 0),
('100.43.91.16', 8, 0),
('84.201.133.27', 13, 0),
('70.122.206.137', 11, 0),
('70.122.206.137', 19, 0),
('70.122.206.137', 21, 0),
('89.200.240.10', 16, 0),
('89.200.240.129', 25, 0),
('154.49.205.166', 25, 0),
('89.200.240.36', 15, 0),
('174.5.70.197', 32, 0),
('86.171.112.236', 12, 0),
('154.49.205.165', 20, 0),
('89.200.240.191', 22, 0),
('89.200.240.30', 16, 0),
('124.149.184.129', 2, 0),
('5.68.211.198', 25, 0),
('89.200.240.106', 16, 0),
('89.200.240.141', 22, 0),
('89.200.240.54', 22, 0),
('154.49.205.162', 23, 0),
('89.200.240.167', 16, 0),
('89.200.240.26', 25, 0),
('89.200.240.195', 25, 0),
('78.147.185.200', 6, 0),
('141.8.143.241', 23, 0),
('78.147.185.200', 7, 0),
('84.201.133.35', 22, 0),
('78.147.185.200', 9, 0),
('141.8.143.195', 7, 0),
('86.169.59.6', 5, 0),
('154.49.205.161', 22, 0),
('89.200.240.38', 16, 0),
('89.200.240.72', 16, 0),
('89.200.240.58', 22, 0),
('73.73.1.219', 6, 0),
('89.200.240.189', 22, 0),
('154.49.205.173', 23, 0),
('154.49.204.70', 16, 0),
('89.200.240.122', 25, 0),
('89.200.240.181', 25, 0),
('108.239.47.22', 11, 0),
('134.228.92.194', 16, 0),
('89.200.240.141', 15, 0),
('67.225.90.62', 4, 0),
('89.200.240.106', 22, 0),
('89.200.240.161', 20, 0),
('154.49.205.162', 16, 0),
('154.49.204.70', 22, 0),
('89.200.240.64', 23, 0),
('89.200.240.161', 25, 0),
('154.49.205.168', 16, 0),
('207.46.13.26', 2, 0),
('47.142.165.56', 5, 0),
('89.200.240.167', 15, 0),
('49.50.202.219', 2, 0),
('207.46.13.26', 30, 0),
('89.200.240.60', 20, 0),
('89.200.240.24', 16, 0),
('154.49.205.170', 16, 0),
('89.200.240.187', 22, 0),
('89.200.240.86', 22, 0),
('71.11.130.149', 9, 0),
('89.200.240.199', 16, 0),
('89.200.240.10', 25, 0),
('89.200.240.94', 25, 0),
('99.253.37.62', 2, 0),
('89.200.240.193', 15, 0),
('89.200.240.181', 22, 0),
('89.200.240.52', 20, 0),
('89.200.240.98', 16, 0),
('89.200.240.163', 16, 0),
('89.200.240.52', 22, 0),
('81.99.26.79', 2, 0),
('81.99.26.79', 16, 0),
('154.49.205.167', 23, 0),
('81.99.26.79', 18, 0),
('99.229.66.212', 7, 0),
('154.49.204.77', 16, 0),
('89.200.240.22', 25, 0),
('154.49.204.68', 25, 0),
('180.76.15.18', 15, 0),
('89.200.240.102', 15, 0),
('68.3.236.106', 8, 0),
('75.131.106.228', 6, 0),
('89.200.240.137', 20, 0),
('89.200.240.183', 22, 0),
('89.200.240.155', 16, 0),
('89.200.240.28', 16, 0),
('89.200.240.74', 22, 0),
('184.16.210.200', 17, 0),
('89.200.240.199', 22, 0),
('89.200.240.197', 23, 0),
('89.200.240.70', 16, 0),
('89.200.240.118', 25, 0),
('89.200.240.82', 25, 0),
('89.200.240.137', 15, 0),
('5.255.250.31', 19, 0),
('95.144.183.162', 8, 0),
('89.200.240.102', 22, 0),
('89.200.240.201', 20, 0),
('89.200.240.16', 16, 0),
('154.49.204.68', 22, 0),
('89.200.240.34', 23, 0),
('154.49.204.76', 16, 0),
('154.49.204.79', 25, 0),
('154.49.205.162', 25, 0),
('162.232.250.10', 17, 0),
('154.49.205.169', 22, 0),
('89.200.240.40', 20, 0),
('89.200.240.157', 22, 0),
('50.101.243.98', 28, 0),
('89.200.240.114', 22, 0),
('89.200.240.20', 23, 0),
('89.200.240.155', 25, 0),
('89.200.240.197', 25, 0),
('127.0.0.1', 27, 0),
('127.0.0.1', 7, 0),
('127.0.0.1', 6, 0),
('127.0.0.1', 5, 0),
('127.0.0.1', 3, 0),
('127.0.0.1', 12, 0),
('127.0.0.1', 14, 0),
('127.0.0.1', 13, 0),
('127.0.0.1', 4, 0),
('127.0.0.1', 35, 0);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `flixyanimes`
--
ALTER TABLE `flixyanimes`
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `flixyblog`
--
ALTER TABLE `flixyblog`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `flixycats`
--
ALTER TABLE `flixycats`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `flixycatsseries`
--
ALTER TABLE `flixycatsseries`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `flixycats_movies`
--
ALTER TABLE `flixycats_movies`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `flixycontact`
--
ALTER TABLE `flixycontact`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `flixyepisodes`
--
ALTER TABLE `flixyepisodes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `flixygrid`
--
ALTER TABLE `flixygrid`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `flixymails`
--
ALTER TABLE `flixymails`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `flixymovies`
--
ALTER TABLE `flixymovies`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `flixyreports`
--
ALTER TABLE `flixyreports`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `flixyseries`
--
ALTER TABLE `flixyseries`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `flixyseries_cats`
--
ALTER TABLE `flixyseries_cats`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `flixyservers`
--
ALTER TABLE `flixyservers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `server` (`server`);

--
-- Index pour la table `flixysuggest`
--
ALTER TABLE `flixysuggest`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `flixytodo`
--
ALTER TABLE `flixytodo`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `flixyanimes`
--
ALTER TABLE `flixyanimes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `flixyblog`
--
ALTER TABLE `flixyblog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `flixycats_movies`
--
ALTER TABLE `flixycats_movies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=458;
--
-- AUTO_INCREMENT pour la table `flixycontact`
--
ALTER TABLE `flixycontact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `flixyepisodes`
--
ALTER TABLE `flixyepisodes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=245;
--
-- AUTO_INCREMENT pour la table `flixygrid`
--
ALTER TABLE `flixygrid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `flixymails`
--
ALTER TABLE `flixymails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `flixymovies`
--
ALTER TABLE `flixymovies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=209;
--
-- AUTO_INCREMENT pour la table `flixyreports`
--
ALTER TABLE `flixyreports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=354;
--
-- AUTO_INCREMENT pour la table `flixyseries`
--
ALTER TABLE `flixyseries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `flixyseries_cats`
--
ALTER TABLE `flixyseries_cats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT pour la table `flixyservers`
--
ALTER TABLE `flixyservers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `flixysuggest`
--
ALTER TABLE `flixysuggest`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;
--
-- AUTO_INCREMENT pour la table `flixytodo`
--
ALTER TABLE `flixytodo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
