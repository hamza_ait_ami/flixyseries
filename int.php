<?php


class Flixy{

		public $id;

		public function GetCat($id){
			global $db;

			$s=$db->prepare("SELECT * FROM `flixycats` where id = :id");
			$s->execute(array(':id'=>$id));
			$g = $s->fetch(PDO:: FETCH_ASSOC);
			return $g['name'];
		}

		public function adfly($url, $key = 'd9ff43cfe4a7da6d5c6525c4e7c6cbf4' , $uid = '4615144' , $domain = 'adf.ly', $advert_type = 'int')
		{
		  // base api url
		  $api = 'http://api.adf.ly/api.php?';

		  // api queries
		  $query = array(
		    'key' => $key,
		    'uid' => $uid,
		    'advert_type' => $advert_type,
		    'domain' => $domain,
		    'url' => $url
		  );

		  // full api url with query string
		  $api = $api . http_build_query($query);
		  // get data
		  if ($data = file_get_contents($api))
		    return $data;
		}
		public function GetSerie($id){
			global $db;

			$s=$db->prepare("SELECT * FROM `flixyseries` where id = :id");
			$s->execute(array(':id'=>$id));
			$g = $s->fetch(PDO:: FETCH_ASSOC);
			return $g['name'];
		}		

		public function GetAnime($id){
			global $db;

			$s=$db->prepare("SELECT * FROM `flixyanimes` where id = :id");
			$s->execute(array(':id'=>$id));
			$g = $s->fetch(PDO:: FETCH_ASSOC);
			return $g['name'];
		}

		public function GetSerieId($id){
			global $db;

			$s=$db->prepare("SELECT * FROM `flixyepisodes` where id = :id");
			$s->execute(array(':id'=>$id));
			$g = $s->fetch(PDO:: FETCH_ASSOC);
			return $g['serieid'];
		}

		public function Get($from, $what, $w , $f){
			global $db;

			$s=$db->prepare("SELECT * FROM $from where $w = :id ");
			$s->execute(array(':id'=>$f));
			$g = $s->fetch(PDO:: FETCH_ASSOC);
			return $g[$what];
		}

		public function GetEpId($id){
			global $db;

			$s=$db->prepare("SELECT * FROM `flixyepisodes` where id = :id");
			$s->execute(array(':id'=>$id));
			$g = $s->fetch(PDO:: FETCH_ASSOC);
			return $g['epid'];
		}
		public function CheckSeasons($id,$seasons){
			global $db;
			$serie = $db->prepare("SELECT * FROM flixyseries where id = :id");
			$serie->execute(array(':id'=>$id));
			$gserieinfos = $serie->fetch(PDO:: FETCH_ASSOC);
					
			if( $seasons <= $gserieinfos['seasons']){
				return true;
			}else{
				return false;
			}
		}
		public function CheckSeasonsA($id,$seasons){
			global $db;
			$serie = $db->prepare("SELECT * FROM flixyanimes where id = :id");
			$serie->execute(array(':id'=>$id));
			$gserieinfos = $serie->fetch(PDO:: FETCH_ASSOC);
					
			if( $seasons <= $gserieinfos['seasons']){
				return true;
			}else{
				return false;
			}
		}
		public function CheckExist($id){
			global $db;
			$serie = $db->prepare("SELECT * FROM flixyseries where id = :id");
			$serie->execute(array(':id'=>$id));
			$gserieinfos = $serie->fetch(PDO:: FETCH_ASSOC);
					
			if($serie -> rowCount() > 0){
				return true;
			}else{
				return false;
			}
		}		

		public function OpenLoadStatus($id){
		global $db;
		$json = file_get_contents('https://api.openload.co/1/file/info?file='.$id.'&login=1ac01974f3bc0bc0&key=nF_YH4RJ');
		$result = json_decode($json, true);
		return $result["msg"];
					

		}



}
?>
