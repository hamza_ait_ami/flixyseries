<html>
<head>
	<title>Page Not found - 123 Movies Online</title>
	<meta charset='UTF-8'>
	<meta name='viewport' content='width=device-width, initial-scale=1.0'>
	<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,600,700' rel='stylesheet' type='text/css'>
	<link rel='stylesheet' href='./css/grid.css'>
	<link rel='stylesheet' href='./css/404.css'>
</head>
<body>
<style type="text/css">
	
</style>
<main>
<div class="container secondrow">
	<div class="row">
		<div class="column-12 main-logo">
			<img src="images/logo4.png">
		</div>
	</div>
	<div class="row">
		<div class="column-6 firstdiv">
			404
		</div>
		<div class="column-6 seconddiv">
			Page not found
			<p>Something went wrong, we couldn't find the page you are looking for.
			Here are our main links :
			<ul>
				<li><a href="./index/">Home</a></li>
				<li><a href="./contact/">Contact us</a></li>
				<li><a href="./popular/">Popular series</a></li>
				<li><a href="./movies/">Movies</a></li>
			</ul>
			</p>
		</div>
	</div>	
</div>
</main>
</body>
</html>

