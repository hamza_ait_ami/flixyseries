<?php
include_once("./header.php");
ini_set('max_execution_time', 600); 
?>
<div class="wrapper">
	<div class="row">
		<div class="column-12">
			<div class="widget">
				<h3>Series ( 
				<?php if (@$_GET['type'] == ""){ echo "<b><u>"; } echo'<a href="./serie.php">LIVE</a>'; if (@$_GET['type'] == ""){ echo '</u></b>'; } ?>  
				- 
				<?php if (@$_GET['type'] == "done"){ echo "<b><u>"; } echo'<a href="./serie.php?type=done">DONE</a>'; if (@$_GET['type'] == "done"){ echo '</u></b>'; } ?>
				-
				<?php if (@$_GET['type'] == "Canceled"){ echo "<b><u>"; } echo'<a href="./serie.php?type=Canceled">Canceled</a>'; if (@$_GET['type'] == "Canceled"){ echo '</u></b>'; } ?>
				)
				<a href="update_series.php" class="hlink" style="color:red"><i class="fa fa-refresh"></i></a><a href="add_serie.php" class="hlink"><i class="fa fa-plus"></i> Add a new serie &nbsp; &nbsp;</a>
				</h3>
				<table class="categories series_table" border="1">
					<tbody>
					<tr class="thead">
						<td><i class="fa fa-refresh"></i></td>
						<td>Title</td>
						<td><i class="fa fa-tags" aria-hidden="true"></i></td>
						<td>ImDB</td>
						<td>TMDid</td>
						<td><i class="fa fa-envelope-o"></i></td>
						<td><i class="fa fa-youtube-play" aria-hidden="true"></i></td>
						<td><i class="fa fa-cloud-upload" aria-hidden="true"></i></td>
						<td><i class="fa fa-heartbeat" aria-hidden="true"></i></td>
						<td width="13%">Edit</td>
						<td width="15%">Last episode</td>
						<td width="15%">Next episode</td>
					</tr>
					</tbody>

						<?php

if(@$_GET['type'] == "done"){
	$status='Ended';
}elseif(@$_GET['type'] == "Canceled"){
	$status='Ended';
}else{
	$status='Returning Series';
}

	$series = $db->prepare("SELECT * FROM flixyseries where status = '$status' order by status desc");
	$series->execute();
	$time = date("Y-m-d",time());
	while($gseries = $series->fetch(PDO:: FETCH_ASSOC)){	
		$c=$db->prepare("SELECT * FROM `flixyepisodes` where serieid = ".$gseries['id']." "); // ALL EPISODES
		$c->execute();
		$e=$db->prepare("SELECT * FROM `flixymails` where serieid = ".$gseries['id']);
		$e->execute();
		$z=$db->prepare("SELECT * FROM `flixyepisodes` where serieid = ".$gseries['id']." and air_date < '$time' and openload != '' "); // EPISODES OUT & UPLOADED
		$z->execute();
		$f=$db->prepare("SELECT * FROM `flixyepisodes` where serieid = ".$gseries['id']." and air_date > '$time' "); // EPISODES COMING SOON
		$f->execute();
		$l=$db->prepare("SELECT * FROM `flixyepisodes` where serieid = ".$gseries['id']." and air_date < '$time' order by air_date desc");
		$l->execute();
		$gl = $l->fetch(PDO:: FETCH_ASSOC);
		$p=$db->prepare("SELECT * FROM `flixyepisodes` where serieid = ".$gseries['id']." and air_date > '$time' order by air_date asc");
		$p->execute();
		$ge = $p->fetch(PDO:: FETCH_ASSOC);
		echo'

					<tr>

						<td><a href="update_serie.php?id='.$gseries['id'].'" class="add"><i class="fa fa-refresh"></i></a></td>
						<td >'.$gseries['name'].' <span style="float:right;"><a href="' .$url.'/serie/'.$gseries['id'].'/'.str_replace(" ", "-",$gseries['name']).'/" target="_blank" ><i class="fa fa-external-link" aria-hidden="true"></i></a></span></td>
						<td>'.$gseries['seasons'].'</td>
						<td>'.$gseries['imdbID'].'</td>
						<td>'.$gseries['TMDid'].'</td>
						<td>'.$e -> rowCount().'</td>
						<td>'.$c -> rowCount().'</td>
						<td>';
						if (($c -> rowCount() - $f -> rowCount()) != $z -> rowCount()){
							echo '<font color="red"><b>'.$z -> rowCount().'</b> <i class="fa fa-exclamation" aria-hidden="true"></i></font>';
						}else{
							echo  $z-> rowCount();
						}
					 	echo '</td><td>';

						if ($gseries['status'] == 'Returning Series'){
							echo '<i class="fa fa-toggle-on" aria-hidden="true" style="color:green;"></i>';
						}else{
							echo '<i class="fa fa-toggle-off" aria-hidden="true" style="color:red;"></i>';
						}



						echo '</td>
						
						<td class="fivelinks">						
							<a href="episode.php?id='.$gseries['id'].'" class="add"><i class="fa fa-plus"></i></a>
							<a href="edit_serie.php?id='.$gseries['id'].'&type=0" class="edit2"><i class="fa fa-pencil"></i></a>
							<a href="show_episodes.php?serie='.$gseries['id'].'" class="edit2"><i class="fa fa-folder-open-o" aria-hidden="true"></i></a>
							<a class="show-series"><i class="fa fa-bars"></i></a>
						</td>
						<td>
								'.$gl['air_date'].'
						</td>
						<td>
								'.$ge['air_date'].'
						</td>
					</tr>
					<tbody class="serie-seasons">
					<tr>
						<td colspan="11" width="80%">Seasons</td>
						<td>Episodes</td>
					</tr>

					';

				$seres = $db->prepare("SELECT * FROM flixyseries where id = :id");
				$seres->bindParam(':id', $gseries['id'], PDO::PARAM_STR);       
				$seres->execute();
				$gseres = $seres->fetch(PDO:: FETCH_ASSOC);
				$season = $gseres['seasons'];
				while($season >= 1){
					$s=$db->prepare("SELECT * FROM `flixyepisodes` where season = $season and serieid = ".$gseries['id']." and type = 0");
					$s->execute();
					$e=$db->prepare("SELECT * FROM `flixyepisodes` where season = $season and serieid = ".$gseries['id']." and type = 0 and openload != '' ");
					$e->execute();
					echo'					
						<tr>
							<td colspan="11" width="80%">Season '.$season.'</td>
							<td >'.$s -> rowCount().'  | ';
						if ($s -> rowCount() != $e -> rowCount()){
							echo '<font color="red"><b>'.$e -> rowCount().'</b> <i class="fa fa-exclamation" aria-hidden="true"></i></font>';
						}else{
							echo $e -> rowCount();
						}

							echo '</td>
						</tr>					
					';
					$season--;
				}
					echo'</tbody>';

}

?>
				</table>
			</div>
		</div>
	</div>
</div>
</body>
</html>