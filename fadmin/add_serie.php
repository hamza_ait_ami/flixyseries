<?php

include_once("./header.php");

?>
<br>
<div class="wrapper">
	<div class="row">
	<?php
ini_set('max_execution_time', 65456456); 
if (@$_GET["type"] == 'add'){
	$imdbID = $_POST['imdbID'];
	$date = date("Y-m-d H:i:s");


	
	$episode = $db->prepare("SELECT * FROM flixyseries where imdbID = '".$imdbID."'");
	$episode->execute();
	$gepisodeinfos = $episode->fetch(PDO:: FETCH_ASSOC);
	if(empty($gepisodeinfos)){

	$sql = "INSERT INTO flixyseries(name,original_name,alternative_titles,keywords,seasons,pic,description,imdbID,TMDid,date,status,releaseday,dayofrelease) VALUES (
	            :name ,
	            :original_name ,
	            :alternative_titles ,
	            :keywords ,
	            :seasons ,
	            :pic ,
	            :description ,
	            :imdbID ,
	            :TMDid ,
	            :date ,
	            :status ,
	            :releaseday ,
	            :dayofrelease
	            )";
	$serieid = $db->prepare("SELECT * FROM flixyseries order by id desc limit 1");
	$serieid->execute();
	$getserieid = $serieid->fetch(PDO:: FETCH_ASSOC);            
	$geserieid = $getserieid['id'] + 1;     
	$jsonomdb = file_get_contents("https://api.themoviedb.org/3/find/$imdbID?api_key=ca98718b60a75fc1211c5fa04ec792be&language=en-US&external_source=imdb_id");
	$result =json_decode($jsonomdb,  true);
	$TMDid = $result["tv_results"][0]['id'];
	if (isset($TMDid)){
		$jsonomdb2 = file_get_contents("https://api.themoviedb.org/3/tv/$TMDid?api_key=ca98718b60a75fc1211c5fa04ec792be&language=en-US&append_to_response=credits,keywords,alternative_titles");
		$result2 =json_decode($jsonomdb2,  true);  
		$z = 0;
		while ($z <= (count($result2['credits']['cast']) - 1)){
			$actor = $db->prepare("SELECT * FROM flixyactors where person_id = '".$result2['credits']['cast'][$z]['id']."' ");
			$actor->execute();
				if ($actor -> rowCount() == 0 ){
					$stmt = $db->prepare("INSERT INTO flixyactors(person_id,name,gender) VALUES (
						            :person_id ,
						            :name,
						            :gender
						        )");
								$stmt->bindParam(':person_id', $result2['credits']['cast'][$z]['id'], PDO::PARAM_STR);        
								$stmt->bindParam(':name', $result2['credits']['cast'][$z]['name'], PDO::PARAM_STR);        
								$stmt->bindParam(':gender',  $result2['credits']['cast'][$z]['gender'], PDO::PARAM_STR);       
							    $stmt->execute(); 
					if ($result2['credits']['cast'][$z]['profile_path'] != null){
						$url = 'https://image.tmdb.org/t/p/w300_and_h450_bestv2'.$result2['credits']['cast'][$z]['profile_path']; 
					}else{
						$url = '../images/no_picture.jpg';
					}
					
					//echo $url." <br>"; 
					$content = file_get_contents($url);
					$fp = fopen("../images/actors/".str_replace(" ", "-",$result2['credits']['cast'][$z]['name']).".jpg", "w");
					fwrite($fp, $content);
					fclose($fp);  
					//echo $result2['credits']['cast'][$z]['name']." Added <br>";
				}

			$actorserie = $db->prepare("SELECT * FROM flixyactors_series where actor_id = '".$result2['credits']['cast'][$z]['id']."' and serie_id = $geserieid ");
			$actorserie->execute();
				if ($actorserie -> rowCount() == 0 ){
					$stmt = $db->prepare("INSERT INTO flixyactors_series(actor_id,serie_id,charaacter) VALUES (
						            :actor_id ,
						            :serie_id,
						            :charaacter
						        )");
					$stmt->bindParam(':actor_id', $result2['credits']['cast'][$z]['id'], PDO::PARAM_STR);        
					$stmt->bindParam(':serie_id', $geserieid, PDO::PARAM_STR);        
					$stmt->bindParam(':charaacter',  $result2['credits']['cast'][$z]['character'], PDO::PARAM_STR);       
					$stmt->execute(); 
					//echo "Character : ".$gactors['credits']['cast'][$z]['character']." Added <br>";	
				}
			
		$z++;
		}
		$i = 1;
		while ($i <= $result2['number_of_seasons']){
			$getseasons = file_get_contents("https://api.themoviedb.org/3/tv/$TMDid/season/$i?api_key=ca98718b60a75fc1211c5fa04ec792be&language=en-US");
			$seasons =json_decode($getseasons,  true);   
			$episodes = count($seasons['episodes']); 
			$j = 0;
			while ($j < $episodes){
				$stmt = $db->prepare("INSERT INTO flixyepisodes(epid,season,name,air_date,serieid) VALUES (
		            :epid ,
		            :season,
		            :name,
		            :air_date,
		            :serieid
		        )");
				$epid = $j + 1;
				$stmt->bindParam(':season', $i, PDO::PARAM_STR);        
				$stmt->bindParam(':air_date', $seasons['episodes'][$j]['air_date'], PDO::PARAM_STR);        
				$stmt->bindParam(':name', $seasons['episodes'][$j]['name'], PDO::PARAM_STR);        
				$stmt->bindParam(':serieid', $geserieid , PDO::PARAM_STR);       
				$stmt->bindParam(':epid',$epid, PDO::PARAM_STR);
			    $stmt->execute();  

			    if ($j == 150){
			    	 ob_flush();
        			 flush();
       				 sleep(2);
			    }

				$j++;
			}

			$i++;
		}
		ob_end_flush();
		if (@$result2['poster_path'] != null){
			$url= 'https://image.tmdb.org/t/p/w300_and_h450_bestv2'.$result2['poster_path'];  
		}else{
			$url = '../images/no_picture.jpg';
		}
		$content = file_get_contents($url);
		$fp = fopen("../images/series/".str_replace("?", "-",str_replace("/", "-",str_replace(" ", "-",$result2["name"]))).".jpg", "w");

		fwrite($fp, $content);
		fclose($fp);   

		if (@$result2['backdrop_path'] != null){
			$url= 'https://image.tmdb.org/t/p/w300_and_h450_bestv2'.$result2['backdrop_path'];  
		}else{
			$url = '../images/no_picture.jpg';
		}
		$content = file_get_contents($url);
		$fp = fopen("../images/series_background/".str_replace("?", "-",str_replace("/", "-",str_replace(" ", "-",$result2["name"]))).".jpg", "w");
		fwrite($fp, $content);
		fclose($fp);   

			                           
    	$stmt = $db->prepare($sql);	
		$stmt->bindParam(':pic', $url, PDO::PARAM_STR);   	                                              
		$stmt->bindParam(':name', $result2['name'], PDO::PARAM_STR);       
		$stmt->bindParam(':original_name', $result2['original_name'], PDO::PARAM_STR);       
		$stmt->bindParam(':alternative_titles', serialize($result2['alternative_titles']['results']), PDO::PARAM_STR);       
		$stmt->bindParam(':keywords', serialize($result2['keywords']['results']), PDO::PARAM_STR);       
		$stmt->bindParam(':dayofrelease', $result2['first_air_date'], PDO::PARAM_STR);       
		$stmt->bindParam(':releaseday', date("l",strtotime($result2['first_air_date'])), PDO::PARAM_STR);       
		$stmt->bindParam(':status', $result2['status'], PDO::PARAM_STR);       
		$stmt->bindParam(':seasons', $result2['number_of_seasons'], PDO::PARAM_STR);          
		$stmt->bindParam(':imdbID', $imdbID, PDO::PARAM_STR);          
		$stmt->bindParam(':TMDid', $TMDid, PDO::PARAM_STR);          
		$stmt->bindParam(':description', $result2['overview'], PDO::PARAM_STR);       
		$stmt->bindParam(':date',$date, PDO::PARAM_STR);       
		$stmt->execute(); 	

	$serieid = $db->prepare("SELECT * FROM flixyseries order by id desc limit 1");
	$serieid->execute();
	$getserieid = $serieid->fetch(PDO:: FETCH_ASSOC); 

	foreach($result2["genres"] as $key){
		
		$sql = "INSERT INTO flixyseries_cats(serieid,catid) VALUES (
		            :id_movie ,
		            :id_cat 

		            )";
		                                          
		$stmt = $db->prepare($sql);
		$stmt->bindParam(':id_movie',$getserieid["id"] , PDO::PARAM_STR);       
		$stmt->bindParam(':id_cat',$key['id'], PDO::PARAM_STR);  
		$stmt->execute(); 
	}
		header("Location: ./serie.php");
	}else{



		echo "Error : ID not valid .. ";
		header("refresh:2;url:/add_serie.php");

	}

	}else{

		echo '<center><div class="error">Error : ID exists .. </div></center> ';
		header("refresh:2;url:/add_serie.php");	

	}

	//echo $result2['name'];
	//echo "<pre>".print_r($result2)."</pre>";

}



?>



		<div class="column-12">

			<div class="widget add-category">

				<h3>Add a new serie</h3>

				<form method="post" action="./add_serie.php?type=add">

				<label for="">Imdb ID</label>

					<input type="text" placeholder="imdbID" name="imdbID">

				<button type="submit">Submit</button>

				</form>

			</div>

	</div>

</div>

</body>

</html>