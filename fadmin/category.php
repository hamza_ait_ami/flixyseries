<?php

include_once("./header.php");

?>
	<script>
		$(document).ready(function() {

	$(".klappa").click(function() {
	var id = $(this).attr("data-catid");
	var text = $("#txt"+id).val();

	$.post( "./ajax.php?type=editcat&id="+id+"&text="+text, function( data ) {
  		location.reload();
	});
	return false;
});	
	$("#sub2").one('click', function() {
	var text2 = $("#text3").val();
	if (text2 == '') {
			alert("Insertion Failed Some Fields are Blank....!!");
	} else {
			$.post("ajax.php?type=cat", {
			text3: text2
		}, function(data) {
			location.reload();
		});
	return false;
	}
});
});
	</script>
<br>
<div class="wrapper">
	<div class="row">
		<div class="column-8">
			<div class="widget">
				<h3>Edit categories</h3>
				<table class="categories" border="1">
					<tr class="thead">
						<td>#</td>
						<td>Category name</td>
						<td>Series</td>
						<td>Edit</td>
					</tr>

						<?php
	$series = $db->prepare("SELECT * FROM flixycats");
	$series->execute();
	while($gseries = $series->fetch(PDO:: FETCH_ASSOC)){
		$s=$db->prepare("SELECT * FROM `flixyseries_cats` where catid=".$gseries['id']);
$s->execute();
	echo'
					<tr>
						<td>'.$gseries['id'].'</td>
						<td>'.$gseries['name'].'<input type="text" id="txt'.$gseries['id'].'" value="'.$gseries['name'].'"></td>
						<td>'.$s -> rowCount().'</td>
						<td>
							<a href="#" class="edit"><i class="fa fa-pencil"></i></a>
							<a href="./ajax.php?type=deletecat&id='.$gseries['id'].'" class="delete"><i class="fa fa-trash"></i></a>
							<button class="klappa" data-catid="'.$gseries['id'].'">Save changes</button>
						</td>
					</tr>';

				}

				?>
				</table>
			</div>
		</div>
		<div class="column-4">
			<div class="widget add-category">
				<h3>Add a new category</h3>
				<form>
				<input type="text" id="text3" placeholder="Category name">
				<button type="submit" id ="sub2">Submit</button>
				</form>
			</div>
		</div>
	</div>
</div>

</body>
</html>