<?php

include_once("./header.php");

?>
<br>
<div class="wrapper">
	<div class="row">

	<?php

if (@$_GET["type"] == 'edit'){

		$sql = "UPDATE flixymovies set name = :name,
 									   openload = :openload,
 									   Quality = :Quality,
 									   splash = :splash,
 									   imdbID = :imdbID
								      WHERE  id = :id";
		$openload = str_replace("/","",str_replace("https://openload.co/f/","",$_POST['openload']));    						      
		$openloadvid = file_get_contents("https://api.openload.co/1/file/getsplash?login=1ac01974f3bc0bc0&key=nF_YH4RJ&file=".$openload);
		$getopenloadvid =json_decode($openloadvid, true);                            
		$stmt = $db->prepare($sql);
		                                              
		$stmt->bindParam(':id', $_POST['id'], PDO::PARAM_STR);       
		$stmt->bindParam(':name', $_POST['name'], PDO::PARAM_STR);          
		$stmt->bindParam(':openload', $_POST['openload'], PDO::PARAM_STR);
		$stmt->bindParam(':Quality', $_POST['Quality'], PDO::PARAM_STR);
		$stmt->bindParam(':imdbID', $_POST['imdbID'], PDO::PARAM_STR);
		$stmt->bindParam(':splash',$getopenloadvid['result'], PDO::PARAM_STR);  
		$stmt->execute(); 

header("Location: ./movie.php");

}
$id = (int) $_GET['id'];
$movie = $db->prepare("SELECT * FROM flixymovies where id = ".$id);
$movie->execute();
$gmovie = $movie->fetch(PDO:: FETCH_ASSOC);

?>
		<div class="column-12">
			<div class="widget add-category">
				<h3>Edit movie : <?=$flixy -> get("flixymovies","name","id",$id)?></h3>
				<form method="post" action="./edit_movie.php?type=edit">
				<label for="">Movie Name</label>
				<input type="text" placeholder="Movie name" name="name" value="<?=$gmovie['name']?>">
				<?php
				foreach (unserialize($gmovie['alternative_titles']) as $key => $value) {
						echo "<img src='../images/flags/".strtolower($value['iso_3166_1']).".png' /> : ".$value['title']."<br>";
				}
				?>
				<label for="">Movie Year</label>
				<input type="text" placeholder="Movie Year" name="Year" value="<?=$gmovie['Year']?>">
				<label for="">Quality</label>
				<select name="Quality" id="">
					<option value="HD" <?php if($gmovie['Quality'] == 'HD'){echo("selected");}?> >HD</option>
					<option value="720p" <?php if($gmovie['Quality'] == '720p'){echo("selected");}?> >720p</option>
					<option value="1080p" <?php if($gmovie['Quality'] == '1080p'){echo("selected");}?> >1080p</option>
					<option value="480p" <?php if($gmovie['Quality'] == '480p'){echo("selected");}?> >480p</option>
					<option value="CAM" <?php if($gmovie['Quality'] == 'CAM'){echo("selected");}?> >Cam</option>
				</select>
				<label for="">Openload</label>
				<input type="text" placeholder="Id" name="openload" value="<?=$gmovie['openload']?>">
				<label for="">IMdb</label>
				<input type="text" placeholder="IMdb" name="imdbID" value="<?=$gmovie['imdbID']?>">
				<button type="submit">Submit</button>
				<input type="hidden" name="id" value="<?=$gmovie['id']?>">
				</form>
			</div>
	</div>
</div>
</body>
</html>