<?php 

include("./header.php");
if ($_POST) {
	$sql = "INSERT INTO flixytodo(text) VALUES (:text)";
		                                          
	$stmt = $db->prepare($sql);		                                              
	$stmt->bindParam(':text', $_POST['text'], PDO::PARAM_STR);          
	$stmt->execute(); 		
	header("location:./index.php");
}
$se=$db->prepare("SELECT * FROM `flixyseries`");
$se->execute();
$v=$db->prepare("SELECT * FROM `flixyviewslog`");
$v->execute();
$e=$db->prepare("SELECT * FROM `flixyepisodes`");
$e->execute();
$m=$db->prepare("SELECT * FROM `flixymovies`");
$m->execute();
$c=$db->prepare("SELECT * FROM `flixycats`");
$c->execute();
/*$shaday = sha1("?vn=3&s=summary&f=JSON&g=daily&sd=".date("d")."&sm=".date("m")."&sy=".date("Y")."&ed=".date("d")."&em=".date("m")."&ey=".date("Y")."&pi=11114737&n=10&t=".time()."&u=hamzalife142536");
$jsonday = file_get_contents("http://api.statcounter.com/stats/?vn=3&s=summary&f=JSON&g=daily&sd=".date("d")."&sm=".date("m")."&sy=".date("Y")."&ed=".date("d")."&em=".date("m")."&ey=".date("Y")."&pi=11114737&n=10&t=".time()."&u=hamzalife&sha1=".$shaday);
$result = json_decode($jsonday, true);*/
?>
<div class="wrapper">
	<div class="row">
		<div class="column-4">
		<h1>Episodes Today</h1>
		<ul class="notification_list">
				<?php
				$daysbefore = date("Y-m-d",time() - 24*60*60);
				$daysafter = date("Y-m-d",time() + 2*24*60*60);
				$episodes = $db->prepare("SELECT * FROM flixyepisodes where openload = '' and  air_date >= '$daysbefore' and air_date <= '$daysafter' order by air_date asc");
				$episodes->execute();
				$counts = $episodes -> rowCount();
				while($gepisodes = $episodes->fetch(PDO:: FETCH_ASSOC)){
				$serie = $db->prepare("SELECT * FROM flixyseries where id = :id");
				$serie->execute(array(':id'=>$gepisodes['serieid']));
				$gserieinfos = $serie->fetch(PDO:: FETCH_ASSOC);
				echo'
					<li>
						<a href="'.ROOTPATH.'/episode/'.$gepisodes['id'].'/'.str_replace(" ", "-",$gserieinfos['name']).'-season-'.$gepisodes['season'].'-episode-'.$gepisodes['epid'].'/">
							<img src="'.ROOTPATH.'/images/series/'.str_replace(" ", "-",$gserieinfos['name']).'.jpg" alt="">
							<span class="notif_epi">Episode '.$gepisodes['epid'].', Season '.$gepisodes['season'].'</span>
							';
							if ($gepisodes['air_date'] < date("Y-m-d",time() + 24*60*60)){
								echo '<span class="new_notif">'.date("l",strtotime($gepisodes['air_date'])).'</span></span>';
							}else{
								echo '<span class="new_notif" style="background-color:#563a3a;">'.date("l",strtotime($gepisodes['air_date'])).'</span></span>';
							}
							echo'
							<span class="notif_serie">'.$gserieinfos['name'].' <a href="./episode.php?id='.$gserieinfos['id'].'"><i class="fa fa-plus"></i></a></span>
						</a>
					</li>
				';
				}
				?>
		</ul>
		</div>	
		<div class="column-4">
		<h1>Movies Today</h1>
		<ul class="notification_list">
				<?php
				$daysbefore = date("Y-m-d",time() - 3*24*60*60);
				$daysafter = date("Y-m-d",time() + 3*24*60*60);
				$movies = $db->prepare("SELECT * FROM flixymovies where openload = '' and  Released < '$daysbefore' order by Released desc limit 15 ");
				$movies->execute();
				$counts = $movies -> rowCount();
				while($gmovies = $movies->fetch(PDO:: FETCH_ASSOC)){
				echo'
					<li>
						<a href="'.ROOTPATH.'/movies/'.$gmovies['id'].'/'.str_replace(" ", "-",$gmovies['name']).'/">
							<img src="'.ROOTPATH.'/images/movies/'.str_replace("?", "_",str_replace(":", "_",str_replace(" ", "-",$gmovies['name']))).'.jpg" alt="">
							<span class="notif_epi">'.$gmovies['name'].'</span>
							';
							if ($gmovies['Released'] < date("Y-m-d",time() + 24*60*60)){
								echo '<span class="new_notif">New</span></span>';
							}
							echo'
							<span class="notif_serie">'.$gmovies['Released'].'</span>
						</a>
					</li>
				';
				}
				?>
		</ul>
		</div>
		<div class="column-4">
			<div class="stats">
				<i class="fa fa-video-camera"></i>
				<span class="Number"> <?= $e -> rowCount()?></span>
				<span class="Text">Episodes</span>
			</div>			
			<div class="stats">
				<i class="fa fa-tv"></i>
				<span class="Number"> <?= $se -> rowCount()?></span>
				<span class="Text">Series</span>
			</div>
			<div class="stats">
				<i class="fa fa-play"></i>
				<span class="Number"> <?= $m -> rowCount()?></span>
				<span class="Text">Movies</span>
			</div>
			<div class="stats">
				<i class="fa fa-files-o"></i>
				<span class="Number"> <?= $c -> rowCount()?></span>
				<span class="Text">Category</span>
			</div>
			<div class="widget to-do">
				<h3><i class="fa fa-calendar-check-o"></i> To do list</h3>
				<ul>
				<?php

					$todolist = $db->prepare("SELECT * FROM flixytodo");
					$todolist->execute();

					while($gtodolist = $todolist->fetch(PDO:: FETCH_ASSOC)){
							echo '<li>';
							if ($gtodolist['status'] == 0) {
								echo "<del>".$gtodolist['text']."</del>";
							}else{
								echo $gtodolist['text'];
							}
							echo' <span style="float:right;" class="record" id="record-',$gtodolist['id'],'"><a href="./ajax.php?type=deletetodo&id=',$gtodolist['id'],'" class="delete"><i class="fa fa-trash"></i></span></a></li>';
					}

				?>
					<li id="result"></li>

				</ul>
				<a class="new-task" >Add a new task</a>
				<form action="index.php" method="post" name="form" class="to-do-form">
					<input type="text" name="text" id="text" placeholder="Write tasks here">
					<button  id="sub" type="submit"  onclick = "sendData()">Submit</button>
				</form>
			</div>
		</div>	
	</div>	
	<div class="row">
		<div class="column-4">
			<table class="categories" border="1">
					<tr class="thead">
						<td>#</td>
						<td>Category name</td>
						<td>Series</td>
					</tr>

						<?php
	$series = $db->prepare("SELECT * FROM flixycatsseries");
	$series->execute();
	while($gseries = $series->fetch(PDO:: FETCH_ASSOC)){
		$s=$db->prepare("SELECT * FROM `flixyseries_cats` where catid=".$gseries['id']);
$s->execute();
	echo'
					<tr>
						<td>'.$gseries['id'].'</td>
						<td>'.$gseries['name'].'</td>
						<td>'.$s -> rowCount().'</td>
					</tr>';

				}

				?>
				</table>
		</div>
	<div class="column-4">
	<table class="categories" border="1">
					<tr class="thead">
						<td>#</td>
						<td>Genre name</td>
						<td>Movies</td>
					</tr>

						<?php
	$series = $db->prepare("SELECT * FROM flixycats");
	$series->execute();
	while($gseries = $series->fetch(PDO:: FETCH_ASSOC)){
		$s=$db->prepare("SELECT * FROM `flixycats_movies` where id_cat=".$gseries['id']);
$s->execute();
	echo'
					<tr>
						<td>'.$gseries['id'].'</td>
						<td>'.$gseries['name'].'</td>
						<td>'.$s -> rowCount().'</td>
					</tr>';

				}

				?>
				</table>
		</div>
	<div class="column-4">

	</div>
</div>	<!--
<div class="row">
		<div class="column-4">
			
		</div>
		<div class="column-8">
			<div class="widget">
				<iframe src="http://statcounter.com/p11114737/summary/daily-pur-labels-area-last7days.png" scrolling="no" frameborder="0" width="100%" height="430" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" ></iframe></center>
			</div>
		</div>
</div>-->
	</div>


</body>
</html>