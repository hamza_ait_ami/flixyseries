<?php

include_once("./header.php");

?>
<br>
<div class="wrapper">
	<div class="row">

	<?php

if (@$_GET["type"] == 'add'){
	$id = $_POST['IMdb'];
	$episode = $db->prepare("SELECT * FROM flixymovies where imdbID = '".$id."'");
	$episode->execute();
	$gepisodeinfos = $episode->fetch(PDO:: FETCH_ASSOC);
	if(empty($gepisodeinfos)){
		$sql = "INSERT INTO flixymovies(name,alternative_titles,keywords,openload,Released,Quality,trailer,Rating,Runtime,splash,Plot,Year,pic,tagline,adult,Genre,imdbID) VALUES (
		            :name ,
		            :alternative_titles ,
		            :keywords ,
		            :openload ,
		            :Released,
		            :Quality,
		            :trailer,
		            :Rating,
		            :Runtime,
		            :splash,
		            :Plot,
		            :Year,
		            :pic,
		            :tagline,
		            :adult,
		            :genre,
		            :imdbID

		            )";
		                                          
	$stmt = $db->prepare($sql);
                                                 
	$openload = str_replace("/","",str_replace("https://openload.co/f/","",$_POST['openload']));      
	$imdbID = $_POST['IMdb'];
	$jsonomdb2 = file_get_contents("https://api.themoviedb.org/3/movie/$imdbID?api_key=ca98718b60a75fc1211c5fa04ec792be&language=en-US&append_to_response=credits,videos,keywords,alternative_titles");
	$result2 =json_decode($jsonomdb2, true);
	//$streamango = file_get_contents("https://api.fruithosted.net/remotedl/add?login=4JAFYFbeCn&key=xsKvcDX8&url=https://openload.co/f/$openload");
	//$mango =json_decode($streamango, true);
	$url= 'https://image.tmdb.org/t/p/w300_and_h450_bestv2'.$result2['poster_path'];  
	$url2= 'https://image.tmdb.org/t/p/w500'.$result2['backdrop_path'];  
	$openloadvid = file_get_contents("https://api.openload.co/1/file/getsplash?login=1ac01974f3bc0bc0&key=nF_YH4RJ&file=".$openload);
	$getopenloadvid =json_decode($openloadvid, true);
	$youtubeid = 'https://www.youtube.com/embed/'.@$result2['videos']['results'][0]['key'];
	$content = file_get_contents($url);
	$fp = fopen("../images/movies/".str_replace("/", "_",str_replace("?", "_",str_replace(":", "_",str_replace(" ", "-",$result2["title"])))).".jpg", "w");
	fwrite($fp, $content);
	fclose($fp);
	$content2 = file_get_contents($url2);
	$fp2 = fopen("../images/background/".str_replace("/", "_",str_replace("?", "_",str_replace(":", "_",str_replace(" ", "-",$result2["title"])))).".jpg", "w");
	fwrite($fp2, $content2);
	fclose($fp2);
	$stmt->bindParam(':name', $result2["title"], PDO::PARAM_STR);             
	$stmt->bindParam(':alternative_titles', serialize($result2["alternative_titles"]["titles"]), PDO::PARAM_STR);             
	$stmt->bindParam(':keywords',  serialize($result2["keywords"]["results"]), PDO::PARAM_STR);             
	$stmt->bindParam(':pic', $url, PDO::PARAM_STR);             
	$stmt->bindParam(':Quality', $_POST['Quality'], PDO::PARAM_STR);       
	$stmt->bindParam(':Year', date('Y', strtotime($result2['release_date'])), PDO::PARAM_STR);       
	$stmt->bindParam(':openload', $openload, PDO::PARAM_STR);       
	//$stmt->bindParam(':streamango', $mango['result']['id'], PDO::PARAM_STR);       
	$stmt->bindParam(':imdbID', $_POST['IMdb'], PDO::PARAM_STR);       
	$stmt->bindParam(':Runtime',$result2["runtime"], PDO::PARAM_STR);
	$stmt->bindParam(':trailer',$youtubeid, PDO::PARAM_STR);
	$stmt->bindParam(':Rating',$result2["vote_average"], PDO::PARAM_STR);
	$stmt->bindParam(':Plot',$result2["overview"], PDO::PARAM_STR);
	$stmt->bindParam(':Released',$result2['release_date'], PDO::PARAM_STR);       
	$stmt->bindParam(':tagline',$result2['tagline'], PDO::PARAM_STR);       
	$stmt->bindParam(':adult',$result2['adult'], PDO::PARAM_STR);       
	$stmt->bindParam(':genre',serialize($result2['genres']), PDO::PARAM_STR);       
	$stmt->bindParam(':splash',$getopenloadvid['result'], PDO::PARAM_STR);       

	$stmt->execute(); 
	$serieid = $db->prepare("SELECT * FROM flixymovies order by id desc limit 1");
	$serieid->execute();
	$getserieid = $serieid->fetch(PDO:: FETCH_ASSOC);    
	foreach($result2["genres"] as $key){
		
		$sql = "INSERT INTO flixycats_movies(id_movie,id_cat) VALUES (
		            :id_movie ,
		            :id_cat 

		            )";
		                                          
		$stmt = $db->prepare($sql);
		$stmt->bindParam(':id_movie',$getserieid["id"] , PDO::PARAM_STR);       
		$stmt->bindParam(':id_cat',$key['id'], PDO::PARAM_STR);  
		$stmt->execute(); 
	}
	$z = 0;
	while ($z < count($result2['credits']['cast'])){
			$actor = $db->prepare("SELECT * FROM flixyactors where person_id = '".$result2['credits']['cast'][$z]['id']."' ");
			$actor->execute();
				if ($actor -> rowCount() == 0 ){
					$stmt = $db->prepare("INSERT INTO flixyactors(person_id,name,gender) VALUES (
						            :person_id ,
						            :name,
						            :gender
						        )");
								$stmt->bindParam(':person_id', $result2['credits']['cast'][$z]['id'], PDO::PARAM_STR);        
								$stmt->bindParam(':name', $result2['credits']['cast'][$z]['name'], PDO::PARAM_STR);        
								$stmt->bindParam(':gender',  $result2['credits']['cast'][$z]['gender'], PDO::PARAM_STR);       
							    $stmt->execute(); 

					$url = 'https://image.tmdb.org/t/p/w300_and_h450_bestv2'.$result2['credits']['cast'][$z]['profile_path']; 
					//echo $url." <br>"; 

					$content = file_get_contents($url);
					if( $result2['credits']['cast'][$z]['profile_path'] == null ){
						$content = file_get_contents("../images/no_picture.jpg");
					}
					$fp = fopen("../images/actors/".str_replace(" ", "-",$result2['credits']['cast'][$z]['name']).".jpg", "w");
					fwrite($fp, $content);
					fclose($fp);  
					//echo $gactors['cast'][$z]['name']." Added <br>";
				}

			$actorserie = $db->prepare("SELECT * FROM flixyactors_movies where actor_id = '".$result2['credits']['cast'][$z]['id']."' and movie_id = ".$getserieid["id"]." ");
			$actorserie->execute();
				if ($actorserie -> rowCount() == 0 ){
					$stmt = $db->prepare("INSERT INTO flixyactors_movies(actor_id,movie_id,charaacter) VALUES (
						            :actor_id ,
						            :movie_id,
						            :charaacter
						        )");
					$stmt->bindParam(':actor_id', $result2['credits']['cast'][$z]['id'], PDO::PARAM_STR);        
					$stmt->bindParam(':movie_id', $getserieid["id"], PDO::PARAM_STR);        
					$stmt->bindParam(':charaacter',  $result2['credits']['cast'][$z]['character'], PDO::PARAM_STR);       
					$stmt->execute(); 
					echo "Character : ".$result2['credits']['cast'][$z]['character']." Added <br>";	
				}
		if ($z == 10){
			break;
		}	
		$z++;
	}
	header("Location: ./add_movie.php");
	}else{
	header("Location: ./add_movie.php");	
	}
}

?>
		<div class="column-12">
			<div class="widget add-category">
				<h3>Add a new movie</h3>
				<form method="post" action="./add_movie.php?type=add">
				<label for="">Quality</label>
				<select name="Quality" id="">
					<option value="HD">HD</option>
					<option value="720p">720p</option>
					<option value="1080p">1080p</option>
					<option value="480p">480p</option>
					<option value="CAM">Cam</option>
				</select>
				<label for="">Openload</label>
				<input type="text" placeholder="Id" name="openload">
				<label for="">IMdb</label>
				<input type="text" placeholder="IMdb" name="IMdb">
				
				<button type="submit">Submit</button>
				</form>
			</div>
		</div>
	</div>
</div>

</body>
</html>