<?php
session_start();
include("../flixy.php");
if ($_SERVER['REMOTE_ADDR'] == "127.0.0.1"){
$_SESSION['login'] = 1;
}elseif($_SESSION['login'] !== 1){

	header("Location: ./login.php");
}
?>

<html content="text/html;charset=utf-8" >
<head>
	<title>Flixy Series Cpanel</title>
	<meta charset="UTF-8">
	<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,400italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel='stylesheet' href='css/fullcalendar.min.css' />
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/grid.css">
	<link rel="stylesheet" href="css/stylesheet.css">
	<link rel="stylesheet" href="css/added.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	<script src='js/moment.min.js'></script>
	<script src='js/fullcalendar.min.js'></script>
	<script src="js/script.mini.js"></script>
</head>
<body>

<header>
	<div class="header">
		<img src="images/logo2.png" class="logo">
		<div class="identity">
			<img src="images/face1.jpg" alt="">
			<span>
				Kappa
				<br>
				<span class="admin">Administrator</span>
			</span>
		</div>
	</div>
	<nav>
		<ul class="headericon">
			<li>
				<a href="index.php"><i class="fa fa-home"></i>Home</a>
			</li>
			<li>
				<a href="../index.php" target="_blank"><i class="fa fa-forward"></i>FlixySeries</a>
			</li>
			<li>
				<a href="series.php"><i class="fa fa-film"></i>Series</a>
			</li>			
			<li>
				<a href="movie.php"><i class="fa fa-file-video-o"></i>Movies</a>
			</li>								
			<li>
				<a href="messages.php"><i class="fa fa-envelope"></i>Messages</a>
			</li>				
			<li>
				<a href="reports.php"><i class="fa fa-flag"></i>Reports</a>
			</li>				
			<li>
				<a href="suggestions.php"><i class="fa fa-lightbulb-o"></i>Suggestions</a>
			</li>	
			<li>
				<a href="searchs.php"><i class="fa fa-history"></i>Search</a>
			</li>		
			<li>
				<a href="logout.php"><i class="fa fa-power-off"></i>Log out</a>
			</li>
		</ul>
	</nav>
	<div class="secondheader">
	<?php
   date_default_timezone_set('America/New_York');
   $currenttime = date('l h:i:s:u');
   list($hrs,$mins,$secs,$msecs) = split(':',$currenttime);
   echo "<span style='float:left;'><b>&nbsp;&nbsp; ET Time Now : $hrs:$mins:$secs\n </b></span>"; ?>
		Welcome to 123MoviesOnline admin panel I hope you are having a good day . :) 
	</div>	
</header>
