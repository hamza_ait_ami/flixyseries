<?php

include_once("./header.php");

?>
<br>
<div class="wrapper">
	<div class="row">
	<?php
ini_set('max_execution_time', 65456456); 
error_reporting(E_ALL);
ini_set("display_errors", 1);
	$date = date("Y-m-d H:i:s");
	$id = $_GET['id'];
	$episode = $db->prepare("SELECT * FROM flixyseries where id = '".$id."'");
	$episode->execute();
	$gepisodeinfos = $episode->fetch(PDO:: FETCH_ASSOC);
	$TMDid = $gepisodeinfos['TMDid'];     

	$jsonomdb2 = file_get_contents("https://api.themoviedb.org/3/tv/$TMDid?api_key=ca98718b60a75fc1211c5fa04ec792be&language=en-US&append_to_response=credits,keywords,alternative_titles");
	$result2 =json_decode($jsonomdb2, true);  
	$seasonTMD = $result2['number_of_seasons'];
	$sql = "UPDATE flixyseries set name = :name, original_name = :original_name, alternative_titles = :alternative_titles, keywords = :keywords, status = '".$result2['status']."' , dayofrelease = '".$result2['first_air_date']."' , releaseday ='".date("l",strtotime($result2['first_air_date']))."' WHERE  id = :id";                                      
	$stmt = $db->prepare($sql);	   
	$alt_titles = serialize($result2['alternative_titles']['results']);                                           
	$keywords =  serialize($result2['keywords']['results']);                                           
	$stmt->bindParam(':id', $id, PDO::PARAM_STR);       
	$stmt->bindParam(':name', $result2['name'], PDO::PARAM_STR);       
	$stmt->bindParam(':original_name', $result2['original_name'], PDO::PARAM_STR);       
	$stmt->bindParam(':alternative_titles',$alt_titles , PDO::PARAM_STR);       
	$stmt->bindParam(':keywords',$keywords , PDO::PARAM_STR);       
	$stmt->execute(); 			
	if (isset($TMDid)){
		$z = 0;
		while ($z < count($result2['credits']['cast'])){
			$actor = $db->prepare("SELECT * FROM flixyactors where person_id = '".$result2['credits']['cast'][$z]['id']."' ");
			$actor->execute();
				if ($actor -> rowCount() == 0 ){
					$stmt = $db->prepare("INSERT INTO flixyactors(person_id,name,gender) VALUES (
						            :person_id ,
						            :name,
						            :gender
						        )");
								$stmt->bindParam(':person_id', $result2['credits']['cast'][$z]['id'], PDO::PARAM_STR);        
								$stmt->bindParam(':name', $result2['credits']['cast'][$z]['name'], PDO::PARAM_STR);        
								$stmt->bindParam(':gender',  $result2['credits']['cast'][$z]['gender'], PDO::PARAM_STR);       
							    $stmt->execute(); 

					$url = 'https://image.tmdb.org/t/p/w300_and_h450_bestv2'.$result2['credits']['cast'][$z]['profile_path']; 
					//echo $url." <br>"; 
					$content = file_get_contents($url);
					$fp = fopen("../images/actors/".str_replace(" ", "-",$result2['credits']['cast'][$z]['name']).".jpg", "w");
					fwrite($fp, $content);
					fclose($fp);  
					//echo $gactors['cast'][$z]['name']." Added <br>";
				}

			$actorserie = $db->prepare("SELECT * FROM flixyactors_series where actor_id = '".$result2['credits']['cast'][$z]['id']."' and serie_id = $id ");
			$actorserie->execute();
				if ($actorserie -> rowCount() == 0 ){
					$stmt = $db->prepare("INSERT INTO flixyactors_series(actor_id,serie_id,charaacter) VALUES (
						            :actor_id ,
						            :serie_id,
						            :charaacter
						        )");
					$stmt->bindParam(':actor_id', $result2['credits']['cast'][$z]['id'], PDO::PARAM_STR);        
					$stmt->bindParam(':serie_id', $id, PDO::PARAM_STR);        
					$stmt->bindParam(':charaacter',  $result2['credits']['cast'][$z]['character'], PDO::PARAM_STR);       
					$stmt->execute(); 
					echo "Character : ".$result2['credits']['cast'][$z]['character']." Added <br>";	
				}
			
		$z++;
		}
		$i = 1;
		if ($gepisodeinfos['seasons'] != $result2['number_of_seasons']){
			while ($i <= $result2['cast']){
			$getseasons = file_get_contents("https://api.themoviedb.org/3/tv/$TMDid/season/$i?api_key=ca98718b60a75fc1211c5fa04ec792be&language=en-US");
			$seasons =json_decode($getseasons, true);   
			$episodesTMD = count($seasons['episodes']); 
			$episodes = $db->prepare("SELECT * FROM flixyepisodes where serieid = '".$id."' and season = '".$i."'");
			$episodes->execute();
			$gepisodes = $episodes -> rowCount();
    		$j = $gepisodes;
			if ($episodesTMD != $gepisodes){
				while ($j < $episodes){
					$stmt = $db->prepare("INSERT INTO flixyepisodes(epid,season,name,air_date,serieid) VALUES (
			            :epid ,
			            :season,
			            :name,
			            :air_date,
			            :serieid
			        )");
					$epid = $j + 1;
					$stmt->bindParam(':season', $i, PDO::PARAM_STR);        
					$stmt->bindParam(':air_date', $seasons['episodes'][$j]['air_date'], PDO::PARAM_STR);        
					$stmt->bindParam(':name', $seasons['episodes'][$j]['name'], PDO::PARAM_STR);        
					$stmt->bindParam(':serieid', $id , PDO::PARAM_STR);       
					$stmt->bindParam(':epid',$epid, PDO::PARAM_STR);
				    $stmt->execute();  

				    if ($j == 150){
				    	 ob_flush();
	        			 flush();
	       				 sleep(2);
				    }

					$j++;
				}
				echo "Done";
			}

			$i++;
			}
		$sql = "UPDATE flixyseries set seasons = '".$result2['number_of_seasons']."' WHERE  id = :id";                                      
		$stmt = $db->prepare($sql);	                                              
		$stmt->bindParam(':id', $id, PDO::PARAM_STR);       
		$stmt->execute(); 			
		}elseif ($gepisodeinfos['seasons'] == $result2['number_of_seasons']){
    	$k = 1;	
			while ($k <= $gepisodeinfos['seasons']){
			$getseasons = file_get_contents("https://api.themoviedb.org/3/tv/$TMDid/season/$k?api_key=ca98718b60a75fc1211c5fa04ec792be&language=en-US");
			$seasons =json_decode($getseasons, true);   
			$episodesTMD = count($seasons['episodes']); 
			$episodes = $db->prepare("SELECT * FROM flixyepisodes where serieid = '".$id."' and season = '".$k."'");
			$episodes->execute();
			$gepisodes = $episodes -> rowCount();
			$j = 0;
			
				while ($j < $episodesTMD){
				$epid = $j + 1;
					if ($j < $gepisodes){
						$sql = "UPDATE flixyepisodes set name = :name,air_date = :air_date WHERE  epid = :epid and season = '".$k."' and serieid = :serieid  ";                     
						$stmt = $db->prepare($sql);	                                              
						$stmt->bindParam(':epid', $epid, PDO::PARAM_STR);       
						$stmt->bindParam(':serieid', $id, PDO::PARAM_STR);       
						$stmt->bindParam(':air_date', $seasons['episodes'][$j]['air_date'], PDO::PARAM_STR);        
						$stmt->bindParam(':name', $seasons['episodes'][$j]['name'], PDO::PARAM_STR);       
						$stmt->execute(); 		
					
					}else{
						$stmt = $db->prepare("INSERT INTO flixyepisodes(epid,season,name,air_date,serieid) VALUES (
				            :epid ,
				            :season,
				            :name,
				            :air_date,
				            :serieid
				        )");
						$stmt->bindParam(':season', $k, PDO::PARAM_STR);        
						$stmt->bindParam(':air_date', $seasons['episodes'][$j]['air_date'], PDO::PARAM_STR);        
						$stmt->bindParam(':name', $seasons['episodes'][$j]['name'], PDO::PARAM_STR);        
						$stmt->bindParam(':serieid', $id , PDO::PARAM_STR);       
						$stmt->bindParam(':epid',$epid, PDO::PARAM_STR);
					    $stmt->execute();  
					  echo "Episode #".$epid." Added";
					}
					$j++;
				}
			$k++;	
			}	
			echo "Done";
		
		}else{
			echo "nothing changed";
		}

		$genres = $db->prepare("SELECT * FROM flixyseries_cats where serieid = '".$id."'");
		$genres->execute();
		$ggenres = $genres -> rowCount();
		if ($result2["genres"] != $ggenres){

			$sql = "DELETE FROM flixyseries_cats where serieid = :id";                                          
			$stmt = $db->prepare($sql);	                                              
			$stmt->bindParam(':id', $id, PDO::PARAM_STR);       
			$stmt->execute(); 


			foreach($result2["genres"] as $key){
				
				$sql = "INSERT INTO flixyseries_cats(serieid,catid) VALUES (
				            :id_movie ,
				            :id_cat 

				            )";
				                                          
				$stmt = $db->prepare($sql);
				$stmt->bindParam(':id_movie',$id , PDO::PARAM_STR);       
				$stmt->bindParam(':id_cat',$key['id'], PDO::PARAM_STR);  
				$stmt->execute(); 
			}
		}
		//header("refresh:2;url:/serie.php");
	}


?>


</div>

</body>

</html>