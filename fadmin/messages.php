<?php

include_once("./header.php");

?>
<br>
<div class="wrapper">
	<div class="row">
		<div class="column-12">
			<div class="widget">
				<?php
					if (@$_GET["type"] == 'see'){
				$series = $db->prepare("SELECT * FROM flixycontact where id = :id ");
				$series->bindParam(':id', $_GET['id'], PDO::PARAM_STR);       
				$series->execute();
				$gseries = $series->fetch(PDO:: FETCH_ASSOC);
						echo'
							<h3>Message subject : '.$gseries['title'].'</h3>
							
							<div class="message-wrapper">
								'.$gseries['text'].'
								<span class="time">Sent 5 hours ago by '.$gseries['email'].'</span>
							</div>
							<textarea class="reply-tarea" placeholder="Reply here"></textarea>
							<button>Reply and delete</button>
							<button class="delete-btn">Delete</button>
						';
					}else{
				?>
				<h3>Messages</h3>
				<table class="categories" border="1">
					<tr class="thead">
						<td>#</td>
						<td>Message subject</td>
						<td>Sent since</td>
						<td>Tools</td>
					</tr>

					<?php
	$series = $db->prepare("SELECT * FROM flixycontact order by date desc");
	$series->execute();
	while($gseries = $series->fetch(PDO:: FETCH_ASSOC)){	
					echo'
					<tr>
						<td>'.$gseries['id'].'</td>
						<td>'.$gseries['title'].'</td>
						<td>'.date(" d M - Y " ,$gseries['date']).'</td>
						<td>
							<a href="messages.php?type=see&id='.$gseries['id'].'" class="add"><i class="fa fa-eye"></i></a>
							<a href="dsd" id="delete"><i class="fa fa-trash"></i></a>
						</td>
					</tr>
					';
				}
					?>
					<?php
						}
					?>
				</table>
			</div>
		</div>
	</div>
</div>

</body>
</html>