<?php

include_once("./header.php");

?>
<br>
<div class="wrapper">
	<div class="row">
	<div class="column-12">
			<div class="widget">
	<?php
ini_set('max_execution_time', 65456456); 
error_reporting(E_ALL);
ini_set("display_errors", 1);
	$id = $_GET['id'];
	$episode = $db->prepare("SELECT * FROM flixymovies where id = '".$id."'");
	$episode->execute();
	$gepisodeinfos = $episode->fetch(PDO:: FETCH_ASSOC);
	$imdbID = $gepisodeinfos['imdbID'];     
	echo $gepisodeinfos['name'].' - '.$gepisodeinfos['Year'].' <br>';  
	$jsonomdb2 = file_get_contents("https://api.themoviedb.org/3/movie/$imdbID?api_key=ca98718b60a75fc1211c5fa04ec792be&language=en-US&append_to_response=credits,videos,alternative_titles");
	$result2 =json_decode($jsonomdb2, true);
	if (!empty($result2['videos']['results'][rand(0, (count($result2['videos']['results']) - 1 ))]['key'])){
		$youtubeid = 'https://www.youtube.com/embed/'.$result2['videos']['results'][rand(0, (count($result2['videos']['results']) - 1 ))]['key'];	
		echo 'Trailer Added : '.$youtubeid.' <br>';           							
	}else{
		$youtubeid = '';				
	}
	//echo "<e>".print_r($result2)."</e>";
	$sql = "UPDATE flixymovies set name = :name , alternative_titles = :alternative_titles , Rating = '".$result2['vote_average']."' , Runtime = '".$result2['runtime']."' , tagline = :tagline , adult = '".$result2['adult']."' , trailer = :trailer , Genre = :genres WHERE  id = :id"; 
	echo 'New Title : '.$result2['title'].'<br>';                                       
	$stmt = $db->prepare($sql);	                     
	$alt_titles = serialize($result2["alternative_titles"]["titles"]);                         
	$genres = serialize($result2["genres"]);                         
	$stmt->bindParam(':id', $id, PDO::PARAM_STR);       
	$stmt->bindParam(':tagline', $result2['tagline'], PDO::PARAM_STR);       
	$stmt->bindParam(':name', $result2['title'], PDO::PARAM_STR);       
	$stmt->bindParam(':genres', $genres, PDO::PARAM_STR);       
	$stmt->bindParam(':alternative_titles', $alt_titles, PDO::PARAM_STR);       
	$stmt->bindParam(':trailer', $youtubeid, PDO::PARAM_STR);       
	$stmt->execute();
	$url2= 'https://image.tmdb.org/t/p/w500'.$result2['backdrop_path'];  
	if (!file_exists("../images/background/".str_replace("/", "_",str_replace("?", "_",str_replace(":", "_",str_replace(" ", "-",$result2["title"])))).".jpg")){
		$content2 = file_get_contents($url2);
		$fp2 = fopen("../images/background/".str_replace("/", "_",str_replace("?", "_",str_replace(":", "_",str_replace(" ", "-",$result2["title"])))).".jpg", "w");
		fwrite($fp2, $content2);
		fclose($fp2); 	
		echo "Background added <br>";		
	}
	$url3= 'https://image.tmdb.org/t/p/w1280'.$result2['backdrop_path'];  
	if (!file_exists("../images/fullbackground/".str_replace("/", "_",str_replace("?", "_",str_replace(":", "_",str_replace(" ", "-",$result2["title"])))).".jpg")){
		$content3 = file_get_contents($url3);
		$fp3 = fopen("../images/fullbackground/".str_replace("/", "_",str_replace("?", "_",str_replace(":", "_",str_replace(" ", "-",$result2["title"])))).".jpg", "w");
		fwrite($fp3, $content3);
		fclose($fp3); 	
		echo "Full Background added <br>";		
	}
	$url= 'https://image.tmdb.org/t/p/w300_and_h450_bestv2'.$result2['poster_path'];  
	if (!file_exists("../images/movies/".str_replace("/", "_",str_replace("?", "_",str_replace(":", "_",str_replace(" ", "-",$result2["title"])))).".jpg")){
		$content = file_get_contents($url);
		$fp = fopen("../images/movies/".str_replace("/", "_",str_replace("?", "_",str_replace(":", "_",str_replace(" ", "-",$result2["title"])))).".jpg", "w");
		fwrite($fp, $content);
		fclose($fp); 	
		echo "Poster added <br>";		
	}
	$z = 0;
	while ($z < count($result2['credits']['cast'])){
			$actor = $db->prepare("SELECT * FROM flixyactors where person_id = '".$result2['credits']['cast'][$z]['id']."' ");
			$actor->execute();
				if ($actor -> rowCount() == 0 ){
					$stmt = $db->prepare("INSERT INTO flixyactors(person_id,name,gender) VALUES (
						            :person_id ,
						            :name,
						            :gender
						        )");
								$stmt->bindParam(':person_id', $result2['credits']['cast'][$z]['id'], PDO::PARAM_STR);        
								$stmt->bindParam(':name', $result2['credits']['cast'][$z]['name'], PDO::PARAM_STR);        
								$stmt->bindParam(':gender',  $result2['credits']['cast'][$z]['gender'], PDO::PARAM_STR);       
							    $stmt->execute(); 

					$url = 'https://image.tmdb.org/t/p/w300_and_h450_bestv2'.$result2['credits']['cast'][$z]['profile_path']; 
					//echo $url." <br>"; 
					$content = file_get_contents($url);
					if( !$content ){
						$content = file_get_contents("../images/no_picture.jpg");
					}
					$fp = fopen("../images/actors/".str_replace(" ", "-",$result2['credits']['cast'][$z]['name']).".jpg", "w");
					fwrite($fp, $content);
					fclose($fp);  
					//echo $gactors['cast'][$z]['name']." Added <br>";
				}

			$actorserie = $db->prepare("SELECT * FROM flixyactors_movies where actor_id = '".$result2['credits']['cast'][$z]['id']."' and movie_id = $id ");
			$actorserie->execute();
				if ($actorserie -> rowCount() == 0 ){
					$stmt = $db->prepare("INSERT INTO flixyactors_movies(actor_id,movie_id,charaacter) VALUES (
						            :actor_id ,
						            :movie_id,
						            :charaacter
						        )");
					$stmt->bindParam(':actor_id', $result2['credits']['cast'][$z]['id'], PDO::PARAM_STR);        
					$stmt->bindParam(':movie_id', $id, PDO::PARAM_STR);        
					$stmt->bindParam(':charaacter',  $result2['credits']['cast'][$z]['character'], PDO::PARAM_STR);       
					$stmt->execute(); 
					echo "Character : ".$result2['credits']['cast'][$z]['character']." Added <br>";	
				}
		if ($z == 10){
			break;
		}	
		$z++;
	}
	/*$genres = $db->prepare("SELECT * FROM flixycats_movies where id_movie = $id ");
	$genres->execute();
	if ($actorserie -> rowCount() != count($result2["genres"])){
							foreach($result2["genres"] as $key){
								
								$sql = "INSERT INTO flixycats_movies(id_movie,id_cat) VALUES (
								            :id_movie ,
								            :id_cat 

								            )";
								                                          
								$stmt = $db->prepare($sql);
								$stmt->bindParam(':id_movie',$id , PDO::PARAM_STR);       
								$stmt->bindParam(':id_cat',$key['id'], PDO::PARAM_STR);  
								$stmt->execute(); 
								echo "Genre ".$key['name'] . " Added";
							}
	}*/
	echo '<center><h1>'.$gepisodeinfos['name']." Updated </h1></center>";
		header("refresh:2;url:/movie.php");	
?>


</div>
</div>
</div>
</div>

</body>

</html>